#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main(void)
{
  while(!feof(stdin))
  {
    char c;

    while(stdin)
    {
      c = getchar();
      if (!isdigit(c))
      {
        putchar(toupper(c));
        continue;
      }
      putchar(c);
    }
  }
  return 0;
}
