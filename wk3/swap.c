#include <stdio.h>

void swap(unsigned *a, unsigned *b)
{
  unsigned temp = *a;
  *a = *b;
  *b = temp;
}

int main(void)
{
  unsigned a = 2, b = 3;
  swap(&a, &b);
  printf("%u %u\n", a, b); // should print 3 2
  return 0;
}
