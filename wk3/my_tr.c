#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
  if (argc < 0 && argc > 2)
  {
    printf("Invalid input");
    return 1;
  }

  char *c = argv[1];
  char *r = argv[2];
  char input[100];

  if (strlen(r) != strlen(c))
  {
    printf("Invalid replace length");
    return 1;
  }

  while(!feof(stdin))
  {
    fscanf(stdin, "%s", input);

    for (int i = 0; i < strlen(input); i++)
    {
      for (int j = 0; j < strlen(c); j++)
      {
        if (input[i] == c[j])
        {
          input[i] = r[j];
        }
      }
    }
  }

  printf("%s\n", input);

  return 0;
}
