#include <stdio.h>

int my_strlen(const char *ptr)
{
  int len;
  for (len = 0; *ptr != '\0'; ptr++)
  {
    len++;
  }
  return len;
}

int main(void)
{
  printf("%d\n", my_strlen("")); // 0
  printf("%d\n", my_strlen("1234")); // 4
  printf("%d\n", my_strlen("bleh\n")); // 5
  printf("%d\n", my_strlen("apex kek\n")); // 7
  printf("%d\n", my_strlen("apex\0kek\n")); // 4

  return 0;
}
