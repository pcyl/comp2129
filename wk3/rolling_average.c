#include <stdio.h>

int main(void)
{
  float count = 0;
  float sum = 0;
  float avg = 0;

  while(!feof(stdin))
  {
    float num;
    fscanf(stdin, "%f", &num);
    sum += num;
    count++;
    avg = sum/count;

    if (count < 4)
    {
      printf("invalid input\n");
    }
    else
    {
      printf("%.3f\n", avg);
    }
  }
  return 0;
}
