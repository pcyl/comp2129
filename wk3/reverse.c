#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main(void)
{
  while(!feof(stdin))
  {
    char c[100];

    while(stdin)
    {
      fgets(c, 100, stdin);

      for (int i = strlen(c) - 2; i > -1; i--)
      {
        putchar(c[i]);
      }
      printf("\n");
    }
  }
  return 0;
}
