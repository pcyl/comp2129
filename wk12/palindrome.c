#include <stdio.h>
#include <string.h>

int is_palin(char *str, int len) { 
	if (len <= 1) {
		return 1; 
	}
	if (str[0] != str[len-1]) { 
		return 0; 
	}
	return is_palin(str+1, len-2); 
}

int main(void) {
	char string[] = "racecar";
	if (is_palin(string, strlen(string))) {
		puts("Is a palindrome.");
	}
	else {
		puts("Not a palindrome");
	}
}