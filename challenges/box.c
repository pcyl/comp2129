// Mystery Box
#include <stdio.h>

int main(void)
{
  int width, height = 0;

  printf("Enter the size of the box: ");
  scanf("%d %d", &width, &height);

  if (width == 0 || height == 0)
  {
    return 0;
  }
  
  printf("+");
  for (int k = 0; k < width - 2; k++)
  {
    printf("-");
  }
  printf("+\n");

  if (height != 0)
  {
    for (int i = 0; i < height - 2; i++)
    {
      printf("|");
      for (int j = 0; j < width - 2; j++)
      {
        printf(" ");
      }
      printf("|\n");
    }
  }

  printf("+");
  for (int k = 0; k < width - 2; k++)
  {
    printf("-");
  }
  printf("+\n");

  return 0;
}
