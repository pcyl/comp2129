#include <stdio.h>
#include <unistd.h>

int main(void) {
    puts("about to launch /usr/bin/sort");
    if (execl("/usr/bin/sort", "sort", "forkdemo.c", NULL) == -1) { perror("exec failed");
    return 1;
}

puts("sort finished");
return 0;
}
