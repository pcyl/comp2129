#include <stdio.h>

struct vector
{
    int x, y;
};

union data
{
    int scalar;
    struct vector vec;
    int *ptr;
};

struct list_head
{
    union data data;
    struct list_head *prev, *next;
};

static void print_vector(union data data)
{
    printf("%d %d\n", data.vec.x, data.vec.y);
}

int main(void)
{
    print_vector((union data) { .vec = (struct vector) { 1, 1 } });
    printf("%lu\n", sizeof(union data));
    return 0;
}
