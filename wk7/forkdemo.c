#include <stdio.h>

#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

int main(void)
{
    int n = 6;

    puts("before fork");
    pid_t pid = fork();

    if (pid < 0)
    {
        perror("fork failed");
        return 1;
    }

    if (pid == 0)
    {
        // we are the child
        n++;
        sleep(1);
    }
    else
    {
        // we are the parent
        n*=2;
        wait(NULL);
    }

    printf("fork returned %d, n  is %d\n", pid, n);
    return 0;
}
