#! /usr/bin/env bash
# Read file line by line, verify card, calculate fare
# and check whether balance is enough

# your code here
FILE=$1
while read line;
do
    var1=$(echo $line | cut -d ":" -f 1 - | ./checksum)
    if [[ $var1 != "" ]]; then
	fare=$(echo $line | cut -d ":" -f 4,5 - | tr ":," " " | ./farecalc $2 $3 $4 $5)
	bal=$(echo $line | cut -d ":" -f 3 -)
	if [[ "$fare" -gt "$bal" ]]; then
	    name=$(echo $line | cut -d ":" -f 2 -)
	    echo "$name"
	fi
    fi
done<$FILE
