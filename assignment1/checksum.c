#include <stdio.h>

int main(void)
{
  while (!feof(stdin)) // Check whether end of file
    {
      int digit, checksum, index = 0, sum = 0;
      char num[100];

      int nread = fscanf(stdin, "%s", num); // Read in the ID
      if (nread <= 0)
	{
	  break;
	}

      checksum = num[9] - '0'; // Store the checksum

      while (index != 9)
	{
	  digit = num[index] - '0'; // Convert char to int
	  sum = sum + digit; // Add each digit
	  index++;
	}

      if ((sum % 10) == checksum) // Compare last sum digit to checksum
	{
	  printf("%s\n", num); // Print ID if valid
	}
    }
  return 0;
}
