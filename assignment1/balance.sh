#! /usr/bin/env bash

# Cut first column (ID) of file | Find valid cards |
# Find rows with the valid cards | Cut thrid column (balance) |
# Convert to row with spaces | Replace spaces with plus sign |
# Calculate sum

# your code here
cut -d ":" -f 1 $1 | ./checksum | fgrep -f - $1 |
cut -d ":" -f 3 | xargs | tr ' ' + | bc
