// fare calculator
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int convert(char num[]) // Convert char string to signed int
{
    char digit;
    int result = 0;
    int sign = 1;
    // Check for minus
    if (num[0] == '-')
      {
	num++;   // Skip over the minus.
	sign = -1;
      }

    while((digit = *num))   // Stop the loop if we are at the end of the string.
      {
	result *= 10; // Shift digit by one place
	result += digit - '0';
	num++;  // Go to next digit.
      }

    return result * sign;
}

int main(int argc, char **argv) {
  while (!feof(stdin)) // Check whether end of file
    {
      int fare;
      int x1, y1, x2, y2,
	u1, v1, u2, v2, a, b, c, d;

      if (argc > 5 || argc <= 1)
        {
          break;
        }

      int nread = fscanf(stdin, "%d", &u1); // Read in the first input u1
      if (nread <= 0)
	{
	  break;
	}

      a = convert(argv[1]);
      b = convert(argv[2]);
      c = convert(argv[3]);
      d = convert(argv[4]);

      //fscanf(stdin, "%d", &u1); // Already checked and assigned above
      fscanf(stdin, "%d", &v1);
      fscanf(stdin, "%d", &u2);
      fscanf(stdin, "%d", &v2);

      x1 = (a*u1 + b*v1);
      y1 = (c*u1 + d*v1);
      x2 = (a*u2 + b*v2);
      y2 = (c*u2 + d*v2);

      fare = abs(x1 - x2) + abs(y1 - y2); // Formula to calculate fare

      fprintf(stdout, "%d\n", fare);
    }
  return 0;
}
