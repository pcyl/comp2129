#! /usr/bin/env bash

clang -std=c11 -Wall checksum.c -o checksum.o
clang -std=c11 -Wall farecalc.c -o farecalc.o

cp balance.sh balance
cp outstanding.sh outstanding

chmod 700 checksum.o farecalc.o balance outstanding
