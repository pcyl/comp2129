	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_add_entry
	.align	4, 0x90
_add_entry:                             ## @add_entry
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
Ltmp3:
	.cfi_offset %rbx, -32
Ltmp4:
	.cfi_offset %r14, -24
	movq	%rdi, %r14
	movl	$1, %edi
	movl	$264, %esi              ## imm = 0x108
	callq	_calloc
	movq	%rax, %rbx
	movl	$264, %edx              ## imm = 0x108
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	___strcpy_chk
	movq	_g_nentries(%rip), %rax
	movq	_g_entries(%rip), %rcx
	movq	%rbx, (%rcx,%rax,8)
	incq	%rax
	movq	%rax, _g_nentries(%rip)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_find_entry
	.align	4, 0x90
_find_entry:                            ## @find_entry
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp5:
	.cfi_def_cfa_offset 16
Ltmp6:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp7:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	pushq	%rax
Ltmp8:
	.cfi_offset %rbx, -56
Ltmp9:
	.cfi_offset %r12, -48
Ltmp10:
	.cfi_offset %r13, -40
Ltmp11:
	.cfi_offset %r14, -32
Ltmp12:
	.cfi_offset %r15, -24
	movq	%rdi, %r14
	movq	_g_nentries(%rip), %r15
	xorl	%r13d, %r13d
	testq	%r15, %r15
	jle	LBB1_4
## BB#1:                                ## %.lr.ph
	movq	_g_entries(%rip), %r12
	xorl	%ebx, %ebx
	.align	4, 0x90
LBB1_3:                                 ## =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbx,8), %r13
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB1_4
## BB#2:                                ##   in Loop: Header=BB1_3 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	movl	$0, %r13d
	jl	LBB1_3
LBB1_4:                                 ## %._crit_edge
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_find_matrix
	.align	4, 0x90
_find_matrix:                           ## @find_matrix
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp13:
	.cfi_def_cfa_offset 16
Ltmp14:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp15:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	pushq	%rax
Ltmp16:
	.cfi_offset %rbx, -56
Ltmp17:
	.cfi_offset %r12, -48
Ltmp18:
	.cfi_offset %r13, -40
Ltmp19:
	.cfi_offset %r14, -32
Ltmp20:
	.cfi_offset %r15, -24
	movq	%rdi, %r15
	movq	_g_nentries(%rip), %r12
	xorl	%eax, %eax
	movq	%rax, -48(%rbp)         ## 8-byte Spill
	testq	%r12, %r12
	jle	LBB2_6
## BB#1:                                ## %.lr.ph.i
	movq	_g_entries(%rip), %r13
	xorl	%eax, %eax
	movq	%rax, -48(%rbp)         ## 8-byte Spill
	xorl	%r14d, %r14d
	.align	4, 0x90
LBB2_3:                                 ## =>This Inner Loop Header: Depth=1
	movq	(%r13,%r14,8), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB2_4
## BB#2:                                ##   in Loop: Header=BB2_3 Depth=1
	incq	%r14
	cmpq	%r12, %r14
	jl	LBB2_3
	jmp	LBB2_6
LBB2_4:                                 ## %find_entry.exit
	xorl	%eax, %eax
	movq	%rax, -48(%rbp)         ## 8-byte Spill
	testq	%rbx, %rbx
	je	LBB2_6
## BB#5:
	movq	256(%rbx), %rax
	movq	%rax, -48(%rbp)         ## 8-byte Spill
LBB2_6:                                 ## %find_entry.exit.thread
	movq	-48(%rbp), %rax         ## 8-byte Reload
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_release
	.align	4, 0x90
_release:                               ## @release
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp21:
	.cfi_def_cfa_offset 16
Ltmp22:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp23:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp24:
	.cfi_offset %rbx, -24
	movq	_g_entries(%rip), %rdi
	testq	%rdi, %rdi
	je	LBB3_6
## BB#1:                                ## %.preheader
	cmpq	$0, _g_nentries(%rip)
	jle	LBB3_5
## BB#2:                                ## %.lr.ph.preheader
	movq	(%rdi), %rax
	movq	256(%rax), %rdi
	callq	_free
	movq	_g_entries(%rip), %rax
	movq	(%rax), %rdi
	callq	_free
	movl	$1, %ebx
	cmpq	$2, _g_nentries(%rip)
	jl	LBB3_4
	.align	4, 0x90
LBB3_3:                                 ## %._crit_edge2
                                        ## =>This Inner Loop Header: Depth=1
	movq	_g_entries(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	movq	256(%rax), %rdi
	callq	_free
	movq	_g_entries(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	_free
	incq	%rbx
	cmpq	_g_nentries(%rip), %rbx
	jl	LBB3_3
LBB3_4:                                 ## %._crit_edge
	movq	_g_entries(%rip), %rdi
LBB3_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	_free                   ## TAILCALL
LBB3_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_define_settings
	.align	4, 0x90
_define_settings:                       ## @define_settings
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp25:
	.cfi_def_cfa_offset 16
Ltmp26:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp27:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
Ltmp28:
	.cfi_offset %rbx, -32
Ltmp29:
	.cfi_offset %r14, -24
	movq	%rsi, %r14
	cmpl	$3, %edi
	jne	LBB4_4
## BB#1:
	movq	8(%r14), %rdi
	callq	_atoll
	movq	%rax, %rbx
	movq	%rbx, _g_order(%rip)
	movq	16(%r14), %rdi
	callq	_atoll
	testq	%rbx, %rbx
	jle	LBB4_4
## BB#2:
	testq	%rax, %rax
	jle	LBB4_4
## BB#3:
	movq	%rax, %rdi
	callq	_set_nthreads
	movq	_g_order(%rip), %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	_set_dimensions         ## TAILCALL
LBB4_4:
	leaq	L_.str(%rip), %rdi
	callq	_puts
	leaq	L_.str1(%rip), %rdi
	callq	_puts
	movl	$1, %edi
	callq	_exit
	.cfi_endproc

	.globl	_command_bye
	.align	4, 0x90
_command_bye:                           ## @command_bye
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp30:
	.cfi_def_cfa_offset 16
Ltmp31:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp32:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp33:
	.cfi_offset %rbx, -24
	leaq	L_str(%rip), %rdi
	callq	_puts
	movq	_g_entries(%rip), %rdi
	testq	%rdi, %rdi
	je	LBB5_6
## BB#1:                                ## %.preheader.i
	cmpq	$0, _g_nentries(%rip)
	jle	LBB5_5
## BB#2:                                ## %.lr.ph.i.preheader
	movq	(%rdi), %rax
	movq	256(%rax), %rdi
	callq	_free
	movq	_g_entries(%rip), %rax
	movq	(%rax), %rdi
	callq	_free
	movl	$1, %ebx
	cmpq	$2, _g_nentries(%rip)
	jl	LBB5_4
	.align	4, 0x90
LBB5_3:                                 ## %._crit_edge2.i
                                        ## =>This Inner Loop Header: Depth=1
	movq	_g_entries(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	movq	256(%rax), %rdi
	callq	_free
	movq	_g_entries(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	_free
	incq	%rbx
	cmpq	_g_nentries(%rip), %rbx
	jl	LBB5_3
LBB5_4:                                 ## %._crit_edge.i
	movq	_g_entries(%rip), %rdi
LBB5_5:
	callq	_free
LBB5_6:                                 ## %release.exit
	xorl	%edi, %edi
	callq	_exit
	.cfi_endproc

	.globl	_command_help
	.align	4, 0x90
_command_help:                          ## @command_help
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp34:
	.cfi_def_cfa_offset 16
Ltmp35:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp36:
	.cfi_def_cfa_register %rbp
	leaq	L_.str4(%rip), %rdi
	leaq	L_.str3(%rip), %rsi
	xorl	%eax, %eax
	popq	%rbp
	jmp	_printf                 ## TAILCALL
	.cfi_endproc

	.globl	_command_set
	.align	4, 0x90
_command_set:                           ## @command_set
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp37:
	.cfi_def_cfa_offset 16
Ltmp38:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp39:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1304, %rsp             ## imm = 0x518
Ltmp40:
	.cfi_offset %rbx, -56
Ltmp41:
	.cfi_offset %r12, -48
Ltmp42:
	.cfi_offset %r13, -40
Ltmp43:
	.cfi_offset %r14, -32
Ltmp44:
	.cfi_offset %r15, -24
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
	movq	%rbx, -48(%rbp)
	leaq	-1328(%rbp), %rax
	movq	%rax, (%rsp)
	leaq	L_.str5(%rip), %rsi
	leaq	-304(%rbp), %rdx
	leaq	-560(%rbp), %rcx
	leaq	-816(%rbp), %r8
	leaq	-1072(%rbp), %r9
	xorl	%eax, %eax
	callq	_sscanf
	cmpl	$2, %eax
	jle	LBB7_99
## BB#1:
	xorl	%ecx, %ecx
	cmpl	$5, %eax
	je	LBB7_36
## BB#2:
	cmpl	$4, %eax
	jne	LBB7_3
## BB#6:
	leaq	L_.str8(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB7_7
## BB#8:
	leaq	L_.str9(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB7_9
## BB#10:
	leaq	L_.str10(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB7_11
## BB#20:
	leaq	L_.str12(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
	je	LBB7_21
## BB#28:
	leaq	L_.str13(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	jne	LBB7_99
## BB#29:
	movq	_g_nentries(%rip), %r15
	testq	%r15, %r15
	jle	LBB7_17
## BB#30:                               ## %.lr.ph.i.i20
	movq	_g_entries(%rip), %r13
	xorl	%ebx, %ebx
	leaq	-1072(%rbp), %r14
LBB7_32:                                ## =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbx,8), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB7_33
## BB#31:                               ##   in Loop: Header=BB7_32 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	jl	LBB7_32
	jmp	LBB7_17
LBB7_36:
	leaq	L_.str14(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB7_37
## BB#38:
	leaq	L_.str15(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB7_39
## BB#46:
	leaq	L_.str16(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB7_47
## BB#54:
	leaq	L_.str17(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB7_55
## BB#68:
	leaq	L_.str18(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB7_69
## BB#82:
	leaq	L_.str19(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
	jne	LBB7_99
## BB#83:
	movq	_g_nentries(%rip), %r15
	testq	%r15, %r15
	jle	LBB7_17
## BB#84:                               ## %.lr.ph.i.i55
	movq	_g_entries(%rip), %r13
	xorl	%ebx, %ebx
	leaq	-1072(%rbp), %r14
LBB7_86:                                ## =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbx,8), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB7_87
## BB#85:                               ##   in Loop: Header=BB7_86 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	jl	LBB7_86
	jmp	LBB7_17
LBB7_3:
	cmpl	$3, %eax
	jne	LBB7_91
## BB#4:
	leaq	L_.str7(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB7_5
LBB7_99:
	leaq	L_.str6(%rip), %rdi
	callq	_puts
	jmp	LBB7_100
LBB7_7:
	leaq	-1072(%rbp), %rdi
	callq	_atoll
	movl	%eax, %edi
	callq	_random_matrix
	jmp	LBB7_90
LBB7_37:
	leaq	-1072(%rbp), %rdi
	callq	_atoll
	movq	%rax, %r14
	leaq	-1328(%rbp), %rdi
	callq	_atoll
	movl	%r14d, %edi
	movl	%eax, %esi
	callq	_sequence_matrix
	jmp	LBB7_90
LBB7_9:
	leaq	-1072(%rbp), %rdi
	callq	_atoll
	movl	%eax, %edi
	callq	_uniform_matrix
	jmp	LBB7_90
LBB7_39:
	movq	_g_nentries(%rip), %r15
	testq	%r15, %r15
	jle	LBB7_17
## BB#40:                               ## %.lr.ph.i.i25
	movq	_g_entries(%rip), %r13
	xorl	%ebx, %ebx
	leaq	-1072(%rbp), %r14
	.align	4, 0x90
LBB7_42:                                ## =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbx,8), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB7_43
## BB#41:                               ##   in Loop: Header=BB7_42 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	jl	LBB7_42
	jmp	LBB7_17
LBB7_5:
	callq	_identity_matrix
LBB7_90:
	movq	%rax, %rcx
LBB7_91:
	movq	%rcx, -1336(%rbp)       ## 8-byte Spill
	movq	_g_nentries(%rip), %r13
	testq	%r13, %r13
	jle	LBB7_96
## BB#92:                               ## %.lr.ph.i
	movq	_g_entries(%rip), %r12
	xorl	%r14d, %r14d
	leaq	-560(%rbp), %r15
	.align	4, 0x90
LBB7_94:                                ## =>This Inner Loop Header: Depth=1
	movq	(%r12,%r14,8), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB7_95
## BB#93:                               ##   in Loop: Header=BB7_94 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	LBB7_94
	jmp	LBB7_96
LBB7_95:                                ## %find_entry.exit
	testq	%rbx, %rbx
	je	LBB7_96
## BB#97:
	movq	256(%rbx), %rdi
	callq	_free
	jmp	LBB7_98
LBB7_96:                                ## %find_entry.exit.thread
	movl	$1, %edi
	movl	$264, %esi              ## imm = 0x108
	callq	_calloc
	movq	%rax, %rbx
	leaq	-560(%rbp), %rsi
	movl	$264, %edx              ## imm = 0x108
	movq	%rbx, %rdi
	callq	___strcpy_chk
	movq	_g_nentries(%rip), %rax
	movq	_g_entries(%rip), %rcx
	movq	%rbx, (%rcx,%rax,8)
	incq	%rax
	movq	%rax, _g_nentries(%rip)
LBB7_98:
	movq	-1336(%rbp), %rax       ## 8-byte Reload
	movq	%rax, 256(%rbx)
	leaq	L_.str20(%rip), %rdi
	jmp	LBB7_18
LBB7_11:
	movq	_g_nentries(%rip), %r15
	testq	%r15, %r15
	jle	LBB7_17
## BB#12:                               ## %.lr.ph.i.i
	movq	_g_entries(%rip), %r13
	xorl	%ebx, %ebx
	leaq	-1072(%rbp), %r14
	.align	4, 0x90
LBB7_14:                                ## =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbx,8), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB7_15
## BB#13:                               ##   in Loop: Header=BB7_14 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	jl	LBB7_14
	jmp	LBB7_17
LBB7_47:
	movq	_g_nentries(%rip), %r15
	testq	%r15, %r15
	jle	LBB7_17
## BB#48:                               ## %.lr.ph.i.i30
	movq	_g_entries(%rip), %r13
	xorl	%ebx, %ebx
	leaq	-1072(%rbp), %r14
	.align	4, 0x90
LBB7_50:                                ## =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbx,8), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB7_51
## BB#49:                               ##   in Loop: Header=BB7_50 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	jl	LBB7_50
	jmp	LBB7_17
LBB7_21:
	movq	_g_nentries(%rip), %r15
	testq	%r15, %r15
	jle	LBB7_17
## BB#22:                               ## %.lr.ph.i.i15
	movq	_g_entries(%rip), %r13
	xorl	%ebx, %ebx
	leaq	-1072(%rbp), %r14
LBB7_24:                                ## =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbx,8), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB7_25
## BB#23:                               ##   in Loop: Header=BB7_24 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	jl	LBB7_24
	jmp	LBB7_17
LBB7_55:
	movq	_g_nentries(%rip), %r13
	testq	%r13, %r13
	jle	LBB7_17
## BB#56:                               ## %.lr.ph.i.i35
	movq	_g_entries(%rip), %r14
	xorl	%eax, %eax
	movq	%rax, -1336(%rbp)       ## 8-byte Spill
	leaq	-1072(%rbp), %r15
	xorl	%ebx, %ebx
LBB7_58:                                ## =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB7_59
## BB#57:                               ##   in Loop: Header=BB7_58 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jl	LBB7_58
	jmp	LBB7_61
LBB7_43:                                ## %find_entry.exit.i27
	testq	%r12, %r12
	je	LBB7_17
## BB#44:                               ## %find_matrix.exit29
	movq	256(%r12), %rbx
	testq	%rbx, %rbx
	je	LBB7_17
## BB#45:
	leaq	-1328(%rbp), %rdi
	callq	_atoll
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	_scalar_add
	jmp	LBB7_90
LBB7_69:
	movq	_g_nentries(%rip), %r13
	testq	%r13, %r13
	jle	LBB7_17
## BB#70:                               ## %.lr.ph.i.i45
	movq	_g_entries(%rip), %r14
	xorl	%eax, %eax
	movq	%rax, -1336(%rbp)       ## 8-byte Spill
	leaq	-1072(%rbp), %r15
	xorl	%ebx, %ebx
LBB7_72:                                ## =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB7_73
## BB#71:                               ##   in Loop: Header=BB7_72 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jl	LBB7_72
	jmp	LBB7_75
LBB7_15:                                ## %find_entry.exit.i
	testq	%r12, %r12
	je	LBB7_17
## BB#16:                               ## %find_matrix.exit
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	LBB7_17
## BB#19:
	callq	_cloned
	jmp	LBB7_90
LBB7_51:                                ## %find_entry.exit.i32
	testq	%r12, %r12
	je	LBB7_17
## BB#52:                               ## %find_matrix.exit34
	movq	256(%r12), %rbx
	testq	%rbx, %rbx
	je	LBB7_17
## BB#53:
	leaq	-1328(%rbp), %rdi
	callq	_atoll
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	_scalar_mul
	jmp	LBB7_90
LBB7_25:                                ## %find_entry.exit.i17
	testq	%r12, %r12
	je	LBB7_17
## BB#26:                               ## %find_matrix.exit19
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	LBB7_17
## BB#27:
	callq	_reversed
	jmp	LBB7_90
LBB7_59:                                ## %find_entry.exit.i37
	xorl	%eax, %eax
	movq	%rax, -1336(%rbp)       ## 8-byte Spill
	testq	%r12, %r12
	je	LBB7_61
## BB#60:
	movq	256(%r12), %rax
	movq	%rax, -1336(%rbp)       ## 8-byte Spill
LBB7_61:                                ## %.lr.ph.i.i40
	xorl	%ebx, %ebx
	leaq	-1328(%rbp), %r15
LBB7_63:                                ## =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB7_64
## BB#62:                               ##   in Loop: Header=BB7_63 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jl	LBB7_63
	jmp	LBB7_17
LBB7_64:                                ## %find_entry.exit.i42
	testq	%r12, %r12
	je	LBB7_17
## BB#65:                               ## %find_matrix.exit44
	cmpq	$0, -1336(%rbp)         ## 8-byte Folded Reload
	je	LBB7_17
## BB#66:                               ## %find_matrix.exit44
	movq	256(%r12), %rsi
	testq	%rsi, %rsi
	je	LBB7_17
## BB#67:
	movq	-1336(%rbp), %rdi       ## 8-byte Reload
	callq	_matrix_add
	jmp	LBB7_90
LBB7_33:                                ## %find_entry.exit.i22
	testq	%r12, %r12
	je	LBB7_17
## BB#34:                               ## %find_matrix.exit24
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	LBB7_17
## BB#35:
	callq	_transposed
	jmp	LBB7_90
LBB7_73:                                ## %find_entry.exit.i47
	xorl	%eax, %eax
	movq	%rax, -1336(%rbp)       ## 8-byte Spill
	testq	%r12, %r12
	je	LBB7_75
## BB#74:
	movq	256(%r12), %rax
	movq	%rax, -1336(%rbp)       ## 8-byte Spill
LBB7_75:                                ## %.lr.ph.i.i50
	xorl	%ebx, %ebx
	leaq	-1328(%rbp), %r15
LBB7_77:                                ## =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB7_78
## BB#76:                               ##   in Loop: Header=BB7_77 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jl	LBB7_77
	jmp	LBB7_17
LBB7_78:                                ## %find_entry.exit.i52
	testq	%r12, %r12
	je	LBB7_17
## BB#79:                               ## %find_matrix.exit54
	cmpq	$0, -1336(%rbp)         ## 8-byte Folded Reload
	je	LBB7_17
## BB#80:                               ## %find_matrix.exit54
	movq	256(%r12), %rsi
	testq	%rsi, %rsi
	je	LBB7_17
## BB#81:
	movq	-1336(%rbp), %rdi       ## 8-byte Reload
	callq	_matrix_mul
	jmp	LBB7_90
LBB7_87:                                ## %find_entry.exit.i57
	testq	%r12, %r12
	je	LBB7_17
## BB#88:                               ## %find_matrix.exit59
	movq	256(%r12), %rbx
	testq	%rbx, %rbx
	je	LBB7_17
## BB#89:
	leaq	-1328(%rbp), %rdi
	callq	_atoll
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	_matrix_pow
	jmp	LBB7_90
LBB7_17:                                ## %find_matrix.exit.thread
	leaq	L_.str11(%rip), %rdi
LBB7_18:
	callq	_puts
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
LBB7_100:
	cmpq	-48(%rbp), %rbx
	jne	LBB7_102
## BB#101:
	addq	$1304, %rsp             ## imm = 0x518
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB7_102:
	callq	___stack_chk_fail
	.cfi_endproc

	.globl	_command_show
	.align	4, 0x90
_command_show:                          ## @command_show
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp45:
	.cfi_def_cfa_offset 16
Ltmp46:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp47:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1304, %rsp             ## imm = 0x518
Ltmp48:
	.cfi_offset %rbx, -56
Ltmp49:
	.cfi_offset %r12, -48
Ltmp50:
	.cfi_offset %r13, -40
Ltmp51:
	.cfi_offset %r14, -32
Ltmp52:
	.cfi_offset %r15, -24
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
	movq	%rbx, -48(%rbp)
	leaq	-1328(%rbp), %rax
	movq	%rax, (%rsp)
	leaq	L_.str21(%rip), %rsi
	leaq	-304(%rbp), %rdx
	leaq	-560(%rbp), %rcx
	leaq	-816(%rbp), %r8
	leaq	-1072(%rbp), %r9
	xorl	%eax, %eax
	callq	_sscanf
	cmpl	$2, %eax
	jl	LBB8_20
## BB#1:
	movl	%eax, -1332(%rbp)       ## 4-byte Spill
	movq	_g_nentries(%rip), %r15
	testq	%r15, %r15
	jle	LBB8_7
## BB#2:                                ## %.lr.ph.i.i
	movq	_g_entries(%rip), %r13
	xorl	%r12d, %r12d
	leaq	-560(%rbp), %r14
	.align	4, 0x90
LBB8_4:                                 ## =>This Inner Loop Header: Depth=1
	movq	(%r13,%r12,8), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB8_5
## BB#3:                                ##   in Loop: Header=BB8_4 Depth=1
	incq	%r12
	cmpq	%r15, %r12
	jl	LBB8_4
	jmp	LBB8_7
LBB8_5:                                 ## %find_entry.exit.i
	testq	%rbx, %rbx
	je	LBB8_7
## BB#6:                                ## %find_matrix.exit
	movq	256(%rbx), %r15
	testq	%r15, %r15
	je	LBB8_7
## BB#8:
	movl	-1332(%rbp), %r13d      ## 4-byte Reload
	cmpl	$2, %r13d
	jne	LBB8_10
## BB#9:
	movq	%r15, %rdi
	callq	_display
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
	jmp	LBB8_21
LBB8_7:                                 ## %find_matrix.exit.thread
	leaq	L_.str11(%rip), %rdi
	callq	_puts
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
	jmp	LBB8_21
LBB8_10:
	leaq	-1072(%rbp), %rdi
	callq	_atoll
	movq	%rax, %r14
	decl	%r14d
	movq	_g_order(%rip), %r12
	cmpq	%r12, %r14
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
	jge	LBB8_20
## BB#11:
	cmpl	$5, %r13d
	jne	LBB8_12
## BB#17:
	leaq	L_.str24(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	jne	LBB8_21
## BB#18:
	leaq	-1328(%rbp), %rdi
	callq	_atoll
	movl	$4294967295, %ecx       ## imm = 0xFFFFFFFF
	addl	%ecx, %eax
	andq	%rcx, %rax
	cmpq	%r12, %rax
	jge	LBB8_20
## BB#19:
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	_display_element
	jmp	LBB8_21
LBB8_20:
	leaq	L_.str6(%rip), %rdi
	callq	_puts
LBB8_21:                                ## %.critedge.thread
	cmpq	-48(%rbp), %rbx
	jne	LBB8_23
## BB#22:                               ## %.critedge.thread
	addq	$1304, %rsp             ## imm = 0x518
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB8_12:
	cmpl	$4, %r13d
	jne	LBB8_21
## BB#13:
	leaq	L_.str22(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB8_14
## BB#15:
	leaq	L_.str23(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	jne	LBB8_21
## BB#16:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	_display_column
	jmp	LBB8_21
LBB8_14:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	_display_row
	jmp	LBB8_21
LBB8_23:                                ## %.critedge.thread
	callq	___stack_chk_fail
	.cfi_endproc

	.globl	_command_compute
	.align	4, 0x90
_command_compute:                       ## @command_compute
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp53:
	.cfi_def_cfa_offset 16
Ltmp54:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp55:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1032, %rsp             ## imm = 0x408
Ltmp56:
	.cfi_offset %rbx, -56
Ltmp57:
	.cfi_offset %r12, -48
Ltmp58:
	.cfi_offset %r13, -40
Ltmp59:
	.cfi_offset %r14, -32
Ltmp60:
	.cfi_offset %r15, -24
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
	movq	%rbx, -48(%rbp)
	leaq	L_.str25(%rip), %rsi
	leaq	-304(%rbp), %rdx
	leaq	-816(%rbp), %rcx
	leaq	-560(%rbp), %r8
	leaq	-1072(%rbp), %r9
	xorl	%eax, %eax
	callq	_sscanf
	cmpl	$3, %eax
	jl	LBB9_21
## BB#1:
	movq	_g_nentries(%rip), %r12
	testq	%r12, %r12
	jle	LBB9_7
## BB#2:                                ## %.lr.ph.i.i
	movq	_g_entries(%rip), %r13
	xorl	%r15d, %r15d
	leaq	-560(%rbp), %r14
	.align	4, 0x90
LBB9_4:                                 ## =>This Inner Loop Header: Depth=1
	movq	(%r13,%r15,8), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB9_5
## BB#3:                                ##   in Loop: Header=BB9_4 Depth=1
	incq	%r15
	cmpq	%r12, %r15
	jl	LBB9_4
	jmp	LBB9_7
LBB9_5:                                 ## %find_entry.exit.i
	testq	%rbx, %rbx
	je	LBB9_7
## BB#6:                                ## %find_matrix.exit
	movq	256(%rbx), %r14
	testq	%r14, %r14
	je	LBB9_7
## BB#8:
	leaq	L_.str26(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB9_9
## BB#11:
	leaq	L_.str27(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB9_12
## BB#13:
	leaq	L_.str28(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
	je	LBB9_14
## BB#15:
	leaq	L_.str29(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB9_16
## BB#17:
	leaq	L_.str30(%rip), %rsi
	leaq	-816(%rbp), %rdi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB9_18
LBB9_21:
	leaq	L_.str6(%rip), %rdi
	callq	_puts
	jmp	LBB9_22
LBB9_7:                                 ## %find_matrix.exit.thread
	leaq	L_.str11(%rip), %rdi
	callq	_puts
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
LBB9_22:
	cmpq	-48(%rbp), %rbx
	jne	LBB9_24
## BB#23:
	addq	$1032, %rsp             ## imm = 0x408
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB9_9:
	movq	%r14, %rdi
	callq	_get_sum
	jmp	LBB9_10
LBB9_12:
	movq	%r14, %rdi
	callq	_get_trace
LBB9_10:
	movl	%eax, %esi
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
	jmp	LBB9_20
LBB9_14:
	movq	%r14, %rdi
	callq	_get_minimum
	jmp	LBB9_19
LBB9_16:
	movq	%r14, %rdi
	callq	_get_maximum
	jmp	LBB9_19
LBB9_18:
	leaq	-1072(%rbp), %rdi
	callq	_atoll
	movq	%r14, %rdi
	movl	%eax, %esi
	callq	_get_frequency
LBB9_19:
	movl	%eax, %esi
LBB9_20:
	leaq	L_.str31(%rip), %rdi
	xorl	%eax, %eax
	callq	_printf
	jmp	LBB9_22
LBB9_24:
	callq	___stack_chk_fail
	.cfi_endproc

	.globl	_compute_engine
	.align	4, 0x90
_compute_engine:                        ## @compute_engine
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp61:
	.cfi_def_cfa_offset 16
Ltmp62:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp63:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$520, %rsp              ## imm = 0x208
Ltmp64:
	.cfi_offset %rbx, -56
Ltmp65:
	.cfi_offset %r12, -48
Ltmp66:
	.cfi_offset %r13, -40
Ltmp67:
	.cfi_offset %r14, -32
Ltmp68:
	.cfi_offset %r15, -24
	movl	$512, %edi              ## imm = 0x200
	movl	$8, %esi
	callq	_calloc
	movq	%rax, _g_entries(%rip)
	leaq	L_.str32(%rip), %rdi
	xorl	%eax, %eax
	callq	_printf
	movq	___stdinp@GOTPCREL(%rip), %r13
	movq	(%r13), %rdx
	leaq	-304(%rbp), %rdi
	movl	$256, %esi              ## imm = 0x100
	callq	_fgets
	testq	%rax, %rax
	je	LBB10_14
## BB#1:
	leaq	L_.str4(%rip), %r14
	leaq	-304(%rbp), %r15
	leaq	-560(%rbp), %rbx
	leaq	L_.str32(%rip), %r12
	.align	4, 0x90
LBB10_2:                                ## =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	_sscanf
	cmpl	$1, %eax
	jne	LBB10_13
## BB#3:                                ##   in Loop: Header=BB10_2 Depth=1
	movq	%rbx, %rdi
	leaq	L_str(%rip), %rsi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB10_14
## BB#4:                                ##   in Loop: Header=BB10_2 Depth=1
	movq	%rbx, %rdi
	leaq	L_.str35(%rip), %rsi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB10_5
## BB#6:                                ##   in Loop: Header=BB10_2 Depth=1
	movq	%rbx, %rdi
	leaq	L_.str36(%rip), %rsi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB10_7
## BB#8:                                ##   in Loop: Header=BB10_2 Depth=1
	movq	%rbx, %rdi
	leaq	L_.str37(%rip), %rsi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB10_9
## BB#10:                               ##   in Loop: Header=BB10_2 Depth=1
	movq	%rbx, %rdi
	leaq	L_.str38(%rip), %rsi
	callq	_strcasecmp
	testl	%eax, %eax
	je	LBB10_11
## BB#12:                               ##   in Loop: Header=BB10_2 Depth=1
	leaq	L_.str39(%rip), %rdi
	callq	_puts
	jmp	LBB10_13
LBB10_5:                                ##   in Loop: Header=BB10_2 Depth=1
	xorl	%eax, %eax
	movq	%r14, %rdi
	leaq	L_.str3(%rip), %rsi
	callq	_printf
	jmp	LBB10_13
LBB10_7:                                ##   in Loop: Header=BB10_2 Depth=1
	movq	%r15, %rdi
	callq	_command_set
	jmp	LBB10_13
LBB10_9:                                ##   in Loop: Header=BB10_2 Depth=1
	movq	%r15, %rdi
	callq	_command_show
	jmp	LBB10_13
LBB10_11:                               ##   in Loop: Header=BB10_2 Depth=1
	movq	%r15, %rdi
	callq	_command_compute
	.align	4, 0x90
LBB10_13:                               ##   in Loop: Header=BB10_2 Depth=1
	movl	$10, %edi
	callq	_putchar
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	_printf
	movq	(%r13), %rdx
	movl	$256, %esi              ## imm = 0x100
	movq	%r15, %rdi
	callq	_fgets
	testq	%rax, %rax
	jne	LBB10_2
LBB10_14:
	callq	_command_bye
	.cfi_endproc

	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp69:
	.cfi_def_cfa_offset 16
Ltmp70:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp71:
	.cfi_def_cfa_register %rbp
	callq	_define_settings
	callq	_compute_engine
	.cfi_endproc

.zerofill __DATA,__bss,_g_nentries,8,3  ## @g_nentries
.zerofill __DATA,__bss,_g_entries,8,3   ## @g_entries
.zerofill __DATA,__bss,_g_order,8,3     ## @g_order
	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"Invalid command line arguments"

L_.str1:                                ## @.str1
	.asciz	"Usage: matrix <width> <# threads>"

L_.str3:                                ## @.str3
	.asciz	"BYE\nHELP\n\nSET <key> = identity\nSET <key> = random <seed>\nSET <key> = uniform <value>\nSET <key> = sequence <start> <step>\n\nSET <key> = cloned <matrix>\nSET <key> = reversed <matrix>\nSET <key> = transposed <matrix>\n\nSET <key> = matrix#add <matrix a> <matrix b>\nSET <key> = matrix#mul <matrix a> <matrix b>\nSET <key> = matrix#pow <matrix> <exponent>\nSET <key> = scalar#add <matrix> <scalar>\nSET <key> = scalar#mul <matrix> <scalar>\n\nSHOW <key>\nSHOW <key> row <number>\nSHOW <key> column <number>\nSHOW <key> element <row> <column>\n\nCOMPUTE sum <key>\nCOMPUTE trace <key>\nCOMPUTE minimum <key>\nCOMPUTE maximum <key>\nCOMPUTE frequency <key> <value>\n"

L_.str4:                                ## @.str4
	.asciz	"%s"

L_.str5:                                ## @.str5
	.asciz	"%s %s = %s %s %s"

L_.str6:                                ## @.str6
	.asciz	"invalid arguments"

L_.str7:                                ## @.str7
	.asciz	"identity"

L_.str8:                                ## @.str8
	.asciz	"random"

L_.str9:                                ## @.str9
	.asciz	"uniform"

L_.str10:                               ## @.str10
	.asciz	"cloned"

L_.str11:                               ## @.str11
	.asciz	"no such matrix"

L_.str12:                               ## @.str12
	.asciz	"reversed"

L_.str13:                               ## @.str13
	.asciz	"transposed"

L_.str14:                               ## @.str14
	.asciz	"sequence"

L_.str15:                               ## @.str15
	.asciz	"scalar#add"

L_.str16:                               ## @.str16
	.asciz	"scalar#mul"

L_.str17:                               ## @.str17
	.asciz	"matrix#add"

L_.str18:                               ## @.str18
	.asciz	"matrix#mul"

L_.str19:                               ## @.str19
	.asciz	"matrix#pow"

L_.str20:                               ## @.str20
	.asciz	"ok"

L_.str21:                               ## @.str21
	.asciz	"%s %s %s %s %s"

L_.str22:                               ## @.str22
	.asciz	"row"

L_.str23:                               ## @.str23
	.asciz	"column"

L_.str24:                               ## @.str24
	.asciz	"element"

L_.str25:                               ## @.str25
	.asciz	"%s %s %s %s"

L_.str26:                               ## @.str26
	.asciz	"sum"

L_.str27:                               ## @.str27
	.asciz	"trace"

L_.str28:                               ## @.str28
	.asciz	"minimum"

L_.str29:                               ## @.str29
	.asciz	"maximum"

L_.str30:                               ## @.str30
	.asciz	"frequency"

L_.str31:                               ## @.str31
	.asciz	"%u\n"

L_.str32:                               ## @.str32
	.asciz	"> "

L_.str35:                               ## @.str35
	.asciz	"help"

L_.str36:                               ## @.str36
	.asciz	"set"

L_.str37:                               ## @.str37
	.asciz	"show"

L_.str38:                               ## @.str38
	.asciz	"compute"

L_.str39:                               ## @.str39
	.asciz	"invalid command"

L_str:                                  ## @str
	.asciz	"bye"


.subsections_via_symbols
