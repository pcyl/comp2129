#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>
#include <inttypes.h>

#include "matrix.h"

static uint32_t g_seed = 0;

static ssize_t g_width = 0;
static ssize_t g_height = 0;
static ssize_t g_elements = 0;

static ssize_t g_nthreads = 1;

////////////////////////////////
///     UTILITY FUNCTIONS    ///
////////////////////////////////

/**
 * Returns pseudorandom number determined by the seed
 */
uint32_t fast_rand(void) {

    g_seed = (214013 * g_seed + 2531011);
    return (g_seed >> 16) & 0x7FFF;
}

/**
 * Sets the seed used when generating pseudorandom numbers
 */
void set_seed(uint32_t seed) {

    g_seed = seed;
}

/**
 * Sets the number of threads available
 */
void set_nthreads(ssize_t count) {

    g_nthreads = count;
}

/**
 * Sets the dimensions of the matrix
 */
void set_dimensions(ssize_t order) {

    g_width = order;
    g_height = order;

    g_elements = g_width * g_height;
}

/**
 * Displays given matrix
 */
void display(const uint32_t* matrix) {

    for (ssize_t y = 0; y < g_height; ++y) {
        for (ssize_t x = 0; x < g_width; ++x) {
            if (x > 0) printf(" ");
            printf("%" PRIu32 " ", matrix[y * g_width + x]);
        }

        printf("\n");
    }
}

/**
 * Displays given matrix row
 */
void display_row(const uint32_t* matrix, ssize_t row) {

    for (ssize_t x = 0; x < g_width; ++x) {
        if (x > 0) printf(" ");
        printf("%" PRIu32, matrix[row * g_width + x]);
    }

    printf("\n");
}

/**
 * Displays given matrix column
 */
void display_column(const uint32_t* matrix, ssize_t column) {

    for (ssize_t i = 0; i < g_height; ++i) {
        printf("%" PRIu32 "\n", matrix[i * g_width + column]);
    }
}

/**
 * Displays the value stored at the given element index
 */
void display_element(const uint32_t* matrix, ssize_t row, ssize_t column) {

    printf("%" PRIu32 "\n", matrix[row * g_width + column]);
}

////////////////////////////////
///     THREAD FUNCTIONS     ///
////////////////////////////////


struct argument
{
    const uint32_t* matA; // 8 bytes
    // const uint32_t* __attribute__((aligned(64))) matA;
    const uint32_t* matB; // 8 bytes
    // const uint32_t* __attribute__((aligned(64))) matB;
    uint32_t* result; // 8 bytes
    // uint32_t* __attribute__((aligned(64))) result;
    uint32_t scalar; // 8 bytes
    // uint32_t __attribute__((aligned(64))) scalar;
    size_t tid; // 4 bytes
    char padding[16];
    // size_t __attribute__((aligned(64))) tid;
};

/**
 * Multiplies two matrices together
 */
void *MatrixMul (void * args)
{
    int thread_dimension = g_width/g_nthreads;
    struct argument arg = *(struct argument *)args;

    const uint32_t* matA = arg.matA;
    const uint32_t* matB = arg.matB;
    uint32_t* result = new_matrix();
    size_t tid = arg.tid;
    // sum the values from start to end
    const size_t start = tid * thread_dimension;
    const size_t end =
        tid == g_nthreads - 1 ? g_width : (tid + 1) * thread_dimension;

    for (size_t y = start; y < end; ++y) {
        for (size_t k = 0; k < g_width; ++k) {
            for (size_t x = 0; x < g_width; ++x) {
                result[y * g_width + x] += matA[y * g_width + k] * matB[k * g_width + x];
            }
        }
    }

    arg.result = result;
    return NULL;
}

/**
 * Adds a scalar to matrix
 */
void *ScalarAdd (void * args)
{
    int thread_dimension = g_width/g_nthreads;
    struct argument arg = *(struct argument *)args;

    const uint32_t* matB = arg.matB;
    uint32_t* result = new_matrix();
    uint32_t scalar = arg.scalar;
    size_t tid = arg.tid;
    // sum the values from start to end
    const size_t start = tid * thread_dimension;
    const size_t end =
        tid == g_nthreads - 1 ? g_width : (tid + 1) * thread_dimension;

    for (size_t y = start; y < end; ++y) {
        for (size_t k = 0; k < g_width; ++k) {
            result[y * g_width + k] += matB[y * g_width + k] + scalar;
        }
    }

    arg.result = result;
    return NULL;
}

/**
 * Multiplies a scalar to matrix
 */
void *ScalarMul (void * args)
{
    int thread_dimension = g_width/g_nthreads;
    struct argument arg = *(struct argument *)args;

    const uint32_t* matB = arg.matB;
    uint32_t* result = new_matrix();
    uint32_t scalar = arg.scalar;
    size_t tid = arg.tid;

    // sum the values from start to end
    const size_t start = tid * thread_dimension;
    const size_t end =
        tid == g_nthreads - 1 ? g_width : (tid + 1) * thread_dimension;

    for (size_t y = start; y < end; ++y) {
        for (size_t k = 0; k < g_width; ++k) {
            result[y * g_width + k] += matB[y * g_width + k] * scalar;
        }
    }

    arg.result = result;
    return NULL;
}

/**
 * Get frequency of value
 */
void *GetFreq (void * args)
{
    int thread_dimension = g_width/g_nthreads;
    struct argument arg = *(struct argument *)args;

    // sum the values from start to end
    const size_t start = arg.tid * thread_dimension;
    const size_t end =
        arg.tid == g_nthreads - 1 ? g_width : (arg.tid + 1) * thread_dimension;

    for (size_t y = start; y < end; ++y) {
        for (size_t k = 0; k < g_width; ++k) {
            if (arg.matB[y * g_width + k] == arg.scalar) {
                arg.result[arg.tid]++;
            }
        }
    }
    return NULL;
}

void *GetMax (void * args)
{
    int thread_dimension = g_width/g_nthreads;
    struct argument arg = *(struct argument *)args;

    // sum the values from start to end
    const size_t start = arg.tid * thread_dimension;
    const size_t end =
        arg.tid == g_nthreads - 1 ? g_width : (arg.tid + 1) * thread_dimension;

    arg.result[arg.tid] = arg.matB[start * g_width + 0];
    for (size_t y = start; y < end; ++y) {
        for (size_t k = 0; k < g_width; ++k) {
        if (arg.matB[y * g_width + k] > arg.result[arg.tid]) {
            arg.result[arg.tid] = arg.matB[y * g_width + y];
        }
    }
    }
    return NULL;
}

void *GetTrace (void * args)
{
    int thread_dimension = g_width/g_nthreads;
    struct argument arg = *(struct argument *)args;

    // sum the values from start to end
    const size_t start = arg.tid * thread_dimension;
    const size_t end =
        arg.tid == g_nthreads - 1 ? g_width : (arg.tid + 1) * thread_dimension;

    for (size_t y = start; y < end; ++y) {
            arg.result[arg.tid] += arg.matB[y * g_width + y];
    }
    return NULL;
}

////////////////////////////////
///   MATRIX INITALISATIONS  ///
////////////////////////////////

/**
 * Returns new matrix with all elements set to zero
 */
uint32_t* new_matrix(void) {

    return calloc(g_elements, sizeof(uint32_t));
}

/**
 * Returns new identity matrix
 */
uint32_t* identity_matrix(void) {

    uint32_t* matrix = new_matrix();

    for (ssize_t i = 0; i < g_width; ++i) {
        matrix[i * g_width + i] = 1;
    }

    return matrix;
}

/**
 * Returns new matrix with elements generated at random using given seed
 */
uint32_t* random_matrix(uint32_t seed) {

    uint32_t* matrix = new_matrix();

    set_seed(seed);

    for (ssize_t i = 0; i < g_elements; ++i) {
        matrix[i] = fast_rand();
    }

    return matrix;
}

/**
 * Returns new matrix with all elements set to given value
 */
uint32_t* uniform_matrix(uint32_t value) {

    uint32_t* matrix = new_matrix();

    for (ssize_t i = 0; i < g_elements; ++i) {
        matrix[i] = value;
    }

    return matrix;
}

/**
 * Returns new matrix with elements in sequence from given start and step
 */
uint32_t* sequence_matrix(uint32_t start, uint32_t step) {

    uint32_t* matrix = new_matrix();
    uint32_t current = start;

    for (ssize_t i = 0; i < g_elements; ++i) {
        matrix[i] = current;
        current += step;
    }

    return matrix;
}

////////////////////////////////
///     MATRIX OPERATIONS    ///
////////////////////////////////

/**
 * Returns new matrix with elements cloned from given matrix
 */
uint32_t* cloned(const uint32_t* matrix) {

    uint32_t* result = new_matrix();

    for (ssize_t i = 0; i < g_elements; ++i) {
        result[i] = matrix[i];
    }

    return result;
}

/**
 * Returns new matrix with elements ordered in reverse
 */
uint32_t* reversed(const uint32_t* matrix) {

    uint32_t* result = new_matrix();

    for (ssize_t i = 0; i < g_elements; ++i) {
        result[i] = matrix[g_elements - 1 - i];
    }

    return result;
}

/**
 * Returns new transposed matrix
 */
uint32_t* transposed(const uint32_t* matrix) {

    uint32_t* result = new_matrix();

    for (ssize_t y = 0; y < g_height; ++y) {
        for (ssize_t x = 0; x < g_width; ++x) {
            result[x * g_width + y] = matrix[y * g_width + x];
        }
    }

    return result;
}

/**
 * Returns new matrix with scalar added to each element
 */
uint32_t* scalar_add(const uint32_t* matrix, uint32_t scalar) {

    uint32_t* result = new_matrix();

    /*
        to do

        1 0        2 1
        0 1 + 1 => 1 2

        1 2        5 6
        3 4 + 4 => 7 8
    */

    // for (ssize_t i = 0; i < g_elements; i++) {
    //     result[i] = matrix[i] + scalar;
    // }
    //
    // return result;
    pthread_t thread_ids[g_nthreads];

    struct argument *args = malloc(sizeof(struct argument) * g_nthreads);

    for (size_t i = 0; i < g_nthreads; ++i) {
        args[i] = (struct argument) {
            .matB = matrix,
            .result = result,
            .scalar = scalar,
            .tid = i,
        };
    }

    // launch
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_create(thread_ids + i, NULL, ScalarAdd, args + i) != 0) {
            perror("pthread_create failed");
            exit(1);
        }
    }

    // wait for the all the threads to finish
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_join(thread_ids[i], NULL) != 0) {
            perror("pthread_join failed");
            exit(1);
        }
    }

    free(args);
    return result;
}

/**
 * Returns new matrix with scalar multiplied to each element
 */
uint32_t* scalar_mul(const uint32_t* matrix, uint32_t scalar) {

    uint32_t* result = new_matrix();

    /*
        to do

        1 0        2 0
        0 1 x 2 => 0 2

        1 2        2 4
        3 4 x 2 => 6 8
    */

    // for (ssize_t i = 0; i < g_elements; i++) {
    //     result[i] = matrix[i] * scalar;
    // }
    //
    // return result;

    pthread_t thread_ids[g_nthreads];

    struct argument *args = malloc(sizeof(struct argument) * g_nthreads);

    for (size_t i = 0; i < g_nthreads; ++i) {
        args[i] = (struct argument) {
            .matB = matrix,
            .result = result,
            .scalar = scalar,
            .tid = i,
        };
    }

    // launch
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_create(thread_ids + i, NULL, ScalarMul, args + i) != 0) {
            perror("pthread_create failed");
            exit(1);
        }
    }

    // wait for the all the threads to finish
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_join(thread_ids[i], NULL) != 0) {
            perror("pthread_join failed");
            exit(1);
        }
    }

    free(args);
    return result;
}

/**
 * Returns new matrix with elements added at the same index
 */
uint32_t* matrix_add(const uint32_t* matrix_a, const uint32_t* matrix_b) {

    uint32_t* result = new_matrix();

    /*
        to do

        1 0   0 1    1 1
        0 1 + 1 0 => 1 1

        1 2   4 4    5 6
        3 4 + 4 4 => 7 8
    */

    for (ssize_t i = 0; i < g_elements; i++) {
        result[i] = matrix_a[i] + matrix_b[i];
    }

    return result;
}

/**
 * Returns new matrix, multiplying the two argument together
 */
uint32_t* matrix_mul(const uint32_t* matrix_a, const uint32_t* matrix_b) {

    uint32_t* result = new_matrix();

    /*
        to do

        1 2   1 0    1 2
        3 4 x 0 1 => 3 4

        1 2   5 6    19 22
        3 4 x 7 8 => 43 50
    */

    // for (ssize_t i = 0; i < g_width; i++) {
    //     for (ssize_t j = 0; j < g_height; j++) {
    //         uint32_t sum = 0;
    //         for (ssize_t k = 0; k < g_width; k++) {
    //             sum += matrix_a[i * g_width + k] * matrix_b[k * g_width + j];
    //         }
    //
    //         result[i * g_width + j] = sum;
    //     }
    // }
    //
    // return result;
    pthread_t thread_ids[g_nthreads];

    struct argument *args = malloc(sizeof(struct argument) * g_nthreads);

    for (size_t i = 0; i < g_nthreads; ++i) {
        args[i] = (struct argument) {
            .matA = matrix_a,
            .matB = matrix_b,
            .result = result,
            .tid = i,
        };
    }

    // launch
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_create(thread_ids + i, NULL, MatrixMul, args + i) != 0) {
            perror("pthread_create failed");
            exit(1);
        }
    }

    // wait for the all the threads to finish
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_join(thread_ids[i], NULL) != 0) {
            perror("pthread_join failed");
            exit(1);
        }
    }

    free(args);
    return result;
}

/**
 * Returns new matrix, powering the matrix to the exponent
 */
uint32_t* matrix_pow(const uint32_t* matrix, uint32_t exponent) {

    // uint32_t* result = new_matrix();
    uint32_t* result = identity_matrix();

    /*
        to do

        1 2        1 0
        3 4 ^ 0 => 0 1

        1 2        1 2
        3 4 ^ 1 => 3 4

        1 2        199 290
        3 4 ^ 4 => 435 634
    */

    // if (exponent == 0) return identity_matrix();
    // if (exponent == 1) return cloned(matrix);
    //
    // result = cloned(matrix);
    //
    // for (uint32_t i = 1; i < exponent; i++)
    //     result = matrix_mul(matrix, result);
    //
    // return result;

    if (exponent == 0)
        return result;

    if (exponent == 2)
        return matrix_mul(matrix, matrix);

    if (exponent == 1)
        return cloned(matrix);

    if (exponent % 2 == 1){
        return matrix_mul(matrix, matrix_pow(matrix,exponent-1));
    }
    else{
        result = matrix_pow(matrix,exponent/2);
        return matrix_mul(result, result);
    }

  return result;
}

////////////////////////////////
///       COMPUTATIONS       ///
////////////////////////////////

/**
 * Returns the sum of all elements
 */
uint32_t get_sum(const uint32_t* matrix) {

    /*
        to do

        1 2
        2 1 => 6

        1 1
        1 1 => 4
    */

    uint32_t sum = matrix[g_elements - 1];

    if (g_width == 1) { return sum;}

    for (ssize_t i = g_elements - 2; i >= 0; ) {
        sum += matrix[i];
        --i;
        sum += matrix[i];
        --i;
        sum += matrix[i];
        --i;
        sum += matrix[i];
        --i;
    }

    return sum;
}

/**
 * Returns the trace of the matrix
 */
uint32_t get_trace(const uint32_t* matrix) {

    /*
        to do

        1 0
        0 1 => 2

        2 1
        1 2 => 4
    */

    uint32_t trace = 0;

    if (g_height == 1) { return trace;}

    for (ssize_t i = 0; i < g_height; ) {
        trace += matrix[i * g_width + i];
        ++i;
        trace += matrix[i * g_width + i];
        ++i;
    }

    return trace;
}

/**
 * Returns the smallest value in the matrix
 */
uint32_t get_minimum(const uint32_t* matrix) {

    /*
        to do

        1 2
        3 4 => 1

        4 3
        2 1 => 1
    */

    uint32_t min = matrix[g_elements - 1];

    if (g_width ==  1) {return min;}

    for (ssize_t i = g_elements - 1; i >= 0;) {
        if (matrix[i] < min)
        {
            min = matrix[i];
        }
        --i;
        if (matrix[i] < min)
        {
            min = matrix[i];
        }
        --i;
        if (matrix[i] < min)
        {
            min = matrix[i];
        }
        --i;
        if (matrix[i] < min)
        {
            min = matrix[i];
        }
        --i;
    }

    // uint32_t* subset_min = malloc(sizeof(uint32_t) * g_nthreads);
    //
    // pthread_t thread_ids[g_nthreads];
    //
    // struct argument *args = malloc(sizeof(struct argument) * g_nthreads);
    //
    // for (size_t i = 0; i < g_nthreads; ++i) {
    //     args[i] = (struct argument) {
    //         .matB = matrix,
    //         .result = subset_min,
    //         .tid = i,
    //     };
    // }
    //
    // // launch
    // for (size_t i = 0; i < g_nthreads; ++i) {
    //     if (pthread_create(thread_ids + i, NULL, GetMin, args + i) != 0) {
    //         perror("pthread_create failed");
    //         exit(1);
    //     }
    // }
    //
    // // wait for the all the threads to finish
    // for (size_t i = 0; i < g_nthreads; ++i) {
    //     if (pthread_join(thread_ids[i], NULL) != 0) {
    //         perror("pthread_join failed");
    //         exit(1);
    //     }
    // }
    //
    // for (int i = 0; i < 4; i++) {
    //     printf("%u\n", subset_min[i]);
    // }
    // uint32_t min = subset_min[0];
    // for (ssize_t i = 0; i < g_elements; i++) {
    //     if (subset_min[i] < min)
    //     {
    //         min = subset_min[i];
    //     }
    // }
    //
    // free(args);
    // free(subset_min);
    return min;
}

/**
 * Returns the largest value in the matrix
 */
uint32_t get_maximum(const uint32_t* matrix) {

    /*
        to do

        1 2
        3 4 => 4

        4 3
        2 1 => 4
    */

    // uint32_t* max = calloc(1, sizeof(uint32_t));
    uint32_t max = matrix[g_elements - 1];

    if (g_width == 1) { return max;}
    
    for (ssize_t i = g_elements - 1; i >= 0;) {
        if (matrix[i] > max)
        {
            max = matrix[i];
        }
        --i;
        if (matrix[i] > max)
        {
            max = matrix[i];
        }
        --i;
        if (matrix[i] > max)
        {
            max = matrix[i];
        }
        --i;
        if (matrix[i] > max)
        {
            max = matrix[i];
        }
        --i;
    }

    return max;

    // pthread_t thread_ids[g_nthreads];

    // struct argument *args = malloc(sizeof(struct argument) * g_nthreads);

    // for (size_t i = 0; i < g_nthreads; ++i) {
    //     args[i] = (struct argument) {
    //         .matB = matrix,
    //         .result = max,
    //         .tid = i,
    //     };
    // }

    // // launch
    // for (size_t i = 0; i < g_nthreads; ++i) {
    //     if (pthread_create(thread_ids + i, NULL, GetMax, args + i) != 0) {
    //         perror("pthread_create failed");
    //         exit(1);
    //     }
    // }

    // // wait for the all the threads to finish
    // for (size_t i = 0; i < g_nthreads; ++i) {
    //     if (pthread_join(thread_ids[i], NULL) != 0) {
    //         perror("pthread_join failed");
    //         exit(1);
    //     }
    // }

    // uint32_t realmax = 0;
    // for (ssize_t i = g_elements - 1; i >= 0;) {
    //     if (matrix[i] > realmax)
    //     {
    //         realmax = matrix[i];
    //     }
    //     --i;
    //     if (matrix[i] > realmax)
    //     {
    //         realmax = matrix[i];
    //     }
    //     --i;
    //     if (matrix[i] > realmax)
    //     {
    //         realmax = matrix[i];
    //     }
    //     --i;
    //     if (matrix[i] > realmax)
    //     {
    //         realmax = matrix[i];
    //     }
    //     --i;
    // }

    // free(args);
    // free(max);
    // return realmax;
}

/**
 * Returns the frequency of the value in the matrix
 */
uint32_t get_frequency(const uint32_t* matrix, uint32_t value) {

    /*
        to do

        1 1
        1 1 :: 1 => 4

        1 0
        0 1 :: 2 => 0
    */

    uint32_t* freq = calloc(1, sizeof(uint32_t) * g_nthreads);

    // if (g_width == 1) {
    //     if (matrix[0] == value)
    //     {
    //         return 1;
    //     }
    //     return 0;
    // }
    //
    // for (ssize_t i = g_elements - 1; i >= 0;) {
    //     if (matrix[i] == value)
    //     {
    //         freq++;
    //     }
    //     --i;
    //     if (matrix[i] == value)
    //     {
    //         freq++;
    //     }
    //     --i;
    // }

    pthread_t thread_ids[g_nthreads];

    struct argument *args = malloc(sizeof(struct argument) * g_nthreads);

    for (size_t i = 0; i < g_nthreads; ++i) {
        args[i] = (struct argument) {
            .matB = matrix,
            .result = freq,
            .scalar = value,
            .tid = i,
        };
    }

    // launch
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_create(thread_ids + i, NULL, GetFreq, args + i) != 0) {
            perror("pthread_create failed");
            exit(1);
        }
    }

    // wait for the all the threads to finish
    for (size_t i = 0; i < g_nthreads; ++i) {
        if (pthread_join(thread_ids[i], NULL) != 0) {
            perror("pthread_join failed");
            exit(1);
        }
    }

    uint32_t sum = 0;
    for (size_t i = 0; i < g_nthreads;) {
        sum += freq[i];
        ++i;
        sum += freq[i];
        ++i;
        sum += freq[i];
        ++i;
        sum += freq[i];
        ++i;
    }

    free(args);
    free(freq);
    return sum;
}
