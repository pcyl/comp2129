	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_fast_rand
	.align	4, 0x90
_fast_rand:                             ## @fast_rand
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	imull	$214013, _g_seed(%rip), %eax ## imm = 0x343FD
	addl	$2531011, %eax          ## imm = 0x269EC3
	movl	%eax, _g_seed(%rip)
	shrl	$16, %eax
	andl	$32767, %eax            ## imm = 0x7FFF
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_set_seed
	.align	4, 0x90
_set_seed:                              ## @set_seed
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp3:
	.cfi_def_cfa_offset 16
Ltmp4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp5:
	.cfi_def_cfa_register %rbp
	movl	%edi, _g_seed(%rip)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_set_nthreads
	.align	4, 0x90
_set_nthreads:                          ## @set_nthreads
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp6:
	.cfi_def_cfa_offset 16
Ltmp7:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp8:
	.cfi_def_cfa_register %rbp
	movq	%rdi, _g_nthreads(%rip)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_set_dimensions
	.align	4, 0x90
_set_dimensions:                        ## @set_dimensions
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp9:
	.cfi_def_cfa_offset 16
Ltmp10:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp11:
	.cfi_def_cfa_register %rbp
	movq	%rdi, _g_width(%rip)
	movq	%rdi, _g_height(%rip)
	imulq	%rdi, %rdi
	movq	%rdi, _g_elements(%rip)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_display
	.align	4, 0x90
_display:                               ## @display
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp12:
	.cfi_def_cfa_offset 16
Ltmp13:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp14:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
Ltmp15:
	.cfi_offset %rbx, -48
Ltmp16:
	.cfi_offset %r12, -40
Ltmp17:
	.cfi_offset %r14, -32
Ltmp18:
	.cfi_offset %r15, -24
	movq	%rdi, %r15
	xorl	%r12d, %r12d
	cmpq	$0, _g_height(%rip)
	jle	LBB4_7
## BB#1:
	leaq	L_.str1(%rip), %r14
	.align	4, 0x90
LBB4_2:                                 ## %.preheader
                                        ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB4_3 Depth 2
	movq	_g_width(%rip), %rax
	testq	%rax, %rax
	movl	$0, %ebx
	jle	LBB4_6
	.align	4, 0x90
LBB4_3:                                 ## %.lr.ph
                                        ##   Parent Loop BB4_2 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	testq	%rbx, %rbx
	jle	LBB4_5
## BB#4:                                ##   in Loop: Header=BB4_3 Depth=2
	movl	$32, %edi
	callq	_putchar
	movq	_g_width(%rip), %rax
LBB4_5:                                 ##   in Loop: Header=BB4_3 Depth=2
	imulq	%r12, %rax
	addq	%rbx, %rax
	movl	(%r15,%rax,4), %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	_printf
	incq	%rbx
	movq	_g_width(%rip), %rax
	cmpq	%rax, %rbx
	jl	LBB4_3
LBB4_6:                                 ## %._crit_edge
                                        ##   in Loop: Header=BB4_2 Depth=1
	movl	$10, %edi
	callq	_putchar
	incq	%r12
	cmpq	_g_height(%rip), %r12
	jl	LBB4_2
LBB4_7:                                 ## %._crit_edge5
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_display_row
	.align	4, 0x90
_display_row:                           ## @display_row
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp19:
	.cfi_def_cfa_offset 16
Ltmp20:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp21:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
Ltmp22:
	.cfi_offset %rbx, -48
Ltmp23:
	.cfi_offset %r12, -40
Ltmp24:
	.cfi_offset %r14, -32
Ltmp25:
	.cfi_offset %r15, -24
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	_g_width(%rip), %rax
	xorl	%ebx, %ebx
	testq	%rax, %rax
	jle	LBB5_5
## BB#1:
	leaq	L_.str3(%rip), %r15
	.align	4, 0x90
LBB5_2:                                 ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	jle	LBB5_4
## BB#3:                                ##   in Loop: Header=BB5_2 Depth=1
	movl	$32, %edi
	callq	_putchar
	movq	_g_width(%rip), %rax
LBB5_4:                                 ##   in Loop: Header=BB5_2 Depth=1
	imulq	%r14, %rax
	addq	%rbx, %rax
	movl	(%r12,%rax,4), %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	_printf
	incq	%rbx
	movq	_g_width(%rip), %rax
	cmpq	%rax, %rbx
	jl	LBB5_2
LBB5_5:                                 ## %._crit_edge
	movl	$10, %edi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_putchar                ## TAILCALL
	.cfi_endproc

	.globl	_display_column
	.align	4, 0x90
_display_column:                        ## @display_column
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp26:
	.cfi_def_cfa_offset 16
Ltmp27:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp28:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
Ltmp29:
	.cfi_offset %rbx, -48
Ltmp30:
	.cfi_offset %r12, -40
Ltmp31:
	.cfi_offset %r14, -32
Ltmp32:
	.cfi_offset %r15, -24
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorl	%ebx, %ebx
	cmpq	$0, _g_height(%rip)
	jle	LBB6_3
## BB#1:
	leaq	L_.str4(%rip), %r15
	.align	4, 0x90
LBB6_2:                                 ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	movq	_g_width(%rip), %rax
	imulq	%rbx, %rax
	addq	%r14, %rax
	movl	(%r12,%rax,4), %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	_printf
	incq	%rbx
	cmpq	_g_height(%rip), %rbx
	jl	LBB6_2
LBB6_3:                                 ## %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_display_element
	.align	4, 0x90
_display_element:                       ## @display_element
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp33:
	.cfi_def_cfa_offset 16
Ltmp34:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp35:
	.cfi_def_cfa_register %rbp
	imulq	_g_width(%rip), %rsi
	addq	%rdx, %rsi
	movl	(%rdi,%rsi,4), %esi
	leaq	L_.str4(%rip), %rdi
	xorl	%eax, %eax
	popq	%rbp
	jmp	_printf                 ## TAILCALL
	.cfi_endproc

	.globl	_MatrixMul
	.align	4, 0x90
_MatrixMul:                             ## @MatrixMul
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp36:
	.cfi_def_cfa_offset 16
Ltmp37:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp38:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp39:
	.cfi_offset %rbx, -56
Ltmp40:
	.cfi_offset %r12, -48
Ltmp41:
	.cfi_offset %r13, -40
Ltmp42:
	.cfi_offset %r14, -32
Ltmp43:
	.cfi_offset %r15, -24
	movq	_g_width(%rip), %r15
	movq	_g_nthreads(%rip), %r14
	movq	%r15, %rax
	cqto
	idivq	%r14
	movq	%rax, %r13
	movq	(%rdi), %r12
	movq	8(%rdi), %rax
	movq	%rax, -56(%rbp)         ## 8-byte Spill
	movq	32(%rdi), %rbx
	movq	_g_elements(%rip), %rdi
	movl	$4, %esi
	callq	_calloc
	movq	%r13, %rsi
	movslq	%esi, %rcx
	movq	%rbx, %r13
	imulq	%rcx, %r13
	decq	%r14
	cmpq	%r14, %rbx
	movq	%r15, %rdx
	je	LBB8_2
## BB#1:
	leaq	1(%rbx), %rdx
	imulq	%rcx, %rdx
LBB8_2:
	movq	%rdx, -64(%rbp)         ## 8-byte Spill
	cmpq	%rdx, %r13
	jae	LBB8_15
## BB#3:                                ## %.preheader2.lr.ph
	movq	-56(%rbp), %rcx         ## 8-byte Reload
	leaq	16(%rcx), %rcx
	movq	%rcx, -72(%rbp)         ## 8-byte Spill
	leaq	(,%r15,4), %r9
	movq	%r15, %r10
	andq	$-8, %r10
	imulq	%r15, %rbx
	movslq	%esi, %r14
	imulq	%rbx, %r14
	movd	%r15, %xmm0
	movdqa	%xmm0, %xmm1
	psrlq	$32, %xmm1
	.align	4, 0x90
LBB8_4:                                 ## %.preheader2
                                        ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB8_5 Depth 2
                                        ##       Child Loop BB8_8 Depth 3
                                        ##       Child Loop BB8_12 Depth 3
	testq	%r15, %r15
	movl	$0, %r11d
	movq	-72(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -48(%rbp)         ## 8-byte Spill
	movl	$0, %r8d
	je	LBB8_14
	.align	4, 0x90
LBB8_5:                                 ## %.lr.ph
                                        ##   Parent Loop BB8_4 Depth=1
                                        ## =>  This Loop Header: Depth=2
                                        ##       Child Loop BB8_8 Depth 3
                                        ##       Child Loop BB8_12 Depth 3
	testq	%r15, %r15
	movl	$0, %ecx
	je	LBB8_11
## BB#6:                                ## %overflow.checked
                                        ##   in Loop: Header=BB8_5 Depth=2
	movq	%r15, %rsi
	andq	$-8, %rsi
	movl	$0, %ecx
	je	LBB8_10
## BB#7:                                ## %vector.ph
                                        ##   in Loop: Header=BB8_5 Depth=2
	movq	%r14, %rbx
	movq	%r9, %rdx
	movd	%r13, %xmm2
	movd	%r8, %xmm3
	movq	-48(%rbp), %r14         ## 8-byte Reload
	xorl	%r9d, %r9d
	.align	4, 0x90
LBB8_8:                                 ## %vector.body
                                        ##   Parent Loop BB8_4 Depth=1
                                        ##     Parent Loop BB8_5 Depth=2
                                        ## =>    This Inner Loop Header: Depth=3
	movdqa	%xmm0, %xmm4
	pmuludq	%xmm2, %xmm4
	movdqa	%xmm2, %xmm5
	psrlq	$32, %xmm5
	pmuludq	%xmm0, %xmm5
	psllq	$32, %xmm5
	paddq	%xmm4, %xmm5
	movdqa	%xmm1, %xmm4
	pmuludq	%xmm2, %xmm4
	psllq	$32, %xmm4
	paddq	%xmm5, %xmm4
	movd	%xmm4, %rcx
	paddq	%xmm3, %xmm4
	movd	%xmm4, %rdi
	movd	(%r12,%rdi,4), %xmm4    ## xmm4 = mem[0],zero,zero,zero
	pshufd	$0, %xmm4, %xmm4        ## xmm4 = xmm4[0,0,0,0]
	movdqu	-16(%r14), %xmm5
	movdqu	(%r14), %xmm6
	pshufd	$245, %xmm5, %xmm7      ## xmm7 = xmm5[1,1,3,3]
	pmuludq	%xmm4, %xmm7
	pmuludq	%xmm4, %xmm5
	shufps	$136, %xmm7, %xmm5      ## xmm5 = xmm5[0,2],xmm7[0,2]
	shufps	$216, %xmm5, %xmm5      ## xmm5 = xmm5[0,2,1,3]
	pshufd	$245, %xmm6, %xmm7      ## xmm7 = xmm6[1,1,3,3]
	pmuludq	%xmm4, %xmm7
	pmuludq	%xmm4, %xmm6
	shufps	$136, %xmm7, %xmm6      ## xmm6 = xmm6[0,2],xmm7[0,2]
	shufps	$216, %xmm6, %xmm6      ## xmm6 = xmm6[0,2,1,3]
	addq	%r9, %rcx
	movdqu	(%rax,%rcx,4), %xmm4
	movdqu	16(%rax,%rcx,4), %xmm7
	paddd	%xmm5, %xmm4
	paddd	%xmm6, %xmm7
	movdqu	%xmm4, (%rax,%rcx,4)
	movdqu	%xmm7, 16(%rax,%rcx,4)
	addq	$8, %r9
	addq	$32, %r14
	cmpq	%r9, %r10
	jne	LBB8_8
## BB#9:                                ##   in Loop: Header=BB8_5 Depth=2
	movq	%rsi, %rcx
	movq	%rdx, %r9
	movq	%rbx, %r14
LBB8_10:                                ## %middle.block
                                        ##   in Loop: Header=BB8_5 Depth=2
	cmpq	%rcx, %r15
	je	LBB8_13
LBB8_11:                                ## %scalar.ph.preheader
                                        ##   in Loop: Header=BB8_5 Depth=2
	movq	%r15, %rdi
	subq	%rcx, %rdi
	leaq	(%rcx,%r11), %rsi
	movq	-56(%rbp), %rdx         ## 8-byte Reload
	leaq	(%rdx,%rsi,4), %rsi
	addq	%r14, %rcx
	leaq	(%rax,%rcx,4), %rcx
	.align	4, 0x90
LBB8_12:                                ## %scalar.ph
                                        ##   Parent Loop BB8_4 Depth=1
                                        ##     Parent Loop BB8_5 Depth=2
                                        ## =>    This Inner Loop Header: Depth=3
	movq	%r15, %rbx
	imulq	%r13, %rbx
	addq	%r8, %rbx
	movl	(%rsi), %edx
	imull	(%r12,%rbx,4), %edx
	addl	%edx, (%rcx)
	addq	$4, %rsi
	addq	$4, %rcx
	decq	%rdi
	jne	LBB8_12
LBB8_13:                                ## %._crit_edge
                                        ##   in Loop: Header=BB8_5 Depth=2
	incq	%r8
	addq	%r9, -48(%rbp)          ## 8-byte Folded Spill
	addq	%r15, %r11
	cmpq	%r15, %r8
	jne	LBB8_5
LBB8_14:                                ## %._crit_edge6
                                        ##   in Loop: Header=BB8_4 Depth=1
	incq	%r13
	addq	%r15, %r14
	movq	-64(%rbp), %rcx         ## 8-byte Reload
	cmpq	%rcx, %r13
	jne	LBB8_4
LBB8_15:                                ## %._crit_edge9
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_new_matrix
	.align	4, 0x90
_new_matrix:                            ## @new_matrix
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp44:
	.cfi_def_cfa_offset 16
Ltmp45:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp46:
	.cfi_def_cfa_register %rbp
	movq	_g_elements(%rip), %rdi
	movl	$4, %esi
	popq	%rbp
	jmp	_calloc                 ## TAILCALL
	.cfi_endproc

	.globl	_ScalarAdd
	.align	4, 0x90
_ScalarAdd:                             ## @ScalarAdd
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp47:
	.cfi_def_cfa_offset 16
Ltmp48:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp49:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
Ltmp50:
	.cfi_offset %rbx, -56
Ltmp51:
	.cfi_offset %r12, -48
Ltmp52:
	.cfi_offset %r13, -40
Ltmp53:
	.cfi_offset %r14, -32
Ltmp54:
	.cfi_offset %r15, -24
	movq	_g_width(%rip), %r12
	movq	_g_nthreads(%rip), %r14
	movq	%r12, %rax
	cqto
	idivq	%r14
	movq	%rax, %r13
	movq	8(%rdi), %rax
	movq	%rax, -48(%rbp)         ## 8-byte Spill
	movl	24(%rdi), %ebx
	movq	32(%rdi), %r15
	movq	_g_elements(%rip), %rdi
	movl	$4, %esi
	callq	_calloc
	movq	%rax, -56(%rbp)         ## 8-byte Spill
	movq	%r13, %rax
	movslq	%eax, %rcx
	movq	%r15, %r13
	imulq	%rcx, %r13
	decq	%r14
	cmpq	%r14, %r15
	movq	%r12, %r8
	je	LBB10_2
## BB#1:
	leaq	1(%r15), %r8
	imulq	%rcx, %r8
LBB10_2:
	cmpq	%r8, %r13
	jae	LBB10_13
## BB#3:                                ## %.preheader.lr.ph
	imulq	%r12, %r15
	movslq	%eax, %r14
	imulq	%r15, %r14
	movq	-48(%rbp), %rax         ## 8-byte Reload
	leaq	16(%rax,%r14,4), %r15
	leaq	(,%r12,4), %r9
	movq	-56(%rbp), %rax         ## 8-byte Reload
	leaq	16(%rax,%r14,4), %r10
	movq	%r12, %rax
	andq	$-8, %rax
	movd	%ebx, %xmm0
	pshufd	$0, %xmm0, %xmm0        ## xmm0 = xmm0[0,0,0,0]
	.align	4, 0x90
LBB10_4:                                ## %.preheader
                                        ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB10_7 Depth 2
                                        ##     Child Loop BB10_11 Depth 2
	testq	%r12, %r12
	je	LBB10_12
## BB#5:                                ## %overflow.checked
                                        ##   in Loop: Header=BB10_4 Depth=1
	movq	%r12, %r11
	andq	$-8, %r11
	movl	$0, %edi
	je	LBB10_9
## BB#6:                                ## %vector.ph
                                        ##   in Loop: Header=BB10_4 Depth=1
	movq	%rax, %rcx
	movq	%r10, %rdx
	movq	%r15, %rdi
	.align	4, 0x90
LBB10_7:                                ## %vector.body
                                        ##   Parent Loop BB10_4 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rdi), %xmm1
	movdqu	(%rdi), %xmm2
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm2
	movdqu	-16(%rdx), %xmm3
	movdqu	(%rdx), %xmm4
	paddd	%xmm1, %xmm3
	paddd	%xmm2, %xmm4
	movdqu	%xmm3, -16(%rdx)
	movdqu	%xmm4, (%rdx)
	addq	$32, %rdi
	addq	$32, %rdx
	addq	$-8, %rcx
	jne	LBB10_7
## BB#8:                                ##   in Loop: Header=BB10_4 Depth=1
	movq	%r11, %rdi
LBB10_9:                                ## %middle.block
                                        ##   in Loop: Header=BB10_4 Depth=1
	movq	%r12, %rcx
	subq	%rdi, %rcx
	je	LBB10_12
## BB#10:                               ## %.lr.ph.preheader
                                        ##   in Loop: Header=BB10_4 Depth=1
	addq	%r14, %rdi
	movq	-56(%rbp), %rdx         ## 8-byte Reload
	leaq	(%rdx,%rdi,4), %rdx
	movq	-48(%rbp), %rsi         ## 8-byte Reload
	leaq	(%rsi,%rdi,4), %rdi
	.align	4, 0x90
LBB10_11:                               ## %.lr.ph
                                        ##   Parent Loop BB10_4 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %esi
	addl	%ebx, %esi
	addl	%esi, (%rdx)
	addq	$4, %rdx
	addq	$4, %rdi
	decq	%rcx
	jne	LBB10_11
LBB10_12:                               ## %._crit_edge
                                        ##   in Loop: Header=BB10_4 Depth=1
	incq	%r13
	addq	%r9, %r15
	addq	%r9, %r10
	addq	%r12, %r14
	cmpq	%r8, %r13
	jne	LBB10_4
LBB10_13:                               ## %._crit_edge5
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_ScalarMul
	.align	4, 0x90
_ScalarMul:                             ## @ScalarMul
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp55:
	.cfi_def_cfa_offset 16
Ltmp56:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp57:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
Ltmp58:
	.cfi_offset %rbx, -56
Ltmp59:
	.cfi_offset %r12, -48
Ltmp60:
	.cfi_offset %r13, -40
Ltmp61:
	.cfi_offset %r14, -32
Ltmp62:
	.cfi_offset %r15, -24
	movq	_g_width(%rip), %r12
	movq	_g_nthreads(%rip), %r14
	movq	%r12, %rax
	cqto
	idivq	%r14
	movq	%rax, %r13
	movq	8(%rdi), %rax
	movq	%rax, -48(%rbp)         ## 8-byte Spill
	movl	24(%rdi), %ebx
	movq	32(%rdi), %r15
	movq	_g_elements(%rip), %rdi
	movl	$4, %esi
	callq	_calloc
	movq	%rax, -56(%rbp)         ## 8-byte Spill
	movq	%r13, %rax
	movslq	%eax, %rcx
	movq	%r15, %r13
	imulq	%rcx, %r13
	decq	%r14
	cmpq	%r14, %r15
	movq	%r12, %r8
	je	LBB11_2
## BB#1:
	leaq	1(%r15), %r8
	imulq	%rcx, %r8
LBB11_2:
	cmpq	%r8, %r13
	jae	LBB11_13
## BB#3:                                ## %.preheader.lr.ph
	imulq	%r12, %r15
	movslq	%eax, %r14
	imulq	%r15, %r14
	movq	-48(%rbp), %rax         ## 8-byte Reload
	leaq	16(%rax,%r14,4), %r15
	leaq	(,%r12,4), %r9
	movq	-56(%rbp), %rax         ## 8-byte Reload
	leaq	16(%rax,%r14,4), %r10
	movq	%r12, %rax
	andq	$-8, %rax
	movd	%ebx, %xmm0
	pshufd	$0, %xmm0, %xmm0        ## xmm0 = xmm0[0,0,0,0]
	pshufd	$245, %xmm0, %xmm1      ## xmm1 = xmm0[1,1,3,3]
	.align	4, 0x90
LBB11_4:                                ## %.preheader
                                        ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB11_7 Depth 2
                                        ##     Child Loop BB11_11 Depth 2
	testq	%r12, %r12
	je	LBB11_12
## BB#5:                                ## %overflow.checked
                                        ##   in Loop: Header=BB11_4 Depth=1
	movq	%r12, %r11
	andq	$-8, %r11
	movl	$0, %edi
	je	LBB11_9
## BB#6:                                ## %vector.ph
                                        ##   in Loop: Header=BB11_4 Depth=1
	movq	%rax, %rcx
	movq	%r10, %rdx
	movq	%r15, %rdi
	.align	4, 0x90
LBB11_7:                                ## %vector.body
                                        ##   Parent Loop BB11_4 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rdi), %xmm2
	movdqu	(%rdi), %xmm3
	pshufd	$245, %xmm2, %xmm4      ## xmm4 = xmm2[1,1,3,3]
	pmuludq	%xmm0, %xmm2
	pmuludq	%xmm1, %xmm4
	shufps	$136, %xmm4, %xmm2      ## xmm2 = xmm2[0,2],xmm4[0,2]
	shufps	$216, %xmm2, %xmm2      ## xmm2 = xmm2[0,2,1,3]
	pshufd	$245, %xmm3, %xmm4      ## xmm4 = xmm3[1,1,3,3]
	pmuludq	%xmm0, %xmm3
	pmuludq	%xmm1, %xmm4
	shufps	$136, %xmm4, %xmm3      ## xmm3 = xmm3[0,2],xmm4[0,2]
	shufps	$216, %xmm3, %xmm3      ## xmm3 = xmm3[0,2,1,3]
	movdqu	-16(%rdx), %xmm4
	movdqu	(%rdx), %xmm5
	paddd	%xmm2, %xmm4
	paddd	%xmm3, %xmm5
	movdqu	%xmm4, -16(%rdx)
	movdqu	%xmm5, (%rdx)
	addq	$32, %rdi
	addq	$32, %rdx
	addq	$-8, %rcx
	jne	LBB11_7
## BB#8:                                ##   in Loop: Header=BB11_4 Depth=1
	movq	%r11, %rdi
LBB11_9:                                ## %middle.block
                                        ##   in Loop: Header=BB11_4 Depth=1
	movq	%r12, %rcx
	subq	%rdi, %rcx
	je	LBB11_12
## BB#10:                               ## %.lr.ph.preheader
                                        ##   in Loop: Header=BB11_4 Depth=1
	addq	%r14, %rdi
	movq	-56(%rbp), %rdx         ## 8-byte Reload
	leaq	(%rdx,%rdi,4), %rdx
	movq	-48(%rbp), %rsi         ## 8-byte Reload
	leaq	(%rsi,%rdi,4), %rdi
	.align	4, 0x90
LBB11_11:                               ## %.lr.ph
                                        ##   Parent Loop BB11_4 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %esi
	imull	%ebx, %esi
	addl	%esi, (%rdx)
	addq	$4, %rdx
	addq	$4, %rdi
	decq	%rcx
	jne	LBB11_11
LBB11_12:                               ## %._crit_edge
                                        ##   in Loop: Header=BB11_4 Depth=1
	incq	%r13
	addq	%r9, %r15
	addq	%r9, %r10
	addq	%r12, %r14
	cmpq	%r8, %r13
	jne	LBB11_4
LBB11_13:                               ## %._crit_edge5
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_GetFreq
	.align	4, 0x90
_GetFreq:                               ## @GetFreq
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp63:
	.cfi_def_cfa_offset 16
Ltmp64:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp65:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
Ltmp66:
	.cfi_offset %rbx, -24
	movq	_g_width(%rip), %r8
	movq	_g_nthreads(%rip), %rbx
	movq	%r8, %rax
	cqto
	idivq	%rbx
	movq	8(%rdi), %r10
	movq	16(%rdi), %rdx
	movl	24(%rdi), %esi
	movq	32(%rdi), %rdi
	movslq	%eax, %rcx
	movq	%rdi, %r11
	imulq	%rcx, %r11
	decq	%rbx
	cmpq	%rbx, %rdi
	movq	%r8, %r9
	je	LBB12_2
## BB#1:
	leaq	1(%rdi), %r9
	imulq	%rcx, %r9
LBB12_2:
	cmpq	%r9, %r11
	jae	LBB12_9
## BB#3:                                ## %.preheader.lr.ph
	movq	%rdi, %rcx
	imulq	%r8, %rcx
	cltq
	imulq	%rcx, %rax
	leaq	(%r10,%rax,4), %rcx
	leaq	(,%r8,4), %r10
	.align	4, 0x90
LBB12_4:                                ## %.preheader
                                        ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB12_5 Depth 2
	testq	%r8, %r8
	movq	%rcx, %rax
	movq	%r8, %rbx
	je	LBB12_8
	.align	4, 0x90
LBB12_5:                                ## %.lr.ph
                                        ##   Parent Loop BB12_4 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	cmpl	%esi, (%rax)
	jne	LBB12_7
## BB#6:                                ##   in Loop: Header=BB12_5 Depth=2
	incl	(%rdx,%rdi,4)
LBB12_7:                                ##   in Loop: Header=BB12_5 Depth=2
	addq	$4, %rax
	decq	%rbx
	jne	LBB12_5
LBB12_8:                                ## %._crit_edge
                                        ##   in Loop: Header=BB12_4 Depth=1
	incq	%r11
	addq	%r10, %rcx
	cmpq	%r9, %r11
	jne	LBB12_4
LBB12_9:                                ## %._crit_edge5
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_GetMax
	.align	4, 0x90
_GetMax:                                ## @GetMax
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp67:
	.cfi_def_cfa_offset 16
Ltmp68:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp69:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
Ltmp70:
	.cfi_offset %rbx, -32
Ltmp71:
	.cfi_offset %r14, -24
	movq	_g_width(%rip), %r14
	movq	_g_nthreads(%rip), %rsi
	movq	%r14, %rax
	cqto
	idivq	%rsi
	movq	8(%rdi), %r8
	movq	16(%rdi), %r10
	movq	32(%rdi), %rdx
	movslq	%eax, %rcx
	movq	%rdx, %rdi
	imulq	%rcx, %rdi
	decq	%rsi
	cmpq	%rsi, %rdx
	movq	%r14, %r9
	je	LBB13_2
## BB#1:
	leaq	1(%rdx), %r9
	imulq	%rcx, %r9
LBB13_2:
	movq	%r14, %rcx
	imulq	%rdi, %rcx
	movl	(%r8,%rcx,4), %esi
	movl	%esi, (%r10,%rdx,4)
	cmpq	%r9, %rdi
	jae	LBB13_9
## BB#3:                                ## %.preheader.lr.ph
	movq	%rdx, %rcx
	imulq	%r14, %rcx
	cltq
	imulq	%rcx, %rax
	leaq	(%r8,%rax,4), %rax
	leaq	(,%r14,4), %r11
	.align	4, 0x90
LBB13_4:                                ## %.preheader
                                        ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB13_5 Depth 2
	testq	%r14, %r14
	movq	%rax, %rbx
	movq	%r14, %rcx
	je	LBB13_8
	.align	4, 0x90
LBB13_5:                                ## %.lr.ph
                                        ##   Parent Loop BB13_4 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	cmpl	%esi, (%rbx)
	jbe	LBB13_7
## BB#6:                                ##   in Loop: Header=BB13_5 Depth=2
	movq	%r14, %rsi
	imulq	%rdi, %rsi
	addq	%rdi, %rsi
	movl	(%r8,%rsi,4), %esi
	movl	%esi, (%r10,%rdx,4)
LBB13_7:                                ##   in Loop: Header=BB13_5 Depth=2
	addq	$4, %rbx
	decq	%rcx
	jne	LBB13_5
LBB13_8:                                ## %._crit_edge
                                        ##   in Loop: Header=BB13_4 Depth=1
	incq	%rdi
	addq	%r11, %rax
	cmpq	%r9, %rdi
	jne	LBB13_4
LBB13_9:                                ## %._crit_edge5
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_GetTrace
	.align	4, 0x90
_GetTrace:                              ## @GetTrace
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp72:
	.cfi_def_cfa_offset 16
Ltmp73:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp74:
	.cfi_def_cfa_register %rbp
	movq	_g_width(%rip), %r8
	movq	_g_nthreads(%rip), %rcx
	movq	%r8, %rax
	cqto
	idivq	%rcx
	movq	8(%rdi), %r9
	movq	16(%rdi), %r10
	movq	32(%rdi), %r11
	movslq	%eax, %rdx
	movq	%r11, %rdi
	imulq	%rdx, %rdi
	decq	%rcx
	cmpq	%rcx, %r11
	movq	%r8, %rcx
	je	LBB14_2
## BB#1:
	leaq	1(%r11), %rcx
	imulq	%rdx, %rcx
LBB14_2:
	cmpq	%rcx, %rdi
	jae	LBB14_5
## BB#3:                                ## %.lr.ph
	movl	(%r10,%r11,4), %edx
	leaq	1(%r8), %rsi
	imulq	%r11, %rsi
	cltq
	imulq	%rsi, %rax
	leaq	(%r9,%rax,4), %rax
	leaq	4(,%r8,4), %rsi
	.align	4, 0x90
LBB14_4:                                ## =>This Inner Loop Header: Depth=1
	addl	(%rax), %edx
	movl	%edx, (%r10,%r11,4)
	incq	%rdi
	addq	%rsi, %rax
	cmpq	%rdi, %rcx
	jne	LBB14_4
LBB14_5:                                ## %._crit_edge
	xorl	%eax, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_identity_matrix
	.align	4, 0x90
_identity_matrix:                       ## @identity_matrix
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp75:
	.cfi_def_cfa_offset 16
Ltmp76:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp77:
	.cfi_def_cfa_register %rbp
	movq	_g_elements(%rip), %rdi
	movl	$4, %esi
	callq	_calloc
	movq	_g_width(%rip), %rcx
	testq	%rcx, %rcx
	jle	LBB15_9
## BB#1:                                ## %.lr.ph.preheader
	xorl	%edx, %edx
	testq	%rcx, %rcx
	je	LBB15_7
## BB#2:                                ## %overflow.checked
	xorl	%edx, %edx
	movq	%rcx, %r8
	andq	$-2, %r8
	je	LBB15_6
## BB#3:                                ## %vector.body.preheader
	leaq	8(,%rcx,8), %r9
	movq	%rcx, %rdi
	andq	$-2, %rdi
	xorl	%esi, %esi
	movq	%rax, %rdx
	.align	4, 0x90
LBB15_4:                                ## %vector.body
                                        ## =>This Inner Loop Header: Depth=1
	movl	$1, (%rdx)
	movl	$1, 4(%rdx,%rcx,4)
	addq	$2, %rsi
	addq	%r9, %rdx
	cmpq	%rsi, %rdi
	jne	LBB15_4
## BB#5:
	movq	%r8, %rdx
LBB15_6:                                ## %middle.block
	cmpq	%rdx, %rcx
	je	LBB15_9
LBB15_7:                                ## %.lr.ph.preheader3
	leaq	1(%rcx), %rsi
	imulq	%rdx, %rsi
	leaq	(%rax,%rsi,4), %rsi
	leaq	4(,%rcx,4), %rdi
	.align	4, 0x90
LBB15_8:                                ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	movl	$1, (%rsi)
	incq	%rdx
	addq	%rdi, %rsi
	cmpq	%rcx, %rdx
	jl	LBB15_8
LBB15_9:                                ## %._crit_edge
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_random_matrix
	.align	4, 0x90
_random_matrix:                         ## @random_matrix
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp78:
	.cfi_def_cfa_offset 16
Ltmp79:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp80:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
Ltmp81:
	.cfi_offset %rbx, -32
Ltmp82:
	.cfi_offset %r14, -24
	movl	%edi, %ebx
	movq	_g_elements(%rip), %r14
	movl	$4, %esi
	movq	%r14, %rdi
	callq	_calloc
	movl	%ebx, _g_seed(%rip)
	xorl	%ecx, %ecx
	testq	%r14, %r14
	jle	LBB16_3
	.align	4, 0x90
LBB16_1:                                ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	imull	$214013, %ebx, %ebx     ## imm = 0x343FD
	addl	$2531011, %ebx          ## imm = 0x269EC3
	movl	%ebx, %edx
	shrl	$16, %edx
	andl	$32767, %edx            ## imm = 0x7FFF
	movl	%edx, (%rax,%rcx,4)
	incq	%rcx
	cmpq	%r14, %rcx
	jl	LBB16_1
## BB#2:                                ## %._crit_edge
	movl	%ebx, _g_seed(%rip)
LBB16_3:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_uniform_matrix
	.align	4, 0x90
_uniform_matrix:                        ## @uniform_matrix
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp83:
	.cfi_def_cfa_offset 16
Ltmp84:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp85:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
Ltmp86:
	.cfi_offset %rbx, -32
Ltmp87:
	.cfi_offset %r14, -24
	movl	%edi, %r14d
	movq	_g_elements(%rip), %rbx
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	_calloc
	testq	%rbx, %rbx
	jle	LBB17_8
## BB#1:                                ## %.lr.ph.preheader
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	LBB17_7
## BB#2:                                ## %overflow.checked
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	andq	$-8, %rdx
	je	LBB17_6
## BB#3:                                ## %vector.ph
	movd	%r14d, %xmm0
	pshufd	$0, %xmm0, %xmm0        ## xmm0 = xmm0[0,0,0,0]
	movq	%rax, %rcx
	addq	$16, %rcx
	movq	%rbx, %rsi
	andq	$-8, %rsi
	.align	4, 0x90
LBB17_4:                                ## %vector.body
                                        ## =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rcx)
	movdqu	%xmm0, (%rcx)
	addq	$32, %rcx
	addq	$-8, %rsi
	jne	LBB17_4
## BB#5:
	movq	%rdx, %rcx
LBB17_6:                                ## %middle.block
	cmpq	%rcx, %rbx
	je	LBB17_8
	.align	4, 0x90
LBB17_7:                                ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	movl	%r14d, (%rax,%rcx,4)
	incq	%rcx
	cmpq	%rbx, %rcx
	jl	LBB17_7
LBB17_8:                                ## %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_sequence_matrix
	.align	4, 0x90
_sequence_matrix:                       ## @sequence_matrix
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp88:
	.cfi_def_cfa_offset 16
Ltmp89:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp90:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	pushq	%rax
Ltmp91:
	.cfi_offset %rbx, -40
Ltmp92:
	.cfi_offset %r14, -32
Ltmp93:
	.cfi_offset %r15, -24
	movl	%esi, %r14d
	movl	%edi, %ebx
	movq	_g_elements(%rip), %r15
	movl	$4, %esi
	movq	%r15, %rdi
	callq	_calloc
	xorl	%ecx, %ecx
	testq	%r15, %r15
	jle	LBB18_2
	.align	4, 0x90
LBB18_1:                                ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	movl	%ebx, (%rax,%rcx,4)
	addl	%r14d, %ebx
	incq	%rcx
	cmpq	%r15, %rcx
	jl	LBB18_1
LBB18_2:                                ## %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_cloned
	.align	4, 0x90
_cloned:                                ## @cloned
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp94:
	.cfi_def_cfa_offset 16
Ltmp95:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp96:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	pushq	%rax
Ltmp97:
	.cfi_offset %rbx, -40
Ltmp98:
	.cfi_offset %r14, -32
Ltmp99:
	.cfi_offset %r15, -24
	movq	%rdi, %r14
	movq	_g_elements(%rip), %rbx
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	_calloc
	movq	%rax, %r15
	testq	%rbx, %rbx
	jle	LBB19_2
## BB#1:                                ## %.lr.ph
	cmpq	$1, %rbx
	leaq	(,%rbx,4), %rax
	movl	$4, %edx
	cmovgq	%rax, %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	_memcpy
LBB19_2:
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_reversed
	.align	4, 0x90
_reversed:                              ## @reversed
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp100:
	.cfi_def_cfa_offset 16
Ltmp101:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp102:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
Ltmp103:
	.cfi_offset %rbx, -32
Ltmp104:
	.cfi_offset %r14, -24
	movq	%rdi, %r14
	movq	_g_elements(%rip), %rbx
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	_calloc
	testq	%rbx, %rbx
	jle	LBB20_9
## BB#1:                                ## %.lr.ph.preheader
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	LBB20_7
## BB#2:                                ## %overflow.checked
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	andq	$-8, %rdx
	je	LBB20_6
## BB#3:                                ## %vector.body.preheader
	leaq	-16(%r14,%rbx,4), %rcx
	movq	%rax, %rsi
	addq	$16, %rsi
	movq	%rbx, %rdi
	andq	$-8, %rdi
	.align	4, 0x90
LBB20_4:                                ## %vector.body
                                        ## =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	pshufd	$27, %xmm1, %xmm1       ## xmm1 = xmm1[3,2,1,0]
	pshufd	$27, %xmm0, %xmm0       ## xmm0 = xmm0[3,2,1,0]
	movdqu	%xmm1, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	addq	$-32, %rcx
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	LBB20_4
## BB#5:
	movq	%rdx, %rcx
LBB20_6:                                ## %middle.block
	cmpq	%rcx, %rbx
	je	LBB20_9
LBB20_7:                                ## %.lr.ph.preheader10
	leaq	-1(%rbx), %rdx
	subq	%rcx, %rdx
	leaq	(%r14,%rdx,4), %rdx
	.align	4, 0x90
LBB20_8:                                ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	movl	(%rdx), %esi
	movl	%esi, (%rax,%rcx,4)
	incq	%rcx
	addq	$-4, %rdx
	cmpq	%rbx, %rcx
	jl	LBB20_8
LBB20_9:                                ## %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_transposed
	.align	4, 0x90
_transposed:                            ## @transposed
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp105:
	.cfi_def_cfa_offset 16
Ltmp106:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp107:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
Ltmp108:
	.cfi_offset %rbx, -48
Ltmp109:
	.cfi_offset %r12, -40
Ltmp110:
	.cfi_offset %r14, -32
Ltmp111:
	.cfi_offset %r15, -24
	movq	%rdi, %r12
	movq	_g_elements(%rip), %rdi
	movl	$4, %esi
	callq	_calloc
	movq	_g_height(%rip), %r8
	testq	%r8, %r8
	jle	LBB21_14
## BB#1:                                ## %.preheader.lr.ph
	movq	_g_width(%rip), %r9
	movq	%r9, %r11
	andq	$-8, %r11
	leaq	(,%r9,4), %rdi
	xorl	%r14d, %r14d
	movl	$1, %ecx
	movd	%rcx, %xmm0
	pslldq	$8, %xmm0               ## xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	xorl	%r15d, %r15d
	.align	4, 0x90
LBB21_2:                                ## %.preheader
                                        ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB21_8 Depth 2
                                        ##     Child Loop BB21_12 Depth 2
	testq	%r9, %r9
	jle	LBB21_13
## BB#3:                                ## %.lr.ph.preheader
                                        ##   in Loop: Header=BB21_2 Depth=1
	movl	$0, %edx
	je	LBB21_11
## BB#4:                                ## %overflow.checked
                                        ##   in Loop: Header=BB21_2 Depth=1
	movq	%r9, %r10
	andq	$-8, %r10
	je	LBB21_5
## BB#6:                                ## %overflow.checked
                                        ##   in Loop: Header=BB21_2 Depth=1
	cmpq	$1, %r9
	movl	$0, %edx
	jne	LBB21_10
## BB#7:                                ## %vector.ph
                                        ##   in Loop: Header=BB21_2 Depth=1
	movd	%r15, %xmm1
	pshufd	$68, %xmm1, %xmm1       ## xmm1 = xmm1[0,1,0,1]
	xorl	%ecx, %ecx
	.align	4, 0x90
LBB21_8:                                ## %vector.body
                                        ##   Parent Loop BB21_2 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	movd	%rcx, %xmm2
	pshufd	$68, %xmm2, %xmm2       ## xmm2 = xmm2[0,1,0,1]
	paddq	%xmm1, %xmm2
	paddq	%xmm0, %xmm2
	movd	%xmm2, %rdx
	movups	(%r12,%rdx,4), %xmm2
	movups	16(%r12,%rdx,4), %xmm3
	movups	%xmm2, (%rax,%rdx,4)
	movups	%xmm3, 16(%rax,%rdx,4)
	addq	$8, %rcx
	cmpq	%rcx, %r11
	jne	LBB21_8
## BB#9:                                ##   in Loop: Header=BB21_2 Depth=1
	movq	%r10, %rdx
	jmp	LBB21_10
LBB21_5:                                ##   in Loop: Header=BB21_2 Depth=1
	xorl	%edx, %edx
LBB21_10:                               ## %middle.block
                                        ##   in Loop: Header=BB21_2 Depth=1
	cmpq	%rdx, %r9
	je	LBB21_13
LBB21_11:                               ## %.lr.ph.preheader12
                                        ##   in Loop: Header=BB21_2 Depth=1
	movq	%r9, %rsi
	subq	%rdx, %rsi
	leaq	(%rdx,%r14), %rcx
	leaq	(%r12,%rcx,4), %rcx
	imulq	%r9, %rdx
	addq	%r15, %rdx
	leaq	(%rax,%rdx,4), %rdx
	.align	4, 0x90
LBB21_12:                               ## %.lr.ph
                                        ##   Parent Loop BB21_2 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %ebx
	movl	%ebx, (%rdx)
	addq	$4, %rcx
	addq	%rdi, %rdx
	decq	%rsi
	jne	LBB21_12
LBB21_13:                               ## %._crit_edge
                                        ##   in Loop: Header=BB21_2 Depth=1
	incq	%r15
	addq	%r9, %r14
	cmpq	%r8, %r15
	jl	LBB21_2
LBB21_14:                               ## %._crit_edge4
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_scalar_add
	.align	4, 0x90
_scalar_add:                            ## @scalar_add
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp112:
	.cfi_def_cfa_offset 16
Ltmp113:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp114:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp115:
	.cfi_offset %rbx, -56
Ltmp116:
	.cfi_offset %r12, -48
Ltmp117:
	.cfi_offset %r13, -40
Ltmp118:
	.cfi_offset %r14, -32
Ltmp119:
	.cfi_offset %r15, -24
	movl	%esi, %r12d
	movq	%rdi, %r13
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	movq	_g_elements(%rip), %rdi
	movl	$4, %esi
	callq	_calloc
	movq	%rax, %r15
	movq	_g_nthreads(%rip), %rbx
	movq	%rsp, %rcx
	leaq	15(,%rbx,8), %rax
	andq	$-16, %rax
	subq	%rax, %rcx
	movq	%rcx, -72(%rbp)         ## 8-byte Spill
	movq	%rcx, %rsp
	imulq	$56, %rbx, %rdi
	callq	_malloc
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	testq	%rbx, %rbx
	je	LBB22_8
## BB#1:                                ## %.lr.ph10
	movq	-80(%rbp), %rax         ## 8-byte Reload
	addq	$40, %rax
	xorl	%ecx, %ecx
	xorps	%xmm0, %xmm0
	.align	4, 0x90
LBB22_2:                                ## =>This Inner Loop Header: Depth=1
	movaps	%xmm0, -64(%rbp)
	movq	$0, -40(%rax)
	movq	%r13, -32(%rax)
	movq	%r15, -24(%rax)
	movl	%r12d, -16(%rax)
	movq	%rcx, -8(%rax)
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%rsi, 8(%rax)
	movq	%rdx, (%rax)
	incq	%rcx
	addq	$56, %rax
	cmpq	%rbx, %rcx
	jb	LBB22_2
## BB#3:                                ## %.preheader4
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	LBB22_8
## BB#4:
	leaq	_ScalarAdd(%rip), %r12
	movq	-80(%rbp), %r13         ## 8-byte Reload
	movq	-72(%rbp), %rbx         ## 8-byte Reload
	.align	4, 0x90
LBB22_5:                                ## %.lr.ph7
                                        ## =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r13, %rcx
	callq	_pthread_create
	testl	%eax, %eax
	jne	LBB22_10
## BB#6:                                ##   in Loop: Header=BB22_5 Depth=1
	incq	%r14
	movq	_g_nthreads(%rip), %rax
	addq	$8, %rbx
	addq	$56, %r13
	cmpq	%rax, %r14
	jb	LBB22_5
## BB#7:                                ## %.preheader
	xorl	%ebx, %ebx
	testq	%rax, %rax
	je	LBB22_8
	.align	4, 0x90
LBB22_13:                               ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	movq	-72(%rbp), %rax         ## 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	xorl	%esi, %esi
	callq	_pthread_join
	testl	%eax, %eax
	jne	LBB22_14
## BB#12:                               ##   in Loop: Header=BB22_13 Depth=1
	incq	%rbx
	cmpq	_g_nthreads(%rip), %rbx
	jb	LBB22_13
LBB22_8:                                ## %._crit_edge
	movq	-80(%rbp), %rdi         ## 8-byte Reload
	callq	_free
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	cmpq	-48(%rbp), %rax
	jne	LBB22_9
## BB#15:                               ## %._crit_edge
	movq	%r15, %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB22_10:
	leaq	L_.str5(%rip), %rdi
	jmp	LBB22_11
LBB22_14:
	leaq	L_.str6(%rip), %rdi
LBB22_11:
	callq	_perror
	movl	$1, %edi
	callq	_exit
LBB22_9:                                ## %._crit_edge
	callq	___stack_chk_fail
	.cfi_endproc

	.globl	_scalar_mul
	.align	4, 0x90
_scalar_mul:                            ## @scalar_mul
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp120:
	.cfi_def_cfa_offset 16
Ltmp121:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp122:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp123:
	.cfi_offset %rbx, -56
Ltmp124:
	.cfi_offset %r12, -48
Ltmp125:
	.cfi_offset %r13, -40
Ltmp126:
	.cfi_offset %r14, -32
Ltmp127:
	.cfi_offset %r15, -24
	movl	%esi, %r12d
	movq	%rdi, %r13
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	movq	_g_elements(%rip), %rdi
	movl	$4, %esi
	callq	_calloc
	movq	%rax, %r15
	movq	_g_nthreads(%rip), %rbx
	movq	%rsp, %rcx
	leaq	15(,%rbx,8), %rax
	andq	$-16, %rax
	subq	%rax, %rcx
	movq	%rcx, -72(%rbp)         ## 8-byte Spill
	movq	%rcx, %rsp
	imulq	$56, %rbx, %rdi
	callq	_malloc
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	testq	%rbx, %rbx
	je	LBB23_8
## BB#1:                                ## %.lr.ph10
	movq	-80(%rbp), %rax         ## 8-byte Reload
	addq	$40, %rax
	xorl	%ecx, %ecx
	xorps	%xmm0, %xmm0
	.align	4, 0x90
LBB23_2:                                ## =>This Inner Loop Header: Depth=1
	movaps	%xmm0, -64(%rbp)
	movq	$0, -40(%rax)
	movq	%r13, -32(%rax)
	movq	%r15, -24(%rax)
	movl	%r12d, -16(%rax)
	movq	%rcx, -8(%rax)
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%rsi, 8(%rax)
	movq	%rdx, (%rax)
	incq	%rcx
	addq	$56, %rax
	cmpq	%rbx, %rcx
	jb	LBB23_2
## BB#3:                                ## %.preheader4
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	LBB23_8
## BB#4:
	leaq	_ScalarMul(%rip), %r12
	movq	-80(%rbp), %r13         ## 8-byte Reload
	movq	-72(%rbp), %rbx         ## 8-byte Reload
	.align	4, 0x90
LBB23_5:                                ## %.lr.ph7
                                        ## =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r13, %rcx
	callq	_pthread_create
	testl	%eax, %eax
	jne	LBB23_10
## BB#6:                                ##   in Loop: Header=BB23_5 Depth=1
	incq	%r14
	movq	_g_nthreads(%rip), %rax
	addq	$8, %rbx
	addq	$56, %r13
	cmpq	%rax, %r14
	jb	LBB23_5
## BB#7:                                ## %.preheader
	xorl	%ebx, %ebx
	testq	%rax, %rax
	je	LBB23_8
	.align	4, 0x90
LBB23_13:                               ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	movq	-72(%rbp), %rax         ## 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	xorl	%esi, %esi
	callq	_pthread_join
	testl	%eax, %eax
	jne	LBB23_14
## BB#12:                               ##   in Loop: Header=BB23_13 Depth=1
	incq	%rbx
	cmpq	_g_nthreads(%rip), %rbx
	jb	LBB23_13
LBB23_8:                                ## %._crit_edge
	movq	-80(%rbp), %rdi         ## 8-byte Reload
	callq	_free
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	cmpq	-48(%rbp), %rax
	jne	LBB23_9
## BB#15:                               ## %._crit_edge
	movq	%r15, %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB23_10:
	leaq	L_.str5(%rip), %rdi
	jmp	LBB23_11
LBB23_14:
	leaq	L_.str6(%rip), %rdi
LBB23_11:
	callq	_perror
	movl	$1, %edi
	callq	_exit
LBB23_9:                                ## %._crit_edge
	callq	___stack_chk_fail
	.cfi_endproc

	.globl	_matrix_add
	.align	4, 0x90
_matrix_add:                            ## @matrix_add
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp128:
	.cfi_def_cfa_offset 16
Ltmp129:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp130:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
Ltmp131:
	.cfi_offset %rbx, -48
Ltmp132:
	.cfi_offset %r12, -40
Ltmp133:
	.cfi_offset %r14, -32
Ltmp134:
	.cfi_offset %r15, -24
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	_g_elements(%rip), %r12
	movl	$4, %esi
	movq	%r12, %rdi
	callq	_calloc
	testq	%r12, %r12
	jle	LBB24_8
## BB#1:                                ## %.lr.ph.preheader
	xorl	%edx, %edx
	testq	%r12, %r12
	je	LBB24_7
## BB#2:                                ## %overflow.checked
	xorl	%edx, %edx
	movq	%r12, %rcx
	andq	$-8, %rcx
	je	LBB24_6
## BB#3:                                ## %vector.body.preheader
	movq	%rax, %rdx
	addq	$16, %rdx
	leaq	16(%r14), %rsi
	leaq	16(%r15), %rdi
	movq	%r12, %rbx
	andq	$-8, %rbx
	.align	4, 0x90
LBB24_4:                                ## %vector.body
                                        ## =>This Inner Loop Header: Depth=1
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	-16(%rsi), %xmm2
	movdqu	(%rsi), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rdx)
	movdqu	%xmm3, (%rdx)
	addq	$32, %rdx
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-8, %rbx
	jne	LBB24_4
## BB#5:
	movq	%rcx, %rdx
LBB24_6:                                ## %middle.block
	cmpq	%rdx, %r12
	je	LBB24_8
	.align	4, 0x90
LBB24_7:                                ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	movl	(%r14,%rdx,4), %ecx
	addl	(%r15,%rdx,4), %ecx
	movl	%ecx, (%rax,%rdx,4)
	incq	%rdx
	cmpq	%r12, %rdx
	jl	LBB24_7
LBB24_8:                                ## %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_matrix_mul
	.align	4, 0x90
_matrix_mul:                            ## @matrix_mul
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp135:
	.cfi_def_cfa_offset 16
Ltmp136:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp137:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp138:
	.cfi_offset %rbx, -56
Ltmp139:
	.cfi_offset %r12, -48
Ltmp140:
	.cfi_offset %r13, -40
Ltmp141:
	.cfi_offset %r14, -32
Ltmp142:
	.cfi_offset %r15, -24
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	movq	_g_elements(%rip), %rdi
	movl	$4, %esi
	callq	_calloc
	movq	%rax, %r15
	movq	_g_nthreads(%rip), %rbx
	movq	%rsp, %rcx
	leaq	15(,%rbx,8), %rax
	andq	$-16, %rax
	subq	%rax, %rcx
	movq	%rcx, -72(%rbp)         ## 8-byte Spill
	movq	%rcx, %rsp
	imulq	$56, %rbx, %rdi
	callq	_malloc
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	testq	%rbx, %rbx
	je	LBB25_8
## BB#1:                                ## %.lr.ph10
	movq	-80(%rbp), %rax         ## 8-byte Reload
	addq	$40, %rax
	xorl	%ecx, %ecx
	xorps	%xmm0, %xmm0
	.align	4, 0x90
LBB25_2:                                ## =>This Inner Loop Header: Depth=1
	movaps	%xmm0, -64(%rbp)
	movq	%r13, -40(%rax)
	movq	%r12, -32(%rax)
	movq	%r15, -24(%rax)
	movl	$0, -16(%rax)
	movq	%rcx, -8(%rax)
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%rsi, 8(%rax)
	movq	%rdx, (%rax)
	incq	%rcx
	addq	$56, %rax
	cmpq	%rbx, %rcx
	jb	LBB25_2
## BB#3:                                ## %.preheader4
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	LBB25_8
## BB#4:
	leaq	_MatrixMul(%rip), %r12
	movq	-80(%rbp), %r13         ## 8-byte Reload
	movq	-72(%rbp), %rbx         ## 8-byte Reload
	.align	4, 0x90
LBB25_5:                                ## %.lr.ph7
                                        ## =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r13, %rcx
	callq	_pthread_create
	testl	%eax, %eax
	jne	LBB25_10
## BB#6:                                ##   in Loop: Header=BB25_5 Depth=1
	incq	%r14
	movq	_g_nthreads(%rip), %rax
	addq	$8, %rbx
	addq	$56, %r13
	cmpq	%rax, %r14
	jb	LBB25_5
## BB#7:                                ## %.preheader
	xorl	%ebx, %ebx
	testq	%rax, %rax
	je	LBB25_8
	.align	4, 0x90
LBB25_13:                               ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	movq	-72(%rbp), %rax         ## 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	xorl	%esi, %esi
	callq	_pthread_join
	testl	%eax, %eax
	jne	LBB25_14
## BB#12:                               ##   in Loop: Header=BB25_13 Depth=1
	incq	%rbx
	cmpq	_g_nthreads(%rip), %rbx
	jb	LBB25_13
LBB25_8:                                ## %._crit_edge
	movq	-80(%rbp), %rdi         ## 8-byte Reload
	callq	_free
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	cmpq	-48(%rbp), %rax
	jne	LBB25_9
## BB#15:                               ## %._crit_edge
	movq	%r15, %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB25_10:
	leaq	L_.str5(%rip), %rdi
	jmp	LBB25_11
LBB25_14:
	leaq	L_.str6(%rip), %rdi
LBB25_11:
	callq	_perror
	movl	$1, %edi
	callq	_exit
LBB25_9:                                ## %._crit_edge
	callq	___stack_chk_fail
	.cfi_endproc

	.globl	_matrix_pow
	.align	4, 0x90
_matrix_pow:                            ## @matrix_pow
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp143:
	.cfi_def_cfa_offset 16
Ltmp144:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp145:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
Ltmp146:
	.cfi_offset %rbx, -48
Ltmp147:
	.cfi_offset %r12, -40
Ltmp148:
	.cfi_offset %r14, -32
Ltmp149:
	.cfi_offset %r15, -24
	movl	%esi, %ebx
	movq	%rdi, %r14
	movq	_g_elements(%rip), %r15
	movl	$4, %esi
	movq	%r15, %rdi
	callq	_calloc
	movq	%rax, %r12
	movq	_g_width(%rip), %rax
	testq	%rax, %rax
	jle	LBB26_9
## BB#1:                                ## %.lr.ph.i.preheader
	xorl	%edx, %edx
	testq	%rax, %rax
	je	LBB26_7
## BB#2:                                ## %overflow.checked
	xorl	%edx, %edx
	movq	%rax, %r8
	andq	$-2, %r8
	je	LBB26_6
## BB#3:                                ## %vector.body.preheader
	leaq	8(,%rax,8), %rdx
	movq	%rax, %rsi
	andq	$-2, %rsi
	xorl	%edi, %edi
	movq	%r12, %rcx
	.align	4, 0x90
LBB26_4:                                ## %vector.body
                                        ## =>This Inner Loop Header: Depth=1
	movl	$1, (%rcx)
	movl	$1, 4(%rcx,%rax,4)
	addq	$2, %rdi
	addq	%rdx, %rcx
	cmpq	%rdi, %rsi
	jne	LBB26_4
## BB#5:
	movq	%r8, %rdx
LBB26_6:                                ## %middle.block
	cmpq	%rdx, %rax
	je	LBB26_9
LBB26_7:                                ## %.lr.ph.i.preheader3
	movq	%rax, %rcx
	subq	%rdx, %rcx
	leaq	1(%rax), %rsi
	imulq	%rdx, %rsi
	leaq	(%r12,%rsi,4), %rdx
	leaq	4(,%rax,4), %rax
	.align	4, 0x90
LBB26_8:                                ## %.lr.ph.i
                                        ## =>This Inner Loop Header: Depth=1
	movl	$1, (%rdx)
	addq	%rax, %rdx
	decq	%rcx
	jne	LBB26_8
LBB26_9:                                ## %identity_matrix.exit
	testl	%ebx, %ebx
	je	LBB26_15
## BB#10:                               ## %identity_matrix.exit
	cmpl	$1, %ebx
	jne	LBB26_11
## BB#13:
	movl	$4, %esi
	movq	%r15, %rdi
	callq	_calloc
	movq	%rax, %r12
	testq	%r15, %r15
	jle	LBB26_15
## BB#14:                               ## %.lr.ph.i1
	cmpq	$1, %r15
	leaq	(,%r15,4), %rax
	movl	$4, %edx
	cmovgq	%rax, %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_memcpy
LBB26_15:                               ## %cloned.exit
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB26_11:                               ## %identity_matrix.exit
	cmpl	$2, %ebx
	jne	LBB26_16
## BB#12:
	movq	%r14, %rdi
	movq	%r14, %rsi
	jmp	LBB26_19
LBB26_16:
	testb	$1, %bl
	jne	LBB26_17
## BB#20:
	shrl	%ebx
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	_matrix_pow
	movq	%rax, %rdi
	jmp	LBB26_18
LBB26_17:
	decl	%ebx
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	_matrix_pow
	movq	%r14, %rdi
LBB26_18:
	movq	%rax, %rsi
LBB26_19:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_matrix_mul             ## TAILCALL
	.cfi_endproc

	.globl	_get_sum
	.align	4, 0x90
_get_sum:                               ## @get_sum
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp150:
	.cfi_def_cfa_offset 16
Ltmp151:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp152:
	.cfi_def_cfa_register %rbp
	movq	_g_elements(%rip), %rsi
	movl	-4(%rdi,%rsi,4), %eax
	cmpq	$1, _g_width(%rip)
	je	LBB27_9
## BB#1:
	movq	%rsi, %r10
	addq	$-2, %r10
	js	LBB27_9
## BB#2:                                ## %overflow.checked
	movq	%r10, %r9
	shrq	$2, %r9
	incq	%r9
	movabsq	$9223372036854775806, %r8 ## imm = 0x7FFFFFFFFFFFFFFE
	movq	%r9, %rdx
	andq	%r8, %rdx
	xorl	%ecx, %ecx
	andq	%r9, %r8
	je	LBB27_3
## BB#4:                                ## %vector.body.preheader
	shlq	$2, %rdx
	subq	%rdx, %r10
	leaq	-8(%rdi,%rsi,4), %rcx
	addq	$-2, %rsi
	shrq	$2, %rsi
	incq	%rsi
	andq	$-2, %rsi
	xorl	%edx, %edx
	.align	4, 0x90
LBB27_5:                                ## %vector.body
                                        ## =>This Inner Loop Header: Depth=1
	addl	(%rcx), %eax
	addl	-16(%rcx), %edx
	addl	-4(%rcx), %eax
	addl	-20(%rcx), %edx
	addl	-8(%rcx), %eax
	addl	-24(%rcx), %edx
	addl	-12(%rcx), %eax
	addl	-28(%rcx), %edx
	addq	$-32, %rcx
	addq	$-2, %rsi
	jne	LBB27_5
## BB#6:
	movq	%r8, %rcx
	jmp	LBB27_7
LBB27_3:
	xorl	%edx, %edx
LBB27_7:                                ## %middle.block
	addl	%edx, %eax
	cmpq	%rcx, %r9
	je	LBB27_9
	.align	4, 0x90
LBB27_8:                                ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	addl	(%rdi,%r10,4), %eax
	addl	-4(%rdi,%r10,4), %eax
	addl	-8(%rdi,%r10,4), %eax
	addl	-12(%rdi,%r10,4), %eax
	addq	$-4, %r10
	jns	LBB27_8
LBB27_9:                                ## %.loopexit
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_get_trace
	.align	4, 0x90
_get_trace:                             ## @get_trace
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp153:
	.cfi_def_cfa_offset 16
Ltmp154:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp155:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
Ltmp156:
	.cfi_offset %rbx, -40
Ltmp157:
	.cfi_offset %r14, -32
Ltmp158:
	.cfi_offset %r15, -24
	movq	_g_height(%rip), %r11
	xorl	%eax, %eax
	cmpq	$2, %r11
	jl	LBB28_8
## BB#1:                                ## %.lr.ph
	leaq	-1(%r11), %r8
	shrq	%r8
	incq	%r8
	movq	%r8, %r9
	andq	$-2, %r9
	xorl	%esi, %esi
	movq	%r8, %r10
	andq	$-2, %r10
	movq	_g_width(%rip), %rdx
	movl	$0, %ebx
	movl	$0, %eax
	movl	$0, %ecx
	je	LBB28_5
## BB#2:                                ## %vector.body.preheader
	addq	%r9, %r9
	leaq	(,%rdx,4), %rax
	leaq	(%rax,%rax,2), %r14
	movq	%rdx, %r15
	shlq	$4, %r15
	addq	$16, %r15
	xorl	%eax, %eax
	movq	%r10, %rsi
	movq	%rdi, %rbx
	xorl	%ecx, %ecx
	.align	4, 0x90
LBB28_3:                                ## %vector.body
                                        ## =>This Inner Loop Header: Depth=1
	addl	(%rbx), %eax
	addl	8(%rbx,%rdx,8), %ecx
	addl	4(%rbx,%rdx,4), %eax
	addl	12(%r14,%rbx), %ecx
	addq	%r15, %rbx
	addq	$-2, %rsi
	jne	LBB28_3
## BB#4:
	movq	%r9, %rsi
	movq	%r10, %rbx
LBB28_5:                                ## %middle.block
	addl	%ecx, %eax
	cmpq	%rbx, %r8
	je	LBB28_8
## BB#6:                                ## %scalar.ph.preheader
	leaq	1(%rsi), %rcx
	imulq	%rdx, %rcx
	leaq	(,%rsi,4), %rbx
	leaq	4(%rbx,%rcx,4), %rcx
	leaq	8(,%rdx,8), %rbx
	incq	%rdx
	imulq	%rsi, %rdx
	.align	4, 0x90
LBB28_7:                                ## %scalar.ph
                                        ## =>This Inner Loop Header: Depth=1
	addl	(%rdi,%rdx,4), %eax
	addl	(%rcx,%rdi), %eax
	addq	$2, %rsi
	addq	%rbx, %rdi
	cmpq	%r11, %rsi
	jl	LBB28_7
LBB28_8:                                ## %.loopexit
                                        ## kill: EAX<def> EAX<kill> RAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_get_minimum
	.align	4, 0x90
_get_minimum:                           ## @get_minimum
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp159:
	.cfi_def_cfa_offset 16
Ltmp160:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp161:
	.cfi_def_cfa_register %rbp
	movq	_g_elements(%rip), %rcx
	movl	-4(%rdi,%rcx,4), %eax
	cmpq	$1, _g_width(%rip)
	je	LBB29_5
## BB#1:
	testq	%rcx, %rcx
	jle	LBB29_5
## BB#2:                                ## %.lr.ph.preheader
	addq	$-5, %rcx
	movl	%eax, %edx
	jmp	LBB29_3
	.align	4, 0x90
LBB29_6:                                ## %._crit_edge
                                        ##   in Loop: Header=BB29_3 Depth=1
	movl	(%rdi,%rcx,4), %eax
	addq	$-4, %rcx
LBB29_3:                                ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	cmpl	%edx, %eax
	cmovbl	%eax, %edx
	movl	12(%rdi,%rcx,4), %eax
	cmpl	%edx, %eax
	cmovbl	%eax, %edx
	movl	8(%rdi,%rcx,4), %eax
	cmpl	%edx, %eax
	cmovbl	%eax, %edx
	movl	4(%rdi,%rcx,4), %eax
	cmpl	%edx, %eax
	cmovbl	%eax, %edx
	testq	%rcx, %rcx
	jns	LBB29_6
## BB#4:
	movl	%edx, %eax
LBB29_5:                                ## %.loopexit
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_get_maximum
	.align	4, 0x90
_get_maximum:                           ## @get_maximum
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp162:
	.cfi_def_cfa_offset 16
Ltmp163:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp164:
	.cfi_def_cfa_register %rbp
	movq	_g_elements(%rip), %rcx
	movl	-4(%rdi,%rcx,4), %eax
	cmpq	$1, _g_width(%rip)
	je	LBB30_5
## BB#1:
	testq	%rcx, %rcx
	jle	LBB30_5
## BB#2:                                ## %.lr.ph.preheader
	addq	$-5, %rcx
	movl	%eax, %edx
	jmp	LBB30_3
	.align	4, 0x90
LBB30_6:                                ## %._crit_edge
                                        ##   in Loop: Header=BB30_3 Depth=1
	movl	(%rdi,%rcx,4), %eax
	addq	$-4, %rcx
LBB30_3:                                ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	cmpl	%edx, %eax
	cmoval	%eax, %edx
	movl	12(%rdi,%rcx,4), %eax
	cmpl	%edx, %eax
	cmoval	%eax, %edx
	movl	8(%rdi,%rcx,4), %eax
	cmpl	%edx, %eax
	cmoval	%eax, %edx
	movl	4(%rdi,%rcx,4), %eax
	cmpl	%edx, %eax
	cmoval	%eax, %edx
	testq	%rcx, %rcx
	jns	LBB30_6
## BB#4:
	movl	%edx, %eax
LBB30_5:                                ## %.loopexit
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_get_frequency
	.align	4, 0x90
_get_frequency:                         ## @get_frequency
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp165:
	.cfi_def_cfa_offset 16
Ltmp166:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp167:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp168:
	.cfi_offset %rbx, -56
Ltmp169:
	.cfi_offset %r12, -48
Ltmp170:
	.cfi_offset %r13, -40
Ltmp171:
	.cfi_offset %r14, -32
Ltmp172:
	.cfi_offset %r15, -24
	movl	%esi, %r12d
	movq	%rdi, %r13
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	movq	_g_nthreads(%rip), %r14
	leaq	(,%r14,4), %rsi
	movl	$1, %edi
	callq	_calloc
	movq	%rax, %r15
	movq	%rsp, %rcx
	leaq	15(,%r14,8), %rax
	andq	$-16, %rax
	subq	%rax, %rcx
	movq	%rcx, -72(%rbp)         ## 8-byte Spill
	movq	%rcx, %rsp
	imulq	$56, %r14, %rdi
	callq	_malloc
	xorl	%ebx, %ebx
	testq	%r14, %r14
	je	LBB31_17
## BB#1:                                ## %.lr.ph15
	movq	%rax, %rdi
	addq	$40, %rdi
	xorl	%ecx, %ecx
	xorps	%xmm0, %xmm0
	.align	4, 0x90
LBB31_2:                                ## =>This Inner Loop Header: Depth=1
	movaps	%xmm0, -64(%rbp)
	movq	$0, -40(%rdi)
	movq	%r13, -32(%rdi)
	movq	%r15, -24(%rdi)
	movl	%r12d, -16(%rdi)
	movq	%rcx, -8(%rdi)
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%rsi, 8(%rdi)
	movq	%rdx, (%rdi)
	incq	%rcx
	addq	$56, %rdi
	cmpq	%r14, %rcx
	jb	LBB31_2
## BB#3:                                ## %.preheader6
	xorl	%ebx, %ebx
	testq	%r14, %r14
	je	LBB31_17
## BB#4:
	leaq	_GetFreq(%rip), %r12
	movq	%rax, %r13
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	movq	-72(%rbp), %r14         ## 8-byte Reload
	.align	4, 0x90
LBB31_5:                                ## %.lr.ph12
                                        ## =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rcx
	callq	_pthread_create
	testl	%eax, %eax
	jne	LBB31_11
## BB#6:                                ##   in Loop: Header=BB31_5 Depth=1
	incq	%rbx
	movq	_g_nthreads(%rip), %rax
	addq	$8, %r14
	addq	$56, %r13
	cmpq	%rax, %rbx
	jb	LBB31_5
## BB#7:                                ## %.preheader5
	xorl	%ebx, %ebx
	testq	%rax, %rax
	movq	-80(%rbp), %rax         ## 8-byte Reload
	je	LBB31_17
## BB#8:
	movq	%rax, %r14
	movq	-72(%rbp), %r12         ## 8-byte Reload
	.align	4, 0x90
LBB31_9:                                ## %.lr.ph10
                                        ## =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbx,8), %rdi
	xorl	%esi, %esi
	callq	_pthread_join
	testl	%eax, %eax
	jne	LBB31_10
## BB#13:                               ##   in Loop: Header=BB31_9 Depth=1
	incq	%rbx
	movq	_g_nthreads(%rip), %rdx
	cmpq	%rdx, %rbx
	jb	LBB31_9
## BB#14:                               ## %.preheader
	xorl	%ebx, %ebx
	testq	%rdx, %rdx
	movq	%r14, %rax
	je	LBB31_17
## BB#15:                               ## %.lr.ph.preheader
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	.align	4, 0x90
LBB31_16:                               ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	addl	(%r15,%rcx,4), %ebx
	addl	4(%r15,%rcx,4), %ebx
	addl	8(%r15,%rcx,4), %ebx
	addl	12(%r15,%rcx,4), %ebx
	addq	$4, %rcx
	cmpq	%rdx, %rcx
	jb	LBB31_16
LBB31_17:                               ## %._crit_edge
	movq	%rax, %rdi
	callq	_free
	movq	%r15, %rdi
	callq	_free
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	cmpq	-48(%rbp), %rax
	jne	LBB31_19
## BB#18:                               ## %._crit_edge
	movl	%ebx, %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB31_11:
	leaq	L_.str5(%rip), %rdi
	jmp	LBB31_12
LBB31_10:
	leaq	L_.str6(%rip), %rdi
LBB31_12:
	callq	_perror
	movl	$1, %edi
	callq	_exit
LBB31_19:                               ## %._crit_edge
	callq	___stack_chk_fail
	.cfi_endproc

.zerofill __DATA,__bss,_g_seed,4,2      ## @g_seed
	.section	__DATA,__data
	.align	3                       ## @g_nthreads
_g_nthreads:
	.quad	1                       ## 0x1

.zerofill __DATA,__bss,_g_width,8,3     ## @g_width
.zerofill __DATA,__bss,_g_height,8,3    ## @g_height
.zerofill __DATA,__bss,_g_elements,8,3  ## @g_elements
	.section	__TEXT,__cstring,cstring_literals
L_.str1:                                ## @.str1
	.asciz	"%u "

L_.str3:                                ## @.str3
	.asciz	"%u"

L_.str4:                                ## @.str4
	.asciz	"%u\n"

L_.str5:                                ## @.str5
	.asciz	"pthread_create failed"

L_.str6:                                ## @.str6
	.asciz	"pthread_join failed"


.subsections_via_symbols
