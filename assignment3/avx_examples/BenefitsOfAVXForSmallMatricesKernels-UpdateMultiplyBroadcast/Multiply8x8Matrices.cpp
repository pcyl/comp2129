
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <immintrin.h>

using namespace std;

typedef struct msBuffer	{
	unsigned uiBufferWidth;
	unsigned uiBufferHeight;
} MSBUFFER;

MSBUFFER msBufferParams1, msBufferParams2;

float* InputBuffer1 = NULL;
float* InputBuffer2 = NULL;
float* OutputBuffer = NULL;
float* ReferenceBuffer = NULL;
int loopCount = 1;
int matrixCount = 2;

//The SetKernelParameters function is passing the kernel the required buffer parameters (Width & Height)
void SetKernelParameters(int argc, char* argv[])
{
	const int matrixSize = 8;

	msBufferParams1.uiBufferWidth = matrixSize;
	msBufferParams1.uiBufferHeight = matrixSize;
	msBufferParams2.uiBufferWidth = matrixSize;
	msBufferParams2.uiBufferHeight = matrixSize;

	if(argc == 2)
	{
		loopCount = atoi(argv[1]);
	}
	else if(argc > 2)
	{//first two parameters are the exe name and the kernel number
		cout << "Multiply8x8Matrices Usage" << endl;
		cout << " No command line arguments are required." << endl;
		cout << " The optional argument = number of times to run the 8x8 matrix multiplications." << endl;
		exit(1);
	}
	return;
}

//Memory allocations could be made in the AllocateBuffers function
int AllocateBuffers(int AlignmentBytes)
{
	//calculate necessary buffer size
	unsigned iBufferSizeBytes = msBufferParams1.uiBufferWidth*msBufferParams1.uiBufferHeight*sizeof(float);

	//Input 1 is NxM
	InputBuffer1 = (float*) _mm_malloc(iBufferSizeBytes, AlignmentBytes); //should be 32bytes alignment for AVX

	//Input 2 is MxK
	iBufferSizeBytes = msBufferParams2.uiBufferWidth*msBufferParams2.uiBufferHeight*sizeof(float);
	InputBuffer2 = (float*) _mm_malloc(iBufferSizeBytes, AlignmentBytes); //should be 32bytes alignment for AVX

	//Output is NxK
	iBufferSizeBytes = msBufferParams2.uiBufferWidth*msBufferParams1.uiBufferHeight*sizeof(float);
	OutputBuffer = (float*) _mm_malloc(iBufferSizeBytes, AlignmentBytes); //should be 32bytes alignment for AVX
	ReferenceBuffer = (float*) _mm_malloc(iBufferSizeBytes, AlignmentBytes); //should be 32bytes alignment for AVX

	return 0;
}

//Memory free could be made in the DeAllocateBuffers function
int DeAllocateBuffers()
{
	//free allocated buffers
	_mm_free(InputBuffer1);
	_mm_free(InputBuffer2);
	_mm_free(OutputBuffer);
	_mm_free(ReferenceBuffer);
	return 0;
}

//The KernelInit function will be called before ProcessOptimizedKernel is called
//(It will be called more then once when generating traces - warm up )
void KernelInit()
{
	unsigned int iBufferWidth1 = msBufferParams1.uiBufferWidth;
	unsigned int iBufferHeight1 = msBufferParams1.uiBufferHeight;
	unsigned int iBufferWidth2 = msBufferParams2.uiBufferWidth;
	unsigned int iBufferHeight2 = msBufferParams2.uiBufferHeight;

	srand ((unsigned)time(NULL));

	for (int i = 0; i < iBufferHeight1; i++)	{
		for (int j = 0; j < iBufferWidth1; j++)	{
			InputBuffer1[i*iBufferWidth1 + j] = rand() %10;
		}
	}

	//Initialize the additional matrix
	for (int i = 0; i < iBufferHeight2; i++)	{
		for (int j = 0; j < iBufferWidth2; j++)	{
			InputBuffer2[i*iBufferWidth2 + j] = rand() %10;
		}
	}
}

void multiplyMatrix(float * const pInput1, float * const pInput2, float * pOutput, int numberRows1, int numberColumns1, int numberRows2, int numberColumns2)	{

	if(numberColumns1 != numberRows2)	{
		cout << "Error: Cannot perform matrix multiplication because columns do not equal rows" << endl;
		return;
	}

	int storeCount = 0;
	int loadCount = 0;
	int dotProducts = 0;
	for(int i = 0; i < numberRows1; i++)	{
		for(int j = 0; j < numberColumns2; j++)	{
			float dotProduct;

			dotProduct = 0.0;
			for(int column = 0; column < numberColumns1; column++)	{
				dotProduct += (*(pInput1+(i*numberColumns1)+column)) * (*(pInput2+(column*numberColumns2)+j));
				loadCount += 2;
				dotProducts++;
			}

			//Store the dot product in the results matrix
			*(pOutput+(i*numberColumns2)+j)= dotProduct;
			storeCount++;
		}
	}
}

//The Intel(R) AVX version of 8x8 matrix multiply using broadcast
void avx_smallMatrices_broadcast()
{
	__m256	ymm0, ymm1, ymm2, ymm3, ymm4, ymm5, ymm6, ymm7,
			ymm8, ymm9, ymm10, ymm11, ymm12, ymm13, ymm14, ymm15;

	//Initialize some pointers
	float * pMatrix2 = InputBuffer2;
	float * pIn = InputBuffer1;
	float * pOut = OutputBuffer;

	pMatrix2 = InputBuffer2;
	pIn = InputBuffer1;
	pOut = OutputBuffer;

	//Read the eight rows of Matrix B into ymm registers
	ymm8 = _mm256_load_ps((float *) (pMatrix2));
	ymm9 = _mm256_load_ps((float *) (pMatrix2 + 1*8));
	ymm10 = _mm256_load_ps((float *) (pMatrix2 + 2*8));
	ymm11 = _mm256_load_ps((float *) (pMatrix2 + 3*8));
	ymm12 = _mm256_load_ps((float *) (pMatrix2 + 4*8));
	ymm13 = _mm256_load_ps((float *) (pMatrix2 + 5*8));
	ymm14 = _mm256_load_ps((float *) (pMatrix2 + 6*8));
	ymm15 = _mm256_load_ps((float *) (pMatrix2 + 7*8));

	//Broadcast each element of Matrix A Row 1 into a ymm register
	ymm0 = _mm256_broadcast_ss(pIn);
	ymm1 = _mm256_broadcast_ss(pIn + 1);
	ymm2 = _mm256_broadcast_ss(pIn + 2);
	ymm3 = _mm256_broadcast_ss(pIn + 3);
	ymm4 = _mm256_broadcast_ss(pIn + 4);
	ymm5 = _mm256_broadcast_ss(pIn + 5);
	ymm6 = _mm256_broadcast_ss(pIn + 6);
	ymm7 = _mm256_broadcast_ss(pIn + 7);

	//Multiply A11 times Row 1 of Matrix B
	ymm0 = _mm256_mul_ps(ymm0, ymm8);

	//Multiply A12 times Row 2 of Matrix B
	ymm1 = _mm256_mul_ps(ymm1, ymm9);

	//Create the first partial sum
	ymm0 = _mm256_add_ps(ymm0, ymm1);

	//Repeat for A13, A14, and Rows 3, 4 of Matrix B
	ymm2 = _mm256_mul_ps(ymm2, ymm10);
	ymm3 = _mm256_mul_ps(ymm3, ymm11);
	ymm2 = _mm256_add_ps(ymm2, ymm3);

	//Repeat for A15, A16, and Rows 5, 6 of Matrix B
	ymm4 = _mm256_mul_ps(ymm4, ymm12);
	ymm5 = _mm256_mul_ps(ymm5, ymm13);
	ymm4 = _mm256_add_ps(ymm4, ymm5);

	//Repeat for A17, A18, and Rows 7, 8 of Matrix B
	ymm6 = _mm256_mul_ps(ymm6, ymm14);
	ymm7 = _mm256_mul_ps(ymm7, ymm15);
	ymm6 = _mm256_add_ps(ymm6, ymm7);

	//Perform the final three adds
	ymm0 = _mm256_add_ps(ymm0, ymm2);
	ymm4 = _mm256_add_ps(ymm4, ymm6);
	ymm0 = _mm256_add_ps(ymm0, ymm4);

	//Store the result to Row 1 of Matrix C
	_mm256_store_ps((float *) (pOut), ymm0);

	//Repeat using Matrix A Row 2
	ymm0 = _mm256_broadcast_ss(pIn + 1*8);
	ymm1 = _mm256_broadcast_ss(pIn + 1*8 + 1);
	ymm2 = _mm256_broadcast_ss(pIn + 1*8 + 2);
	ymm3 = _mm256_broadcast_ss(pIn + 1*8 + 3);
	ymm4 = _mm256_broadcast_ss(pIn + 1*8 + 4);
	ymm5 = _mm256_broadcast_ss(pIn + 1*8 + 5);
	ymm6 = _mm256_broadcast_ss(pIn + 1*8 + 6);
	ymm7 = _mm256_broadcast_ss(pIn + 1*8 + 7);
	ymm0 = _mm256_mul_ps(ymm0, ymm8);
	ymm1 = _mm256_mul_ps(ymm1, ymm9);
	ymm0 = _mm256_add_ps(ymm0, ymm1);
	ymm2 = _mm256_mul_ps(ymm2, ymm10);
	ymm3 = _mm256_mul_ps(ymm3, ymm11);
	ymm2 = _mm256_add_ps(ymm2, ymm3);
	ymm4 = _mm256_mul_ps(ymm4, ymm12);
	ymm5 = _mm256_mul_ps(ymm5, ymm13);
	ymm4 = _mm256_add_ps(ymm4, ymm5);
	ymm6 = _mm256_mul_ps(ymm6, ymm14);
	ymm7 = _mm256_mul_ps(ymm7, ymm15);
	ymm6 = _mm256_add_ps(ymm6, ymm7);
	ymm0 = _mm256_add_ps(ymm0, ymm2);
	ymm4 = _mm256_add_ps(ymm4, ymm6);
	ymm0 = _mm256_add_ps(ymm0, ymm4);
	_mm256_store_ps((float *) (pOut + 1*8), ymm0);

	//Repeat using Matrix A Row 3
	ymm0 = _mm256_broadcast_ss(pIn + 2*8);
	ymm1 = _mm256_broadcast_ss(pIn + 2*8 + 1);
	ymm2 = _mm256_broadcast_ss(pIn + 2*8 + 2);
	ymm3 = _mm256_broadcast_ss(pIn + 2*8 + 3);
	ymm4 = _mm256_broadcast_ss(pIn + 2*8 + 4);
	ymm5 = _mm256_broadcast_ss(pIn + 2*8 + 5);
	ymm6 = _mm256_broadcast_ss(pIn + 2*8 + 6);
	ymm7 = _mm256_broadcast_ss(pIn + 2*8 + 7);
	ymm0 = _mm256_mul_ps(ymm0, ymm8);
	ymm1 = _mm256_mul_ps(ymm1, ymm9);
	ymm0 = _mm256_add_ps(ymm0, ymm1);
	ymm2 = _mm256_mul_ps(ymm2, ymm10);
	ymm3 = _mm256_mul_ps(ymm3, ymm11);
	ymm2 = _mm256_add_ps(ymm2, ymm3);
	ymm4 = _mm256_mul_ps(ymm4, ymm12);
	ymm5 = _mm256_mul_ps(ymm5, ymm13);
	ymm4 = _mm256_add_ps(ymm4, ymm5);
	ymm6 = _mm256_mul_ps(ymm6, ymm14);
	ymm7 = _mm256_mul_ps(ymm7, ymm15);
	ymm6 = _mm256_add_ps(ymm6, ymm7);
	ymm0 = _mm256_add_ps(ymm0, ymm2);
	ymm4 = _mm256_add_ps(ymm4, ymm6);
	ymm0 = _mm256_add_ps(ymm0, ymm4);
	_mm256_store_ps((float *) (pOut + 2*8), ymm0);

	//Repeat using Matrix A Row 4
	ymm0 = _mm256_broadcast_ss(pIn + 3*8);
	ymm1 = _mm256_broadcast_ss(pIn + 3*8 + 1);
	ymm2 = _mm256_broadcast_ss(pIn + 3*8 + 2);
	ymm3 = _mm256_broadcast_ss(pIn + 3*8 + 3);
	ymm4 = _mm256_broadcast_ss(pIn + 3*8 + 4);
	ymm5 = _mm256_broadcast_ss(pIn + 3*8 + 5);
	ymm6 = _mm256_broadcast_ss(pIn + 3*8 + 6);
	ymm7 = _mm256_broadcast_ss(pIn + 3*8 + 7);
	ymm0 = _mm256_mul_ps(ymm0, ymm8);
	ymm1 = _mm256_mul_ps(ymm1, ymm9);
	ymm0 = _mm256_add_ps(ymm0, ymm1);
	ymm2 = _mm256_mul_ps(ymm2, ymm10);
	ymm3 = _mm256_mul_ps(ymm3, ymm11);
	ymm2 = _mm256_add_ps(ymm2, ymm3);
	ymm4 = _mm256_mul_ps(ymm4, ymm12);
	ymm5 = _mm256_mul_ps(ymm5, ymm13);
	ymm4 = _mm256_add_ps(ymm4, ymm5);
	ymm6 = _mm256_mul_ps(ymm6, ymm14);
	ymm7 = _mm256_mul_ps(ymm7, ymm15);
	ymm6 = _mm256_add_ps(ymm6, ymm7);
	ymm0 = _mm256_add_ps(ymm0, ymm2);
	ymm4 = _mm256_add_ps(ymm4, ymm6);
	ymm0 = _mm256_add_ps(ymm0, ymm4);
	_mm256_store_ps((float *) (pOut + 3*8), ymm0);

	//Repeat using Matrix A Row 5
	ymm0 = _mm256_broadcast_ss(pIn + 4*8);
	ymm1 = _mm256_broadcast_ss(pIn + 4*8 + 1);
	ymm2 = _mm256_broadcast_ss(pIn + 4*8 + 2);
	ymm3 = _mm256_broadcast_ss(pIn + 4*8 + 3);
	ymm4 = _mm256_broadcast_ss(pIn + 4*8 + 4);
	ymm5 = _mm256_broadcast_ss(pIn + 4*8 + 5);
	ymm6 = _mm256_broadcast_ss(pIn + 4*8 + 6);
	ymm7 = _mm256_broadcast_ss(pIn + 4*8 + 7);
	ymm0 = _mm256_mul_ps(ymm0, ymm8);
	ymm1 = _mm256_mul_ps(ymm1, ymm9);
	ymm0 = _mm256_add_ps(ymm0, ymm1);
	ymm2 = _mm256_mul_ps(ymm2, ymm10);
	ymm3 = _mm256_mul_ps(ymm3, ymm11);
	ymm2 = _mm256_add_ps(ymm2, ymm3);
	ymm4 = _mm256_mul_ps(ymm4, ymm12);
	ymm5 = _mm256_mul_ps(ymm5, ymm13);
	ymm4 = _mm256_add_ps(ymm4, ymm5);
	ymm6 = _mm256_mul_ps(ymm6, ymm14);
	ymm7 = _mm256_mul_ps(ymm7, ymm15);
	ymm6 = _mm256_add_ps(ymm6, ymm7);
	ymm0 = _mm256_add_ps(ymm0, ymm2);
	ymm4 = _mm256_add_ps(ymm4, ymm6);
	ymm0 = _mm256_add_ps(ymm0, ymm4);
	_mm256_store_ps((float *) (pOut + 4*8), ymm0);

	//Repeat using Matrix A Row 6
	ymm0 = _mm256_broadcast_ss(pIn + 5*8);
	ymm1 = _mm256_broadcast_ss(pIn + 5*8 + 1);
	ymm2 = _mm256_broadcast_ss(pIn + 5*8 + 2);
	ymm3 = _mm256_broadcast_ss(pIn + 5*8 + 3);
	ymm4 = _mm256_broadcast_ss(pIn + 5*8 + 4);
	ymm5 = _mm256_broadcast_ss(pIn + 5*8 + 5);
	ymm6 = _mm256_broadcast_ss(pIn + 5*8 + 6);
	ymm7 = _mm256_broadcast_ss(pIn + 5*8 + 7);
	ymm0 = _mm256_mul_ps(ymm0, ymm8);
	ymm1 = _mm256_mul_ps(ymm1, ymm9);
	ymm0 = _mm256_add_ps(ymm0, ymm1);
	ymm2 = _mm256_mul_ps(ymm2, ymm10);
	ymm3 = _mm256_mul_ps(ymm3, ymm11);
	ymm2 = _mm256_add_ps(ymm2, ymm3);
	ymm4 = _mm256_mul_ps(ymm4, ymm12);
	ymm5 = _mm256_mul_ps(ymm5, ymm13);
	ymm4 = _mm256_add_ps(ymm4, ymm5);
	ymm6 = _mm256_mul_ps(ymm6, ymm14);
	ymm7 = _mm256_mul_ps(ymm7, ymm15);
	ymm6 = _mm256_add_ps(ymm6, ymm7);
	ymm0 = _mm256_add_ps(ymm0, ymm2);
	ymm4 = _mm256_add_ps(ymm4, ymm6);
	ymm0 = _mm256_add_ps(ymm0, ymm4);
	_mm256_store_ps((float *) (pOut + 5*8), ymm0);

	//Repeat using Matrix A Row 7
	ymm0 = _mm256_broadcast_ss(pIn + 6*8);
	ymm1 = _mm256_broadcast_ss(pIn + 6*8 + 1);
	ymm2 = _mm256_broadcast_ss(pIn + 6*8 + 2);
	ymm3 = _mm256_broadcast_ss(pIn + 6*8 + 3);
	ymm4 = _mm256_broadcast_ss(pIn + 6*8 + 4);
	ymm5 = _mm256_broadcast_ss(pIn + 6*8 + 5);
	ymm6 = _mm256_broadcast_ss(pIn + 6*8 + 6);
	ymm7 = _mm256_broadcast_ss(pIn + 6*8 + 7);
	ymm0 = _mm256_mul_ps(ymm0, ymm8);
	ymm1 = _mm256_mul_ps(ymm1, ymm9);
	ymm0 = _mm256_add_ps(ymm0, ymm1);
	ymm2 = _mm256_mul_ps(ymm2, ymm10);
	ymm3 = _mm256_mul_ps(ymm3, ymm11);
	ymm2 = _mm256_add_ps(ymm2, ymm3);
	ymm4 = _mm256_mul_ps(ymm4, ymm12);
	ymm5 = _mm256_mul_ps(ymm5, ymm13);
	ymm4 = _mm256_add_ps(ymm4, ymm5);
	ymm6 = _mm256_mul_ps(ymm6, ymm14);
	ymm7 = _mm256_mul_ps(ymm7, ymm15);
	ymm6 = _mm256_add_ps(ymm6, ymm7);
	ymm0 = _mm256_add_ps(ymm0, ymm2);
	ymm4 = _mm256_add_ps(ymm4, ymm6);
	ymm0 = _mm256_add_ps(ymm0, ymm4);
	_mm256_store_ps((float *) (pOut + 6*8), ymm0);

	//Repeat using Matrix A Row 8
	ymm0 = _mm256_broadcast_ss(pIn + 7*8);
	ymm1 = _mm256_broadcast_ss(pIn + 7*8 + 1);
	ymm2 = _mm256_broadcast_ss(pIn + 7*8 + 2);
	ymm3 = _mm256_broadcast_ss(pIn + 7*8 + 3);
	ymm4 = _mm256_broadcast_ss(pIn + 7*8 + 4);
	ymm5 = _mm256_broadcast_ss(pIn + 7*8 + 5);
	ymm6 = _mm256_broadcast_ss(pIn + 7*8 + 6);
	ymm7 = _mm256_broadcast_ss(pIn + 7*8 + 7);
	ymm0 = _mm256_mul_ps(ymm0, ymm8);
	ymm1 = _mm256_mul_ps(ymm1, ymm9);
	ymm0 = _mm256_add_ps(ymm0, ymm1);
	ymm2 = _mm256_mul_ps(ymm2, ymm10);
	ymm3 = _mm256_mul_ps(ymm3, ymm11);
	ymm2 = _mm256_add_ps(ymm2, ymm3);
	ymm4 = _mm256_mul_ps(ymm4, ymm12);
	ymm5 = _mm256_mul_ps(ymm5, ymm13);
	ymm4 = _mm256_add_ps(ymm4, ymm5);
	ymm6 = _mm256_mul_ps(ymm6, ymm14);
	ymm7 = _mm256_mul_ps(ymm7, ymm15);
	ymm6 = _mm256_add_ps(ymm6, ymm7);
	ymm0 = _mm256_add_ps(ymm0, ymm2);
	ymm4 = _mm256_add_ps(ymm4, ymm6);
	ymm0 = _mm256_add_ps(ymm0, ymm4);
	_mm256_store_ps((float *) (pOut + 7*8), ymm0);
}

//The Intel(R) SSE version of 8x8 matrix multiply using broadcast (by shuffle)
void sse_smallMatrices_broadcast()
{
	__m128 xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6, xmm7;
	__m128 xmm8, xmm9, xmm10, xmm11, xmm12, xmm13, xmm14, xmm15;
	float * pM1;
	float * pM2;
	float * pOut;

	for(int count = 0; count < loopCount; count++)	{

		pM1 = InputBuffer1;
		pM2 = InputBuffer2;
		pOut = OutputBuffer;

		//******************************
		//Begin calculating output row 0
		//Read the top-left quarter of Matrix 2 (1)
		xmm8 = _mm_load_ps(pM2);			//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 8);		//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 16);	//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 24);	//Four from the fourth row

		//Read the left half of the first row of Matrix 1
		//Broadcast using pshuf
		xmm3 = _mm_load_ps(pM1);
		xmm0 = _mm_shuffle_ps(xmm3, xmm3, 0x00);	//Broadcast element 0
		xmm1 = _mm_shuffle_ps(xmm3, xmm3, 0x55);	//Broadcast element 1
		xmm2 = _mm_shuffle_ps(xmm3, xmm3, 0xaa);	//Broadcast element 2
		xmm3 = _mm_shuffle_ps(xmm3, xmm3, 0xff);	//Broadcast element 3

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 0
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 1
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 2
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 00, 01, 02, 03

		//Read the bottom-left quarter of Matrix 2 (3)
		xmm8 = _mm_load_ps(pM2 + 32);				//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 40);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 48);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 56);				//Four from the fourth row

		//Read the right half of the first row of Matrix 1
		//Broadcast using pshuf
		xmm7 = _mm_load_ps(pM1 + 4);
		xmm4 = _mm_shuffle_ps(xmm7, xmm7, 0x00);	//Broadcast element 0
		xmm5 = _mm_shuffle_ps(xmm7, xmm7, 0x55);	//Broadcast element 1
		xmm6 = _mm_shuffle_ps(xmm7, xmm7, 0xaa);	//Broadcast element 2
		xmm7 = _mm_shuffle_ps(xmm7, xmm7, 0xff);	//Broadcast element 3

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm4, xmm8);				//Output column 0
		xmm9 = _mm_mul_ps(xmm5, xmm9);				//Output column 1
		xmm10 = _mm_mul_ps(xmm6, xmm10);			//Output column 2
		xmm11 = _mm_mul_ps(xmm7, xmm11);			//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 00, 01, 02, 03
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 00, 01, 02, 03

		//Write the results for 00, 01, 02, and 03 (1/16th of the work complete)
		_mm_store_ps(pOut, xmm8);

		//Begin calculating 04, 05, 06, and 07
		//Read the top-right quarter of Matrix 2 (2)
		//Reuse the broadcasts from last section
		xmm8 = _mm_load_ps(pM2 + 4);				//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 12);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 20);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 28);				//Four from the fourth row

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 0
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 1
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 2
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 04, 05, 06, 07

		//Read the bottom-right quarter of Matrix 2 (4)
		//Note the reuse of xmm0-xmm3, they will be reused in the next section too.
		xmm0 = _mm_load_ps(pM2 + 36);			//Four from the first row
		xmm1 = _mm_load_ps(pM2 + 44);			//Four from the second row
		xmm2 = _mm_load_ps(pM2 + 52);			//Four from the third row
		xmm3 = _mm_load_ps(pM2 + 60);			//Four from the fourth row

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm4, xmm0);			//Output column 0
		xmm9 = _mm_mul_ps(xmm5, xmm1);			//Output column 1
		xmm10 = _mm_mul_ps(xmm6, xmm2);			//Output column 2
		xmm11 = _mm_mul_ps(xmm7, xmm3);			//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 04, 05, 06, 07
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 04, 05, 06, 07

		//Write the results for 04, 05, 06, and 07 (2/16th of the work)
		_mm_store_ps(pOut + 4, xmm8);

		//******************************
		//Begin calculating output row 1
		//Since we already have the bottom-right quarter of Matrix 2 (4)
		//We begin by calculating 14, 15, 16, and 17. This saves four loads from M2.
		//Read the right half of the second row of Matrix 1 (4)
		//Broadcast using pshuf
		xmm7 = _mm_load_ps(pM1 + 12);
		xmm4 = _mm_shuffle_ps(xmm7, xmm7, 0x00);	//Broadcast element 0
		xmm5 = _mm_shuffle_ps(xmm7, xmm7, 0x55);	//Broadcast element 1
		xmm6 = _mm_shuffle_ps(xmm7, xmm7, 0xaa);	//Broadcast element 2
		xmm7 = _mm_shuffle_ps(xmm7, xmm7, 0xff);	//Broadcast element 3

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm4, xmm0);				//Output column 4
		xmm9 = _mm_mul_ps(xmm5, xmm1);				//Output column 5
		xmm10 = _mm_mul_ps(xmm6, xmm2);				//Output column 6
		xmm11 = _mm_mul_ps(xmm7, xmm3);				//Output column 7
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 14, 15, 16, 17

		//Read the top-right quarter of Matrix 2 (2)
		xmm8 = _mm_load_ps(pM2 + 4);				//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 12);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 20);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 28);				//Four from the fourth row

		//Read the left half of the second row of Matrix 1 (3)
		//Broadcast using pshuf
		xmm3 = _mm_load_ps(pM1 + 8);
		xmm0 = _mm_shuffle_ps(xmm3, xmm3, 0x00);	//Broadcast element 0
		xmm1 = _mm_shuffle_ps(xmm3, xmm3, 0x55);	//Broadcast element 1
		xmm2 = _mm_shuffle_ps(xmm3, xmm3, 0xaa);	//Broadcast element 2
		xmm3 = _mm_shuffle_ps(xmm3, xmm3, 0xff);	//Broadcast element 3

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 4
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 5
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 6
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 7
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 14, 15, 16, 17
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 14, 15, 16, 17

		//Write the results for 14, 15, 16, and 17 (4) (3/16th of the work complete)
		_mm_store_ps(pOut + 12, xmm8);

		//Begin calculating 10, 11, 12, and 13
		//Read the top-left quarter of Matrix 2 (1)
		//Reuse the broadcasts from last section
		xmm8 = _mm_load_ps(pM2);					//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 8);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 16);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 24);				//Four from the fourth row

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 0
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 1
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 2
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 14, 15, 16, 17

		//Read the bottom-left quarter of Matrix 2 (3)
		xmm0 = _mm_load_ps(pM2 + 32);				//Four from the first row
		xmm1 = _mm_load_ps(pM2 + 40);				//Four from the second row
		xmm2 = _mm_load_ps(pM2 + 48);				//Four from the third row
		xmm3 = _mm_load_ps(pM2 + 56);				//Four from the fourth row

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm4, xmm0);				//Output column 0
		xmm9 = _mm_mul_ps(xmm5, xmm1);				//Output column 1
		xmm10 = _mm_mul_ps(xmm6, xmm2);				//Output column 2
		xmm11 = _mm_mul_ps(xmm7, xmm3);				//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 14, 15, 16, 17
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 14, 15, 16, 17

		//Write the results for 10, 11, 12, and 13 (3) (4/16th of the work)
		_mm_store_ps(pOut + 8, xmm8);

		//******************************
		//Begin calculating output row 2
		//Since we already have the bottom-left quarter of Matrix 2 (3)
		//We begin by calculating 20, 21, 22, and 23. This saves four loads from M2.
		//Read the right half of the third row of Matrix 1 (6)
		//Broadcast using pshuf
		xmm7 = _mm_load_ps(pM1 + 20);
		xmm4 = _mm_shuffle_ps(xmm7, xmm7, 0x00);	//Broadcast element 0
		xmm5 = _mm_shuffle_ps(xmm7, xmm7, 0x55);	//Broadcast element 1
		xmm6 = _mm_shuffle_ps(xmm7, xmm7, 0xaa);	//Broadcast element 2
		xmm7 = _mm_shuffle_ps(xmm7, xmm7, 0xff);	//Broadcast element 3

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm4, xmm0);				//Output column 0
		xmm9 = _mm_mul_ps(xmm5, xmm1);				//Output column 1
		xmm10 = _mm_mul_ps(xmm6, xmm2);				//Output column 2
		xmm11 = _mm_mul_ps(xmm7, xmm3);				//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 20, 21, 22, 23

		//Read the top-left quarter of Matrix 2 (1)
		xmm8 = _mm_load_ps(pM2);					//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 8);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 16);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 24);				//Four from the fourth row

		//Read the left half of the third row of Matrix 1 (5)
		//Broadcast using pshuf
		xmm3 = _mm_load_ps(pM1 + 16);
		xmm0 = _mm_shuffle_ps(xmm3, xmm3, 0x00);	//Broadcast element 0
		xmm1 = _mm_shuffle_ps(xmm3, xmm3, 0x55);	//Broadcast element 1
		xmm2 = _mm_shuffle_ps(xmm3, xmm3, 0xaa);	//Broadcast element 2
		xmm3 = _mm_shuffle_ps(xmm3, xmm3, 0xff);	//Broadcast element 3

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 0
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 1
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 2
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 20, 21, 22, 23
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 20, 21, 22, 23

		//Write the results for 20, 21, 22, and 23 (5) (5/16th of the work complete)
		_mm_store_ps(pOut + 16, xmm8);

		//Begin calculating 24, 25, 26, and 27
		//Read the upper-right quarter of Matrix 2 (2)
		//Reuse the broadcasts from last section
		xmm8 = _mm_load_ps(pM2 + 4);				//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 12);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 20);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 28);				//Four from the fourth row

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 4
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 5
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 6
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 7
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 24, 25, 26, 27

		//Read the bottom-right quarter of Matrix 2 (4)
		xmm0 = _mm_load_ps(pM2 + 36);				//Four from the first row
		xmm1 = _mm_load_ps(pM2 + 44);				//Four from the second row
		xmm2 = _mm_load_ps(pM2 + 52);				//Four from the third row
		xmm3 = _mm_load_ps(pM2 + 60);				//Four from the fourth row

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm4, xmm0);				//Output column 4
		xmm9 = _mm_mul_ps(xmm5, xmm1);				//Output column 5
		xmm10 = _mm_mul_ps(xmm6, xmm2);				//Output column 6
		xmm11 = _mm_mul_ps(xmm7, xmm3);				//Output column 7
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 24, 25, 26, 27
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 24, 25, 26, 27

		//Write the results for 24, 25, 26, and 27 (6) (6/16th of the work)
		_mm_store_ps(pOut + 20, xmm8);

		//******************************
		//Begin calculating output row 3
		//Since we already have the bottom-right quarter of Matrix 2 (4)
		//We begin by calculating 34, 35, 36 and 37. This saves four loads from M2.
		//Read the right half of the fourth row of Matrix 1 (8)
		//Broadcast using pshuf
		xmm7 = _mm_load_ps(pM1 + 28);
		xmm4 = _mm_shuffle_ps(xmm7, xmm7, 0x00);	//Broadcast element 0
		xmm5 = _mm_shuffle_ps(xmm7, xmm7, 0x55);	//Broadcast element 1
		xmm6 = _mm_shuffle_ps(xmm7, xmm7, 0xaa);	//Broadcast element 2
		xmm7 = _mm_shuffle_ps(xmm7, xmm7, 0xff);	//Broadcast element 3

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm4, xmm0);				//Output column 4
		xmm9 = _mm_mul_ps(xmm5, xmm1);				//Output column 5
		xmm10 = _mm_mul_ps(xmm6, xmm2);				//Output column 6
		xmm11 = _mm_mul_ps(xmm7, xmm3);				//Output column 7
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 34, 35, 36, 37

		//Read the top-right quarter of Matrix 2 (2)
		xmm8 = _mm_load_ps(pM2 + 4);				//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 12);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 20);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 28);				//Four from the fourth row

		//Read the left half of the fourth row of Matrix 1 (7)
		//Broadcast using pshuf
		xmm3 = _mm_load_ps(pM1 + 24);
		xmm0 = _mm_shuffle_ps(xmm3, xmm3, 0x00);	//Broadcast element 0
		xmm1 = _mm_shuffle_ps(xmm3, xmm3, 0x55);	//Broadcast element 1
		xmm2 = _mm_shuffle_ps(xmm3, xmm3, 0xaa);	//Broadcast element 2
		xmm3 = _mm_shuffle_ps(xmm3, xmm3, 0xff);	//Broadcast element 3

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 4
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 5
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 6
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 7
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 34, 35, 36, 37
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 34, 35, 36, 37

		//Write the results for 34, 35, 36, 37 (5) (7/16th of the work complete)
		_mm_store_ps(pOut + 28, xmm8);

		//Begin calculating 30, 31, 32, 33
		//Read the upper-left quarter of Matrix 2 (1)
		//Reuse the broadcasts from last section
		xmm8 = _mm_load_ps(pM2);					//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 8);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 16);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 24);				//Four from the fourth row

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 0
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 1
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 2
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 30, 31, 32, 33

		//Read the bottom-left quarter of Matrix 2 (3)
		xmm0 = _mm_load_ps(pM2 + 32);				//Four from the first row
		xmm1 = _mm_load_ps(pM2 + 40);				//Four from the second row
		xmm2 = _mm_load_ps(pM2 + 48);				//Four from the third row
		xmm3 = _mm_load_ps(pM2 + 56);				//Four from the fourth row

		//Multiply and add
		xmm8 = _mm_mul_ps(xmm4, xmm0);				//Output column 0
		xmm9 = _mm_mul_ps(xmm5, xmm1);				//Output column 1
		xmm10 = _mm_mul_ps(xmm6, xmm2);				//Output column 2
		xmm11 = _mm_mul_ps(xmm7, xmm3);				//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 30, 31, 32, 33
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 30, 31, 32, 33

		//Write the results for 30, 31, 32, and 33 (7) (8/16th of the work)
		_mm_store_ps(pOut + 24, xmm8);

		//************  HALF WAY DONE ...............

		//******************************
		//Begin calculating output row 4
		//We already have the bottom-left quarter of Matrix 2 (3)
		//We begin by calculating 40, 41, 42, and 43.
		//Read the right half of the fifth row of Matrix 1 (10)
		xmm7 = _mm_load_ps(pM1 + 36);
		xmm4 = _mm_shuffle_ps(xmm7, xmm7, 0x00);	//Broadcast element 0
		xmm5 = _mm_shuffle_ps(xmm7, xmm7, 0x55);	//Broadcast element 1
		xmm6 = _mm_shuffle_ps(xmm7, xmm7, 0xaa);	//Broadcast element 2
		xmm7 = _mm_shuffle_ps(xmm7, xmm7, 0xff);	//Broadcast element 3
		xmm8 = _mm_mul_ps(xmm4, xmm0);				//Output column 0
		xmm9 = _mm_mul_ps(xmm5, xmm1);				//Output column 1
		xmm10 = _mm_mul_ps(xmm6, xmm2);				//Output column 2
		xmm11 = _mm_mul_ps(xmm7, xmm3);				//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 40, 41, 42, and 43

		//Read the top-left quarter of Matrix 2 (1)
		xmm8 = _mm_load_ps(pM2);					//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 8);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 16);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 24);				//Four from the fourth row

		//Read the left half of the fifth row of Matrix 1 (9)
		xmm3 = _mm_load_ps(pM1 + 32);
		xmm0 = _mm_shuffle_ps(xmm3, xmm3, 0x00);	//Broadcast element 0
		xmm1 = _mm_shuffle_ps(xmm3, xmm3, 0x55);	//Broadcast element 1
		xmm2 = _mm_shuffle_ps(xmm3, xmm3, 0xaa);	//Broadcast element 2
		xmm3 = _mm_shuffle_ps(xmm3, xmm3, 0xff);	//Broadcast element 3
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 0
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 1
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 2
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 40, 41, 42, and 43
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 40, 41, 42, and 43

		//Write the results for 40, 41, 42, and 43 (9) (9/16th of the work complete)
		_mm_store_ps(pOut + 32, xmm8);

		//Begin calculating 44, 45, 46, and 47
		//Read the upper-right quarter of Matrix 2 (2)
		//Reuse the broadcasts from last section
		xmm8 = _mm_load_ps(pM2 + 4);				//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 12);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 20);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 28);				//Four from the fourth row
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 4
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 5
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 6
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 7
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 44, 45, 46, and 47

		//Read the bottom-right quarter of Matrix 2 (4)
		xmm0 = _mm_load_ps(pM2 + 36);				//Four from the first row
		xmm1 = _mm_load_ps(pM2 + 44);				//Four from the second row
		xmm2 = _mm_load_ps(pM2 + 52);				//Four from the third row
		xmm3 = _mm_load_ps(pM2 + 60);				//Four from the fourth row
		xmm8 = _mm_mul_ps(xmm4, xmm0);				//Output column 4
		xmm9 = _mm_mul_ps(xmm5, xmm1);				//Output column 5
		xmm10 = _mm_mul_ps(xmm6, xmm2);				//Output column 6
		xmm11 = _mm_mul_ps(xmm7, xmm3);				//Output column 7
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 44, 45, 46, and 47
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 44, 45, 46, and 47

		//Write the results for 44, 45, 46, and 47 (10) (10/16th of the work)
		_mm_store_ps(pOut + 36, xmm8);

		//******************************
		//Begin calculating output row 5
		//We already have the bottom-right quarter of Matrix 2 (4)
		//We begin by calculating 54, 55, 56 and 57.
		//Read the right half of the sixth row of Matrix 1 (12)
		xmm7 = _mm_load_ps(pM1 + 44);
		xmm4 = _mm_shuffle_ps(xmm7, xmm7, 0x00);	//Broadcast element 0
		xmm5 = _mm_shuffle_ps(xmm7, xmm7, 0x55);	//Broadcast element 1
		xmm6 = _mm_shuffle_ps(xmm7, xmm7, 0xaa);	//Broadcast element 2
		xmm7 = _mm_shuffle_ps(xmm7, xmm7, 0xff);	//Broadcast element 3
		xmm8 = _mm_mul_ps(xmm4, xmm0);				//Output column 4
		xmm9 = _mm_mul_ps(xmm5, xmm1);				//Output column 5
		xmm10 = _mm_mul_ps(xmm6, xmm2);				//Output column 6
		xmm11 = _mm_mul_ps(xmm7, xmm3);				//Output column 7
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 54, 55, 56, 57

		//Read the top-right quarter of Matrix 2 (2)
		xmm8 = _mm_load_ps(pM2 + 4);				//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 12);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 20);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 28);				//Four from the fourth row

		//Read the left half of the fourth row of Matrix 1 (11)
		//Broadcast using pshuf
		xmm3 = _mm_load_ps(pM1 + 40);
		xmm0 = _mm_shuffle_ps(xmm3, xmm3, 0x00);	//Broadcast element 0
		xmm1 = _mm_shuffle_ps(xmm3, xmm3, 0x55);	//Broadcast element 1
		xmm2 = _mm_shuffle_ps(xmm3, xmm3, 0xaa);	//Broadcast element 2
		xmm3 = _mm_shuffle_ps(xmm3, xmm3, 0xff);	//Broadcast element 3
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 4
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 5
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 6
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 7
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 54, 55, 56, 57
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 54, 55, 56, 57

		//Write the results for 54, 55, 56, 57 (12) (11/16th of the work complete)
		_mm_store_ps(pOut + 44, xmm8);

		//Begin calculating 50, 51, 52, 53
		//Read the upper-left quarter of Matrix 2 (1)
		//Reuse the broadcasts from last section
		xmm8 = _mm_load_ps(pM2);					//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 8);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 16);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 24);				//Four from the fourth row
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 0
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 1
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 2
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 50, 51, 52, 53

		//Read the bottom-left quarter of Matrix 2 (3)
		xmm0 = _mm_load_ps(pM2 + 32);				//Four from the first row
		xmm1 = _mm_load_ps(pM2 + 40);				//Four from the second row
		xmm2 = _mm_load_ps(pM2 + 48);				//Four from the third row
		xmm3 = _mm_load_ps(pM2 + 56);				//Four from the fourth row
		xmm8 = _mm_mul_ps(xmm4, xmm0);				//Output column 0
		xmm9 = _mm_mul_ps(xmm5, xmm1);				//Output column 1
		xmm10 = _mm_mul_ps(xmm6, xmm2);				//Output column 2
		xmm11 = _mm_mul_ps(xmm7, xmm3);				//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 50, 51, 52, 53
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 50, 51, 52, 53

		//Write the results for 50, 51, 52, 53 (11) (12/16th of the work)
		_mm_store_ps(pOut + 40, xmm8);

		//******************************
		//Begin calculating output row 6
		//We already have the bottom-left quarter of Matrix 2 (3)
		//We begin by calculating 60, 61, 62, and 63.
		//Read the right half of the seventh row of Matrix 1 (14)
		xmm7 = _mm_load_ps(pM1 + 52);
		xmm4 = _mm_shuffle_ps(xmm7, xmm7, 0x00);	//Broadcast element 0
		xmm5 = _mm_shuffle_ps(xmm7, xmm7, 0x55);	//Broadcast element 1
		xmm6 = _mm_shuffle_ps(xmm7, xmm7, 0xaa);	//Broadcast element 2
		xmm7 = _mm_shuffle_ps(xmm7, xmm7, 0xff);	//Broadcast element 3
		xmm8 = _mm_mul_ps(xmm4, xmm0);				//Output column 0
		xmm9 = _mm_mul_ps(xmm5, xmm1);				//Output column 1
		xmm10 = _mm_mul_ps(xmm6, xmm2);				//Output column 2
		xmm11 = _mm_mul_ps(xmm7, xmm3);				//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 60, 61, 62, and 63

		//Read the top-left quarter of Matrix 2 (1)
		xmm8 = _mm_load_ps(pM2);					//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 8);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 16);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 24);				//Four from the fourth row

		//Read the left half of the seventh row of Matrix 1 (13)
		xmm3 = _mm_load_ps(pM1 + 48);
		xmm0 = _mm_shuffle_ps(xmm3, xmm3, 0x00);	//Broadcast element 0
		xmm1 = _mm_shuffle_ps(xmm3, xmm3, 0x55);	//Broadcast element 1
		xmm2 = _mm_shuffle_ps(xmm3, xmm3, 0xaa);	//Broadcast element 2
		xmm3 = _mm_shuffle_ps(xmm3, xmm3, 0xff);	//Broadcast element 3
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 0
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 1
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 2
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 60, 61, 62, and 63
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 60, 61, 62, and 63

		//Write the results for 60, 61, 62, and 63 (13) (13/16th of the work complete)
		_mm_store_ps(pOut + 48, xmm8);

		//Begin calculating 64, 65, 66, and 67
		//Read the upper-right quarter of Matrix 2 (2)
		//Reuse the broadcasts from last section
		xmm8 = _mm_load_ps(pM2 + 4);				//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 12);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 20);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 28);				//Four from the fourth row
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 4
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 5
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 6
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 7
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 64, 65, 66, and 67

		//Read the bottom-right quarter of Matrix 2 (4)
		xmm0 = _mm_load_ps(pM2 + 36);				//Four from the first row
		xmm1 = _mm_load_ps(pM2 + 44);				//Four from the second row
		xmm2 = _mm_load_ps(pM2 + 52);				//Four from the third row
		xmm3 = _mm_load_ps(pM2 + 60);				//Four from the fourth row
		xmm8 = _mm_mul_ps(xmm4, xmm0);				//Output column 4
		xmm9 = _mm_mul_ps(xmm5, xmm1);				//Output column 5
		xmm10 = _mm_mul_ps(xmm6, xmm2);				//Output column 6
		xmm11 = _mm_mul_ps(xmm7, xmm3);				//Output column 7
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 64, 65, 66, and 67
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 64, 65, 66, and 67

		//Write the results for 64, 65, 66, and 67 (14) (14/16th of the work)
		_mm_store_ps(pOut + 52, xmm8);

		//******************************
		//Begin calculating output row 7
		//We already have the bottom-right quarter of Matrix 2 (4)
		//We begin by calculating 74, 75, 76 and 77.
		//Read the right half of the eigth row of Matrix 1 (16)
		xmm7 = _mm_load_ps(pM1 + 60);
		xmm4 = _mm_shuffle_ps(xmm7, xmm7, 0x00);	//Broadcast element 0
		xmm5 = _mm_shuffle_ps(xmm7, xmm7, 0x55);	//Broadcast element 1
		xmm6 = _mm_shuffle_ps(xmm7, xmm7, 0xaa);	//Broadcast element 2
		xmm7 = _mm_shuffle_ps(xmm7, xmm7, 0xff);	//Broadcast element 3
		xmm8 = _mm_mul_ps(xmm4, xmm0);				//Output column 4
		xmm9 = _mm_mul_ps(xmm5, xmm1);				//Output column 5
		xmm10 = _mm_mul_ps(xmm6, xmm2);				//Output column 6
		xmm11 = _mm_mul_ps(xmm7, xmm3);				//Output column 7
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 74, 75, 76, 77

		//Read the top-right quarter of Matrix 2 (2)
		xmm8 = _mm_load_ps(pM2 + 4);				//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 12);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 20);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 28);				//Four from the fourth row

		//Read the left half of the fourth row of Matrix 1 (15)
		//Broadcast using pshuf
		xmm3 = _mm_load_ps(pM1 + 56);
		xmm0 = _mm_shuffle_ps(xmm3, xmm3, 0x00);	//Broadcast element 0
		xmm1 = _mm_shuffle_ps(xmm3, xmm3, 0x55);	//Broadcast element 1
		xmm2 = _mm_shuffle_ps(xmm3, xmm3, 0xaa);	//Broadcast element 2
		xmm3 = _mm_shuffle_ps(xmm3, xmm3, 0xff);	//Broadcast element 3
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 4
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 5
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 6
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 7
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 74, 75, 76, 77
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 74, 75, 76, 77

		//Write the results for 74, 75, 76, 77 (16) (15/16th of the work complete)
		_mm_store_ps(pOut + 60, xmm8);

		//Begin calculating 70, 71, 72, 73
		//Read the upper-left quarter of Matrix 2 (1)
		//Reuse the broadcasts from last section
		xmm8 = _mm_load_ps(pM2);					//Four from the first row
		xmm9 = _mm_load_ps(pM2 + 8);				//Four from the second row
		xmm10 = _mm_load_ps(pM2 + 16);				//Four from the third row
		xmm11 = _mm_load_ps(pM2 + 24);				//Four from the fourth row
		xmm8 = _mm_mul_ps(xmm0, xmm8);				//Output column 0
		xmm9 = _mm_mul_ps(xmm1, xmm9);				//Output column 1
		xmm10 = _mm_mul_ps(xmm2, xmm10);			//Output column 2
		xmm11 = _mm_mul_ps(xmm3, xmm11);			//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm14 = _mm_add_ps(xmm12, xmm13);			//Creates first half-sum 70, 71, 72, 73

		//Read the bottom-left quarter of Matrix 2 (3)
		xmm0 = _mm_load_ps(pM2 + 32);				//Four from the first row
		xmm1 = _mm_load_ps(pM2 + 40);				//Four from the second row
		xmm2 = _mm_load_ps(pM2 + 48);				//Four from the third row
		xmm3 = _mm_load_ps(pM2 + 56);				//Four from the fourth row
		xmm8 = _mm_mul_ps(xmm4, xmm0);				//Output column 0
		xmm9 = _mm_mul_ps(xmm5, xmm1);				//Output column 1
		xmm10 = _mm_mul_ps(xmm6, xmm2);				//Output column 2
		xmm11 = _mm_mul_ps(xmm7, xmm3);				//Output column 3
		xmm12 = _mm_add_ps(xmm8, xmm9);
		xmm13 = _mm_add_ps(xmm10, xmm11);
		xmm15 = _mm_add_ps(xmm12, xmm13);			//Creates second half-sum 70, 71, 72, 73
		xmm8 = _mm_add_ps(xmm14, xmm15);			//Creates final sum 70, 71, 72, 73

		//Write the results for 70, 71, 72, 73 (15) (16/16th of the work)
		_mm_store_ps(pOut + 56, xmm8);
	}
}

//The ReferenceKernelInit function will be called before ReferenceKernelProcessBuffer is called
void ReferenceKernelInit()
{
	unsigned int iBufferWidth = msBufferParams2.uiBufferWidth;
	unsigned int iBufferHeight = msBufferParams1.uiBufferHeight;

	float * pReference = ReferenceBuffer;
	for (int i = 0; i < iBufferHeight; i++)	{
		for (int j = 0; j < iBufferWidth; j++)	{
			*(pReference + i*iBufferWidth + j) = 0;
		}
	}
}

//the ReferenceKernelProcessBuffer function should contain reference code for the kernel
void ReferenceKernelProcessBuffer()
{

	multiplyMatrix(InputBuffer1,
				   InputBuffer2,
				   ReferenceBuffer,
				   msBufferParams1.uiBufferHeight,
				   msBufferParams1.uiBufferWidth,
				   msBufferParams2.uiBufferHeight,
				   msBufferParams2.uiBufferWidth);
}

//the function should check that the kernel generated results are valid
bool ValidateResult()
{
	unsigned int iBufferWidth = msBufferParams2.uiBufferWidth;
	unsigned int iBufferHeight = msBufferParams1.uiBufferHeight;

	float *pBuffer1 = OutputBuffer;
	float *pBuffer2 = ReferenceBuffer;

	bool flag = true;
	int errorCount = 0;
	for (int i = 0; i < iBufferHeight && errorCount < 10; i++){
		for (int j = 0; j < iBufferWidth; j++){
			if (*(pBuffer1 + (i*iBufferWidth +j)) != *(pBuffer2 + (i*iBufferWidth +j))){
				cout << "Out[" << i << ", " << j << "] = " << *(pBuffer1 + (i*iBufferWidth +j))
				<< " <> Ref[" << i << ", " <<  j << "] = " << *(pBuffer2 + (i*iBufferWidth +j)) << endl;
				errorCount++;
				flag = false;
			}
		}
	}
	if(errorCount > 9)	{
		cout << "Stopped compare after 9 errors" << endl;
	}
	return flag;
}

int main(int argc, char *argp[])	{

	cout << "Welcome to Multiply8x8Matrices" << endl;

	//Initialization steps
	SetKernelParameters(argc, argp);
	AllocateBuffers(32);
	ReferenceKernelInit();
	KernelInit();
	ReferenceKernelProcessBuffer();

	//Multiply using Intel(R) AVX instructions
	avx_smallMatrices_broadcast();
	if(ValidateResult() == true)	{
		cout << "256-bit results matched - small matrices using broadcast" << endl;
	}
	else	{
		cout << "256-bit results failed - small matrices using broadcast" << endl;
	}

	//Multiply using Intel(R) SSE instructions
	sse_smallMatrices_broadcast();
	if(ValidateResult() == true)	{
		cout << "128-bit results matched - small matrices using broadcast" << endl;
	}
	else	{
		cout << "128-bit results failed - small matrices using broadcast" << endl;
	}

	//Cleanup and exit
	DeAllocateBuffers();
}
