
#include <iostream>
#include <math.h>
#include <gmmintrin.h>

using namespace std;

#define NUM_MATS 8
#define NUM_MATS_HALF 4
#define MAT_WIDTH 4
#define MAT_HEIGHT 4
#define MAT_SIZE (MAT_WIDTH * MAT_HEIGHT)

float* InputBuffer = NULL;
float* OutputBuffer = NULL;
float* ReferenceBuffer = NULL;
// loop count can be changed to more than 1 if want to iterate the determinant of 8 4x4 matrices 
// for more than 1 iterations for performance timing purposes.
int loopCount = 1; 

float evaluateDeterminant(float * const pInput, int size)	{
	
	float determinant;

	if(size == 2)	{

		determinant = pInput[0]*pInput[3] - pInput[1]*pInput[2];
		return determinant;
	}

	float minorDeterminant;
	int majorRowIndex = 0;
//	float sign = 1.0;
	determinant = 0.0;

	//Allocate memory for the minor matrix
	float * ptrMinor = (float *) malloc((size-1)*(size-1)*sizeof(float));

	//Get the cofactors and evaluate determinate
	for(int majorColumnIndex = 0; majorColumnIndex < size; majorColumnIndex++)	{
								
		//Fill in the minor matrix for this row, column
		for(int i = 0, minorRowIndex = 0; i  < size; i++)	{
				
			//Do all the rows except for the current row
			if(i != majorRowIndex)	{				
						
				//Do all the columns except for the current column
				for(int j = 0, minorColumnIndex = 0; j < size; j++)	{
					if(j != majorColumnIndex)	{

						//Move a value from the matrix to the minor matrix
						*(ptrMinor+(minorRowIndex*(size-1)+minorColumnIndex)) = *(pInput+(i*size)+j);
							
						//Next column of the minor matrix
						minorColumnIndex++;
					}
				}
						
				//Next row of the minor matrix
				minorRowIndex++;
			}
		}

		//Get the determinant for the minor matrix for this row, column
		minorDeterminant = evaluateDeterminant(ptrMinor, size-1);
				
		//Multiple that determinant by (-1) raised to the ((majorColumnIndex+1)+(majorRowIndex+1)) power
		//That gives us a cofactor
		minorDeterminant *= (int) pow(-1.0, int ((majorColumnIndex+1)+(majorRowIndex+1)));
//		minorDeterminant *= sign;
//		sign *= -1.0;

		//Multiply that cofactor by the element and accumulate the result for the top row
		determinant += (*(pInput+(majorRowIndex*size)+majorColumnIndex)) * minorDeterminant;
	}
	free(ptrMinor);

	return determinant;
}

void SetKernelParameters(int argc, char* argv[])
{
	if(argc == 2)
	{
		loopCount = atoi(argv[1]);
	}
	else if(argc > 2)
	{//first two parameters are the exe name and the kernel number
		cout << "Determinant4x4Matrices Usage" << endl;
		cout << " No command line arguments are required." << endl;
		cout << " The optional argument = number of times to run the 4x4 Evaluation of matrix determinant." << endl;		
		exit(1);
	}
	return;
}

//Memory allocations could be made in the AllocateBuffers function
int AllocateBuffers(int AlignmentBytes)
{
	//calculate necessary buffer size
	int iBufferSizeBytes = NUM_MATS*MAT_WIDTH*MAT_HEIGHT*sizeof(float);
	InputBuffer = (float*) _mm_malloc(iBufferSizeBytes, AlignmentBytes); //should be 32bytes alignment for AVX

	iBufferSizeBytes = NUM_MATS*sizeof(float);
	OutputBuffer = (float*) _mm_malloc(iBufferSizeBytes, AlignmentBytes); //should be 32bytes alignment for AVX
	ReferenceBuffer = (float*) _mm_malloc(iBufferSizeBytes, AlignmentBytes); //should be 32bytes alignment for AVX
	return 0;
}

//Memory free could be made in the DeAllocateBuffers function
int DeAllocateBuffers()
{
	//free allocated buffers
	_mm_free(InputBuffer); 
	_mm_free(OutputBuffer);
	_mm_free(ReferenceBuffer);
	return 0;
}

//The KernelInit function will be called before ProcessOptimizedKernel is called
//(It will be called more then once when generating traces - warm up )
void KernelInit()
{
	for (int numMats = 0; numMats < NUM_MATS; numMats++)
	{
		// These init values give a final determinant value of 5
		InputBuffer[(MAT_SIZE*numMats)+0] = 2.0; 
		InputBuffer[(MAT_SIZE*numMats)+1] = -1.0; 
		InputBuffer[(MAT_SIZE*numMats)+2] = 0.0; 
		InputBuffer[(MAT_SIZE*numMats)+3] = 0.0; 
		InputBuffer[(MAT_SIZE*numMats)+4] = -1.0;
		InputBuffer[(MAT_SIZE*numMats)+5] = 2.0; 
		InputBuffer[(MAT_SIZE*numMats)+6] = -1.0; 
		InputBuffer[(MAT_SIZE*numMats)+7] = 0.0; 
		InputBuffer[(MAT_SIZE*numMats)+8] = 0.0; 
		InputBuffer[(MAT_SIZE*numMats)+9] = -1.0; 
		InputBuffer[(MAT_SIZE*numMats)+10] = 2.0; 
		InputBuffer[(MAT_SIZE*numMats)+11] = -1.0; 
		InputBuffer[(MAT_SIZE*numMats)+12] = 0.0; 
		InputBuffer[(MAT_SIZE*numMats)+13] = 0.0; 
		InputBuffer[(MAT_SIZE*numMats)+14] = -1.0; 
		InputBuffer[(MAT_SIZE*numMats)+15] = 2.0; 
	}	
}


//The ProcessOptimizedKernel should contain the optimized kernel code
void sse_determinant()
{
	__m128	xmm_a11, xmm_a12, xmm_a13, xmm_a14;
	__m128	xmm_a21, xmm_a22, xmm_a23, xmm_a24;
	__m128	xmm_a31, xmm_a32, xmm_a33, xmm_a34;
	__m128	xmm_a41, xmm_a42, xmm_a43, xmm_a44;
	__m128 xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6, xmm7;

	float * pIn = (float *) InputBuffer;
	float * pOut = (float *) OutputBuffer;
	int l_numIter = 2; // set to 2 since sse version evaluates 4 matrix determinats at a time. To compare sse with 
					   // avx version which evaluates 4 matrix determinats ata time, the number of iterations is set to 2.
	int lcount = loopCount;

	for (unsigned int k = 0; k < lcount; k++) {
	pIn = (float *) InputBuffer;
	for (int t = 0; t < l_numIter; 
		t++,
		pIn += NUM_MATS_HALF*MAT_SIZE)
	{
		xmm0 = _mm_load_ps((float *) (pIn + (0*MAT_SIZE+0*MAT_WIDTH) ));			//1st Mat - 4 from the 1st row
		xmm1 = _mm_load_ps((float *) (pIn + (1*MAT_SIZE+0*MAT_WIDTH) ));			//2nd Mat - 4 from the 1st row	
		xmm2 = _mm_load_ps((float *) (pIn + (2*MAT_SIZE+0*MAT_WIDTH) ));			//3rd Mat - 4 from the 1st row
		xmm3 = _mm_load_ps((float *) (pIn + (3*MAT_SIZE+0*MAT_WIDTH) ));			//4th Mat - 4 from the 1st row

		xmm4 = _mm_unpacklo_ps(xmm0, xmm1);			//11, 11, 12, 12
		xmm0 = _mm_unpackhi_ps(xmm0, xmm1);			//13, 13, 14, 14
		xmm1 = _mm_unpacklo_ps(xmm2, xmm3);			//11, 11, 12, 12
		xmm2 = _mm_unpackhi_ps(xmm2, xmm3);			//13, 13, 14, 14

		xmm3 = _mm_shuffle_ps(xmm4, xmm1, 0x4e);	
		xmm_a11 = _mm_blend_ps(xmm4, xmm3, 0xc);	//11, 11, 11, 11	
		xmm_a12 = _mm_blend_ps(xmm1, xmm3, 0x3);	//12, 12, 12, 12	
		
		xmm3 = _mm_shuffle_ps(xmm0, xmm2, 0x4e);	
		xmm_a13 = _mm_blend_ps(xmm0, xmm3, 0xc);	
		xmm_a14 = _mm_blend_ps(xmm2, xmm3, 0x3);	
		xmm0 = _mm_load_ps((float *) (pIn + (0*MAT_SIZE+1*MAT_WIDTH) ));			//1st Mat - 4 from the 2nd row
		xmm1 = _mm_load_ps((float *) (pIn + (1*MAT_SIZE+1*MAT_WIDTH) ));			//2nd Mat - 4 from the 2nd row	
		xmm2 = _mm_load_ps((float *) (pIn + (2*MAT_SIZE+1*MAT_WIDTH) ));			//3rd Mat - 4 from the 2nd row
		xmm3 = _mm_load_ps((float *) (pIn + (3*MAT_SIZE+1*MAT_WIDTH) ));			//4th Mat - 4 from the 2nd row

		xmm4 = _mm_unpacklo_ps(xmm0, xmm1);			
		xmm0 = _mm_unpackhi_ps(xmm0, xmm1);			
		xmm1 = _mm_unpacklo_ps(xmm2, xmm3);			
		xmm2 = _mm_unpackhi_ps(xmm2, xmm3);			

		xmm3 = _mm_shuffle_ps(xmm4, xmm1, 0x4e);	
		xmm_a21 = _mm_blend_ps(xmm4, xmm3, 0xc);	//21, 21, 21, 21
		xmm_a22 = _mm_blend_ps(xmm1, xmm3, 0x3);	//22, 22, 22, 22
		
		xmm3 = _mm_shuffle_ps(xmm0, xmm2, 0x4e);	
		xmm_a23 = _mm_blend_ps(xmm0, xmm3, 0xc);	
		xmm_a24 = _mm_blend_ps(xmm2, xmm3, 0x3);	

		xmm0 = _mm_load_ps((float *) (pIn + (0*MAT_SIZE+2*MAT_WIDTH) ));			//1st Mat - 4 from the 3rd row
		xmm1 = _mm_load_ps((float *) (pIn + (1*MAT_SIZE+2*MAT_WIDTH) ));			//2nd Mat - 4 from the 3rd row	
		xmm2 = _mm_load_ps((float *) (pIn + (2*MAT_SIZE+2*MAT_WIDTH) ));			//3rd Mat - 4 from the 3rd row
		xmm3 = _mm_load_ps((float *) (pIn + (3*MAT_SIZE+2*MAT_WIDTH) ));			//4th Mat - 4 from the 3rd row

		xmm4 = _mm_unpacklo_ps(xmm0, xmm1);			
		xmm0 = _mm_unpackhi_ps(xmm0, xmm1);			
		xmm1 = _mm_unpacklo_ps(xmm2, xmm3);			
		xmm2 = _mm_unpackhi_ps(xmm2, xmm3);			

		xmm3 = _mm_shuffle_ps(xmm4, xmm1, 0x4e);	
		xmm_a31 = _mm_blend_ps(xmm4, xmm3, 0xc);	//31, 31, 31, 31	
		xmm_a32 = _mm_blend_ps(xmm1, xmm3, 0x3);	//32, 32, 32, 32	
		
		xmm3 = _mm_shuffle_ps(xmm0, xmm2, 0x4e);	
		xmm_a33 = _mm_blend_ps(xmm0, xmm3, 0xc);	
		xmm_a34 = _mm_blend_ps(xmm2, xmm3, 0x3);	

		xmm0  = _mm_load_ps((float *) (pIn + (0*MAT_SIZE+3*MAT_WIDTH) ));			//1st Mat - 4 from the 4th row
		xmm1  = _mm_load_ps((float *) (pIn + (1*MAT_SIZE+3*MAT_WIDTH) ));			//2nd Mat - 4 from the 4th row	
		xmm2  = _mm_load_ps((float *) (pIn + (2*MAT_SIZE+3*MAT_WIDTH) ));			//3rd Mat - 4 from the 4th row
		xmm3  = _mm_load_ps((float *) (pIn + (3*MAT_SIZE+3*MAT_WIDTH) ));			//4th Mat - 4 from the 4th row


		xmm4 = _mm_unpacklo_ps(xmm0, xmm1);			
		xmm0 = _mm_unpackhi_ps(xmm0, xmm1);			
		xmm1 = _mm_unpacklo_ps(xmm2, xmm3);			
		xmm2 = _mm_unpackhi_ps(xmm2, xmm3);			

		xmm3 = _mm_shuffle_ps(xmm4, xmm1, 0x4e);	
		xmm_a41 = _mm_blend_ps(xmm4, xmm3, 0xc);	//41, 41, 41, 41	
		xmm_a42 = _mm_blend_ps(xmm1, xmm3, 0x3);	//42, 42, 42, 42	
		
		xmm3 = _mm_shuffle_ps(xmm0, xmm2, 0x4e);	
		xmm_a43 = _mm_blend_ps(xmm0, xmm3, 0xc);	
		xmm_a44 = _mm_blend_ps(xmm2, xmm3, 0x3);	

		/// *******************************************//
		/// ******Determinants for a11*****************
		__m128  xmm_a11a22, xmm_a33a44, xmm_a43a34, xmm_a11a23, xmm_a32a44, xmm_a34a42, xmm_a11a24, xmm_a32a43, xmm_a42a33;
		__m128	xmm_a11a22_D, xmm_a11a23_D, xmm_a11a24_D, xmm_a11_D;
		__m128	xmm_a33a44_a43a34, xmm_a32a44_a34a42, xmm_a32a43_a42a33;

		 // Calculate detrminant for 4 matrices in parallel

		xmm_a11a22 = _mm_mul_ps(xmm_a11, xmm_a22);
		xmm_a33a44 = _mm_mul_ps(xmm_a33, xmm_a44);
		xmm_a43a34 = _mm_mul_ps(xmm_a43, xmm_a34);
		xmm_a33a44_a43a34 = _mm_sub_ps(xmm_a33a44, xmm_a43a34);
		xmm_a11a22_D = _mm_mul_ps(xmm_a11a22, xmm_a33a44_a43a34); // 4 a11 a22 D values
		
		xmm_a11a23 = _mm_mul_ps(xmm_a11, xmm_a23);
		xmm_a32a44 = _mm_mul_ps(xmm_a32, xmm_a44);
		xmm_a34a42 = _mm_mul_ps(xmm_a34, xmm_a42);
		xmm_a32a44_a34a42 = _mm_sub_ps(xmm_a32a44, xmm_a34a42);
		xmm_a11a23_D = _mm_mul_ps(xmm_a11a23, xmm_a32a44_a34a42); // 4 a11 a23 D values
		
		xmm_a11a24 = _mm_mul_ps(xmm_a11, xmm_a24);
		xmm_a32a43 = _mm_mul_ps(xmm_a32, xmm_a43);
		xmm_a42a33 = _mm_mul_ps(xmm_a42, xmm_a33);
		xmm_a32a43_a42a33 = _mm_sub_ps(xmm_a32a43, xmm_a42a33);
		xmm_a11a24_D = _mm_mul_ps(xmm_a11a24, xmm_a32a43_a42a33); // 4 a11 a24 D values

		xmm_a11_D = _mm_add_ps(_mm_sub_ps(xmm_a11a22_D, xmm_a11a23_D), xmm_a11a24_D); // Final 4 a11 D values

		/// *******************************************//
		/// ******Determinants for a12*****************//
		__m128	xmm_a12a21, xmm_a12a23, xmm_a31a44, xmm_a41a34, xmm_a12a24, xmm_a31a43, xmm_a41a33;
		__m128	xmm_a12a21_D, xmm_a12a23_D, xmm_a12a24_D, xmm_a12_D;
		__m128	xmm_a31a44_a41a34, xmm_a31a43_a41a33;

		xmm_a12a21 = _mm_mul_ps(xmm_a12, xmm_a21);
		xmm_a12a21_D = _mm_mul_ps(xmm_a12a21, xmm_a33a44_a43a34); // 4 a12 a21 D values

		xmm_a12a23 = _mm_mul_ps(xmm_a12, xmm_a23);
		xmm_a31a44 = _mm_mul_ps(xmm_a31, xmm_a44);
		xmm_a41a34 = _mm_mul_ps(xmm_a41, xmm_a34);
		xmm_a31a44_a41a34 = _mm_sub_ps(xmm_a31a44, xmm_a41a34);
		xmm_a12a23_D = _mm_mul_ps(xmm_a12a23, xmm_a31a44_a41a34); // 4 a12 a23 D values

		xmm_a12a24 = _mm_mul_ps(xmm_a12, xmm_a24);
		xmm_a31a43 = _mm_mul_ps(xmm_a31, xmm_a43);
		xmm_a41a33 = _mm_mul_ps(xmm_a41, xmm_a33);
		xmm_a31a43_a41a33 = _mm_sub_ps(xmm_a31a43, xmm_a41a33);
		xmm_a12a24_D = _mm_mul_ps(xmm_a12a24, xmm_a31a43_a41a33); // 4 a12 a24 D values

		xmm_a12_D = _mm_sub_ps(_mm_sub_ps(xmm_a12a23_D, xmm_a12a21_D), xmm_a12a24_D); // Final 4 a12 D values

		/// *******************************************//
		/// ******Determinants for a13*****************//
		__m128	xmm_a13a21, xmm_a13a22, xmm_a13a24, xmm_a31a42, xmm_a41a32;
		__m128	xmm_a13a21_D, xmm_a13a22_D, xmm_a13a24_D, xmm_a13_D;
		__m128	xmm_a31a42_a41a32;

		xmm_a13a21 = _mm_mul_ps(xmm_a13, xmm_a21);
		xmm_a13a21_D = _mm_mul_ps(xmm_a13a21, xmm_a32a44_a34a42); // 4 a13 a21 D values
		xmm_a13a22 = _mm_mul_ps(xmm_a13, xmm_a22);
		xmm_a13a22_D = _mm_mul_ps(xmm_a13a22, xmm_a31a43_a41a33); // 4 a13 a22 D values
		xmm_a13a24 = _mm_mul_ps(xmm_a13, xmm_a24);
		xmm_a31a42 = _mm_mul_ps(xmm_a31, xmm_a42);
		xmm_a41a32 = _mm_mul_ps(xmm_a41, xmm_a32);
		xmm_a31a42_a41a32 = _mm_sub_ps(xmm_a31a42, xmm_a41a32);
		xmm_a13a24_D = _mm_mul_ps(xmm_a13a24, xmm_a31a42_a41a32); // 4 a13 a24 D values

		xmm_a13_D = _mm_add_ps(_mm_sub_ps(xmm_a13a21_D, xmm_a13a22_D), xmm_a13a24_D); // Final 4 a13 D values

		/// *******************************************//
		/// ******Determinants for a14*****************//
		__m128	xmm_a14a21, xmm_a14a22, xmm_a14a23;
		__m128	xmm_a14a21_D, xmm_a14a22_D, xmm_a14a23_D, xmm_a14_D;

		xmm_a14a21 = _mm_mul_ps(xmm_a14, xmm_a21);
		xmm_a14a21_D = _mm_mul_ps(xmm_a14a21, xmm_a32a43_a42a33); // 4 a14 a21 D values
		xmm_a14a22 = _mm_mul_ps(xmm_a14, xmm_a22);
		xmm_a14a22_D = _mm_mul_ps(xmm_a14a22, xmm_a31a43_a41a33); // 4 a14 a22 D values
		xmm_a14a23 = _mm_mul_ps(xmm_a14, xmm_a23);
		xmm_a14a23_D = _mm_mul_ps(xmm_a14a23, xmm_a31a42_a41a32); // 4 a14 a23 D values
		xmm_a14_D = _mm_sub_ps(_mm_sub_ps(xmm_a14a22_D, xmm_a14a22_D), xmm_a14a21_D); // Final 4 a14 D values

		/// *******************************************//
		// Now ADD all 4 final D values: a11, a12, a13, a14
		xmm_a11_D = _mm_add_ps(xmm_a11_D, xmm_a12_D);
		xmm_a13_D = _mm_add_ps(xmm_a13_D, xmm_a14_D);
		xmm_a11_D = _mm_add_ps(xmm_a11_D, xmm_a13_D); // Final D values for 4 matrices

		_mm_store_ps((float *) (pOut + (t*NUM_MATS)), xmm_a11_D);
	
	}
	}

	return;
}

void avx_determinant()
{
	__m256	ymm_a11, ymm_a12, ymm_a13, ymm_a14;
	__m256	ymm_a21, ymm_a22, ymm_a23, ymm_a24;
	__m256	ymm_a31, ymm_a32, ymm_a33, ymm_a34;
	__m256	ymm_a41, ymm_a42, ymm_a43, ymm_a44;
	__m256  ymm0, ymm1, ymm2, ymm3, ymm4, ymm5, ymm6, ymm7;
	__m256	ymma, ymmb, ymmc, ymmd, ymme, ymmf, ymmg, ymmh;
	float * pIn = (float *) InputBuffer;
	float * pOut = (float *) OutputBuffer;

	int lcount = loopCount; 

	for (unsigned int k = 0; k < lcount; k++) {
		//Read the first eight rows of the 8x8 block of floats into ymm registers
		ymm0 = _mm256_load_ps((float *) (pIn + (0*MAT_SIZE)));	//8 (first 2 rows) from matrix 1 
		ymm1 = _mm256_load_ps((float *) (pIn + (1*MAT_SIZE)));	//8 (first 2 rows) from matrix 2 
		ymm2 = _mm256_load_ps((float *) (pIn + (2*MAT_SIZE)));	//8 (first 2 rows) from matrix 3 
		ymm3 = _mm256_load_ps((float *) (pIn + (3*MAT_SIZE)));	//8 (first 2 rows) from matrix 4 
		ymm4 = _mm256_load_ps((float *) (pIn + (4*MAT_SIZE)));	//8 (first 2 rows) from matrix 5 
		ymm5 = _mm256_load_ps((float *) (pIn + (5*MAT_SIZE)));	//8 (first 2 rows) from matrix 6 
		ymm6 = _mm256_load_ps((float *) (pIn + (6*MAT_SIZE)));	//8 (first 2 rows) from matrix 7 
		ymm7 = _mm256_load_ps((float *) (pIn + (7*MAT_SIZE)));	//8 (first 2 rows) from matrix 8 

		//Begin the transpose
		ymma = _mm256_unpacklo_ps(ymm0, ymm1);	
		ymmb = _mm256_unpackhi_ps(ymm0, ymm1);	
		ymmc = _mm256_unpacklo_ps(ymm2, ymm3);	
		ymmd = _mm256_unpackhi_ps(ymm2, ymm3);	
		ymme = _mm256_unpacklo_ps(ymm4, ymm5);	
		ymmf = _mm256_unpackhi_ps(ymm4, ymm5);	
		ymmg = _mm256_unpacklo_ps(ymm6, ymm7);	
		ymmh = _mm256_unpackhi_ps(ymm6, ymm7);	

		//Create output rows 0, 1, 4 and 5
		ymm7 = _mm256_shuffle_ps(ymma, ymmc, 0x4e);	
		ymm5 = _mm256_blend_ps(ymm7, ymma, 0x33);	
		ymm3 = _mm256_blend_ps(ymm7, ymmc, 0xcc);	
		
		ymm6 = _mm256_shuffle_ps(ymme, ymmg, 0x4e);	
		ymm4 = _mm256_blend_ps(ymm6, ymme, 0x33);	
		ymm2 = _mm256_blend_ps(ymm6, ymmg, 0xcc);	

		//Last step to create rows 0, 1, 4, and 5
		ymm_a11 = _mm256_permute2f128_ps(ymm5, ymm4, 0x20);	// 11, 11, 11, 11, 11, 11, 11, 11
		ymm_a12 = _mm256_permute2f128_ps(ymm3, ymm2, 0x20);	// 12, 12, 12, 12, 12, 12, 12, 12
		ymm_a21 = _mm256_permute2f128_ps(ymm5, ymm4, 0x31);	// 13, 13, 13, 13, 13, 13, 13, 13
		ymm_a22 = _mm256_permute2f128_ps(ymm3, ymm2, 0x31);	// 14, 14, 14, 14, 14, 14, 14, 14

		//Create output rows 2, 3, 6 and 7
		ymm7 = _mm256_shuffle_ps(ymmb, ymmd, 0x4e);	
		ymm5 = _mm256_blend_ps(ymm7, ymmb, 0x33);	
		ymm3 = _mm256_blend_ps(ymm7, ymmd, 0xcc);

		ymm6 = _mm256_shuffle_ps(ymmf, ymmh, 0x4e);	
		ymm4 = _mm256_blend_ps(ymm6, ymmf, 0x33);	
		ymm2 = _mm256_blend_ps(ymm6, ymmh, 0xcc);	

		//Last step to create rows 2, 3, 6, and 7
		ymm_a13 = _mm256_permute2f128_ps(ymm5, ymm4, 0x20);	
		ymm_a14 = _mm256_permute2f128_ps(ymm3, ymm2, 0x20);	
		ymm_a23 = _mm256_permute2f128_ps(ymm5, ymm4, 0x31);	
		ymm_a24 = _mm256_permute2f128_ps(ymm3, ymm2, 0x31);

		//Read the first eight rows of the 8x8 block of floats into ymm registers
		ymm0 = _mm256_load_ps((float *) (pIn + (0*MAT_SIZE) + (2*MAT_WIDTH)));	//8 (next 2 rows) from matrix 1 
		ymm1 = _mm256_load_ps((float *) (pIn + (1*MAT_SIZE) + (2*MAT_WIDTH)));	//8 (next 2 rows) from matrix 2 
		ymm2 = _mm256_load_ps((float *) (pIn + (2*MAT_SIZE) + (2*MAT_WIDTH)));	//8 (next 2 rows) from matrix 3 
		ymm3 = _mm256_load_ps((float *) (pIn + (3*MAT_SIZE) + (2*MAT_WIDTH)));	//8 (next 2 rows) from matrix 4 
		ymm4 = _mm256_load_ps((float *) (pIn + (4*MAT_SIZE) + (2*MAT_WIDTH)));	//8 (next 2 rows) from matrix 5 
		ymm5 = _mm256_load_ps((float *) (pIn + (5*MAT_SIZE) + (2*MAT_WIDTH)));	//8 (next 2 rows) from matrix 6 
		ymm6 = _mm256_load_ps((float *) (pIn + (6*MAT_SIZE) + (2*MAT_WIDTH)));	//8 (next 2 rows) from matrix 7 
		ymm7 = _mm256_load_ps((float *) (pIn + (7*MAT_SIZE) + (2*MAT_WIDTH)));	//8 (next 2 rows) from matrix 8 

		//Begin the transpose
		ymma = _mm256_unpacklo_ps(ymm0, ymm1);	
		ymmb = _mm256_unpackhi_ps(ymm0, ymm1);	
		ymmc = _mm256_unpacklo_ps(ymm2, ymm3);	
		ymmd = _mm256_unpackhi_ps(ymm2, ymm3);	
		ymme = _mm256_unpacklo_ps(ymm4, ymm5);	
		ymmf = _mm256_unpackhi_ps(ymm4, ymm5);	
		ymmg = _mm256_unpacklo_ps(ymm6, ymm7);	
		ymmh = _mm256_unpackhi_ps(ymm6, ymm7);	

		//Create output rows 0, 1, 4 and 5
		ymm7 = _mm256_shuffle_ps(ymma, ymmc, 0x4e);	 
		ymm5 = _mm256_blend_ps(ymm7, ymma, 0x33);	
		ymm3 = _mm256_blend_ps(ymm7, ymmc, 0xcc);	 
		
		ymm6 = _mm256_shuffle_ps(ymme, ymmg, 0x4e);	 
		ymm4 = _mm256_blend_ps(ymm6, ymme, 0x33);	 
		ymm2 = _mm256_blend_ps(ymm6, ymmg, 0xcc);	 

		//Last step to create rows 0, 1, 4, and 5
		ymm_a31 = _mm256_permute2f128_ps(ymm5, ymm4, 0x20);	
		ymm_a32 = _mm256_permute2f128_ps(ymm3, ymm2, 0x20);	
		ymm_a41 = _mm256_permute2f128_ps(ymm5, ymm4, 0x31);	
		ymm_a42 = _mm256_permute2f128_ps(ymm3, ymm2, 0x31);	

		//Create output rows 2, 3, 6 and 7
		ymm7 = _mm256_shuffle_ps(ymmb, ymmd, 0x4e);	
		ymm5 = _mm256_blend_ps(ymm7, ymmb, 0x33);	
		ymm3 = _mm256_blend_ps(ymm7, ymmd, 0xcc);	

		ymm6 = _mm256_shuffle_ps(ymmf, ymmh, 0x4e);	
		ymm4 = _mm256_blend_ps(ymm6, ymmf, 0x33);	
		ymm2 = _mm256_blend_ps(ymm6, ymmh, 0xcc);	

		//Last step to create rows 2, 3, 6, and 7
		ymm_a33 = _mm256_permute2f128_ps(ymm5, ymm4, 0x20);	//33, 33, 33, 33, 33, 33, 33, 33
		ymm_a34 = _mm256_permute2f128_ps(ymm3, ymm2, 0x20);	
		ymm_a43 = _mm256_permute2f128_ps(ymm5, ymm4, 0x31);	
		ymm_a44 = _mm256_permute2f128_ps(ymm3, ymm2, 0x31);	

		/// ******Determinants for a11*****************
		__m256  ymm_a11a22, ymm_a33a44, ymm_a43a34, ymm_a11a23, ymm_a32a44, ymm_a34a42, ymm_a11a24, ymm_a32a43, ymm_a42a33;
		__m256	ymm_a11a22_D, ymm_a11a23_D, ymm_a11a24_D, ymm_a11_D;
		__m256	ymm_a33a44_a43a34, ymm_a32a44_a34a42, ymm_a32a43_a42a33;

		// calculate a11a22(a33a44 - a43a34) for 8 matrices
		ymm_a11a22 = _mm256_mul_ps(ymm_a11, ymm_a22); 
		ymm_a33a44 = _mm256_mul_ps(ymm_a33, ymm_a44);
		ymm_a43a34 = _mm256_mul_ps(ymm_a43, ymm_a34);
		ymm_a33a44_a43a34 = _mm256_sub_ps(ymm_a33a44, ymm_a43a34);
		ymm_a11a22_D = _mm256_mul_ps(ymm_a11a22, ymm_a33a44_a43a34); 
		
		// calculate a11a23(a32a44 - a34a42) for 8 matrices
		ymm_a11a23 = _mm256_mul_ps(ymm_a11, ymm_a23);
		ymm_a32a44 = _mm256_mul_ps(ymm_a32, ymm_a44);
		ymm_a34a42 = _mm256_mul_ps(ymm_a34, ymm_a42);
		ymm_a32a44_a34a42 = _mm256_sub_ps(ymm_a32a44, ymm_a34a42);
		ymm_a11a23_D = _mm256_mul_ps(ymm_a11a23, ymm_a32a44_a34a42); 
		
		// calculate a11a24(a32a43 - a42a33) for 8 matrices
		ymm_a11a24 = _mm256_mul_ps(ymm_a11, ymm_a24);
		ymm_a32a43 = _mm256_mul_ps(ymm_a32, ymm_a43);
		ymm_a42a33 = _mm256_mul_ps(ymm_a42, ymm_a33);
		ymm_a32a43_a42a33 = _mm256_sub_ps(ymm_a32a43, ymm_a42a33);
		ymm_a11a24_D = _mm256_mul_ps(ymm_a11a24, ymm_a32a43_a42a33); 

		// calculate partial determinant for 8 matrices: (a11a22(a33a44 - a43a34) + 
		//a11a23(a32a44 - a34a42) + a11a24(a32a43 - a42a33)) 
		ymm_a11_D = _mm256_add_ps(_mm256_sub_ps(ymm_a11a22_D, ymm_a11a23_D), ymm_a11a24_D);

		/// ******Determinants for a12*****************//
		__m256	ymm_a12a21, ymm_a12a23, ymm_a31a44, ymm_a41a34, ymm_a12a24, ymm_a31a43, ymm_a41a33;
		__m256	ymm_a12a21_D, ymm_a12a23_D, ymm_a12a24_D, ymm_a12_D;
		__m256	ymm_a31a44_a41a34, ymm_a31a43_a41a33;

		// calculate a12a21(a33a44 - a43a34) for 8 matrices
		ymm_a12a21 = _mm256_mul_ps(ymm_a12, ymm_a21);
		ymm_a12a21_D = _mm256_mul_ps(ymm_a12a21, ymm_a33a44_a43a34); 
		
		// calculate a12a23(a31a44 - a41a34) for 8 matrices
		ymm_a12a23 = _mm256_mul_ps(ymm_a12, ymm_a23);
		ymm_a31a44 = _mm256_mul_ps(ymm_a31, ymm_a44);
		ymm_a41a34 = _mm256_mul_ps(ymm_a41, ymm_a34);
		ymm_a31a44_a41a34 = _mm256_sub_ps(ymm_a31a44, ymm_a41a34);
		ymm_a12a23_D = _mm256_mul_ps(ymm_a12a23, ymm_a31a44_a41a34); 
		
		// calculate a12a24(a31a43 - a41a33) for 8 matrices
		ymm_a12a24 = _mm256_mul_ps(ymm_a12, ymm_a24);
		ymm_a31a43 = _mm256_mul_ps(ymm_a31, ymm_a43);
		ymm_a41a33 = _mm256_mul_ps(ymm_a41, ymm_a33);
		ymm_a31a43_a41a33 = _mm256_sub_ps(ymm_a31a43, ymm_a41a33);
		ymm_a12a24_D = _mm256_mul_ps(ymm_a12a24, ymm_a31a43_a41a33); 

		// calculate partial determinant for 8 matrices: (a12a21(a33a44 - a43a34) + 
		// a12a23(a31a44 - a41a34) + a12a24(a31a43 - a41a33)) 
		ymm_a12_D = _mm256_sub_ps(_mm256_sub_ps(ymm_a12a23_D, ymm_a12a21_D), ymm_a12a24_D); 

		/// ******Determinants for a13*****************//
		__m256	ymm_a13a21, ymm_a13a22, ymm_a13a24, ymm_a31a42, ymm_a41a32;
		__m256	ymm_a13a21_D, ymm_a13a22_D, ymm_a13a24_D, ymm_a13_D;
		__m256	ymm_a31a42_a41a32;

		// calculate a13a21(a32a44 - a42a34) for 8 matrices
		ymm_a13a21 = _mm256_mul_ps(ymm_a13, ymm_a21);
		ymm_a13a21_D = _mm256_mul_ps(ymm_a13a21, ymm_a32a44_a34a42); 
		
		// calculate a13a22(a31a43 - a41a33) for 8 matrices
		ymm_a13a22 = _mm256_mul_ps(ymm_a13, ymm_a22);
		ymm_a13a22_D = _mm256_mul_ps(ymm_a13a22, ymm_a31a43_a41a33); 
		
		// calculate a13a24(a31a42 - a41a32) for 8 matrices
		ymm_a13a24 = _mm256_mul_ps(ymm_a13, ymm_a24);
		ymm_a31a42 = _mm256_mul_ps(ymm_a31, ymm_a42);
		ymm_a41a32 = _mm256_mul_ps(ymm_a41, ymm_a32);
		ymm_a31a42_a41a32 = _mm256_sub_ps(ymm_a31a42, ymm_a41a32);
		ymm_a13a24_D = _mm256_mul_ps(ymm_a13a24, ymm_a31a42_a41a32); 

		// calculate partial determinant for 8 matrices: (a13a21(a32a44 - a42a34) + 
		// a13a22(a31a43 - a41a33) + a13a24(a31a42 - a41a32)) 
		ymm_a13_D = _mm256_add_ps(_mm256_sub_ps(ymm_a13a21_D, ymm_a13a22_D), ymm_a13a24_D); 

		/// ******Determinants for a14*****************//
		__m256	ymm_a14a21, ymm_a14a22, ymm_a14a23;
		__m256	ymm_a14a21_D, ymm_a14a22_D, ymm_a14a23_D, ymm_a14_D;

		// calculate a14a21(a32a43 - a42a33) for 8 matrices
		ymm_a14a21 = _mm256_mul_ps(ymm_a14, ymm_a21);
		ymm_a14a21_D = _mm256_mul_ps(ymm_a14a21, ymm_a32a43_a42a33); 
		
		// calculate a14a22(a31a43 - a41a33) for 8 matrices
		ymm_a14a22 = _mm256_mul_ps(ymm_a14, ymm_a22);
		ymm_a14a22_D = _mm256_mul_ps(ymm_a14a22, ymm_a31a43_a41a33); 
		
		// calculate a14a23(a31a42 - a41a32) for 8 matrices
		ymm_a14a23 = _mm256_mul_ps(ymm_a14, ymm_a23);
		ymm_a14a23_D = _mm256_mul_ps(ymm_a14a23, ymm_a31a42_a41a32); 
		
		// calculate partial determinant for 8 matrices: (a14a21(a32a43 - a42a33)) + 
		// a14a22(a31a43 - a41a33) + a14a23(a31a42 - a41a32)) 
		ymm_a14_D = _mm256_sub_ps(_mm256_sub_ps(ymm_a14a22_D, ymm_a14a21_D), ymm_a14a23_D); 

		// Calculate final Determinant value for 8 matrices: addition of 
		// determinants for a11, a12, a13, a14
		ymm_a11_D = _mm256_add_ps(ymm_a11_D, ymm_a12_D);
		ymm_a13_D = _mm256_add_ps(ymm_a13_D, ymm_a14_D);
		ymm_a11_D = _mm256_add_ps(ymm_a11_D, ymm_a13_D); 

		_mm256_store_ps((float *)pOut, ymm_a11_D);
	
	}
	return;
}

//the ReferenceKernelProcessBuffer function should contain reference code for the kernel
void ReferenceKernelProcessBuffer()
{
	float* pInput = InputBuffer;
	float* pRef = ReferenceBuffer;

	for (int numMats = 0; numMats < NUM_MATS; numMats++)
	{
		*pRef = evaluateDeterminant(pInput, 4);
		pInput += MAT_SIZE;
		pRef++;
	}
}

//the function should check that the kernel generated results are valid
bool ValidateResult()
{
	float *pBuffer1 = OutputBuffer;
	float *pBuffer2 = ReferenceBuffer;
	bool flag = true;

	for (int i = 0; i < NUM_MATS; i++) {
		if (*(pBuffer1 + i) != *(pBuffer2 + i)){
			cout << "Out[" << i << "] = " << *(pBuffer1 + i)
			<< " <> Ref[" << i  << "] = " << *(pBuffer2 + i) << endl;
			flag = false;
		}
	}

	return flag;
}


int main(int argc, char * argp[])	{
	
	cout << "Welcome to Determinat4x4Matrices" << endl;
	
	//Initialization steps
	SetKernelParameters(argc, argp);
	AllocateBuffers(32);
	KernelInit();
	ReferenceKernelProcessBuffer();

	//Calculate Determinant using Intel(R) AVX instructions
	avx_determinant();
	if(ValidateResult() == true)	{
		cout << "256-bit results matched for evaluation of a determinant" << endl;
	}
	else	{
		cout << "256-bit results failed for evaluation of a determinant" << endl;
	}

	//Calculate Determinant using Intel(R) SSE instructions
	sse_determinant();
	if(ValidateResult() == true)	{
		cout << "128-bit results matched for evaluation of a determinant" << endl;
	}
	else	{
		cout << "128-bit results failed for evaluation of a determinant" << endl;
	}
	
	//Cleanup and exit
	DeAllocateBuffers();

	

	return 0;
}