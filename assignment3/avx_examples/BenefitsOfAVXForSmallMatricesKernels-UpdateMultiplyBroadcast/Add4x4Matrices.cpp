#include <iostream>
#include <math.h>
#include <gmmintrin.h>

using namespace std;

#define MAT_WIDTH 10000
#define MAT_HEIGHT 10000

float* InputBuffer1 = NULL;
float* InputBuffer2 = NULL;
float* OutputBuffer = NULL;
float* ReferenceBuffer = NULL;
// loop count can be changed to more than 1 if want to iterate the addition of 4x4 matrices
// for more than 1 iterations for performance timing purposes.
int loopCount = 1;


void SetKernelParameters(int argc, char* argv[])
{
	if(argc == 2)
	{
		loopCount = atoi(argv[1]);
	}
	else if(argc > 2)
	{//first two parameters are the exe name and the kernel number
		cout << "Add4x4Matrices Usage" << endl;
		cout << " No command line arguments are required." << endl;
		cout << " The optional argument = number of times to run the 4x4 Addiiton of matrix." << endl;
		exit(1);
	}
	return;
}

//Memory allocations could be made in the AllocateBuffers function
int AllocateBuffers(int AlignmentBytes)
{
	//calculate necessary buffer size
	int iBufferSizeBytes = MAT_WIDTH*MAT_HEIGHT*sizeof(float);
	InputBuffer1 = (float*) _mm_malloc(iBufferSizeBytes, AlignmentBytes); //should be 32bytes alignment for AVX
	InputBuffer2 = (float*) _mm_malloc(iBufferSizeBytes, AlignmentBytes); //should be 32bytes alignment for AVX
	OutputBuffer = (float*) _mm_malloc(iBufferSizeBytes, AlignmentBytes); //should be 32bytes alignment for AVX
	ReferenceBuffer = (float*) _mm_malloc(iBufferSizeBytes, AlignmentBytes); //should be 32bytes alignment for AVX
	return 0;
}

//Memory free could be made in the DeAllocateBuffers function
int DeAllocateBuffers()
{
	//free allocated buffers
	_mm_free(InputBuffer1);
	_mm_free(InputBuffer2);
	_mm_free(OutputBuffer);
	_mm_free(ReferenceBuffer);
	return 0;
}

//The KernelInit function will be called before ProcessOptimizedKernel is called
//(It will be called more then once when generating traces - warm up )
void KernelInit()
{
	int iBufferHeight = MAT_HEIGHT;
	int iBufferWidth = MAT_WIDTH;

	for (int i = 0; i < iBufferHeight; i++)
	{
		for (int j = 0; j < iBufferWidth; j++)
		{
			InputBuffer1[i*iBufferWidth + j] = (float)j;
			InputBuffer2[i*iBufferWidth + j] = (float)j;
		}
	}
}


//The ProcessOptimizedKernel should contain the optimized kernel code
void sse_add()
{
	int lcount = loopCount;
	int iBufferHeight = MAT_HEIGHT;
	for (int i = 0; i < lcount; i++)
	{
		float* pImage1 = InputBuffer1;
		float* pImage2 = InputBuffer2;
		float* pOutImage = OutputBuffer;

		for(int iY = 0; iY < iBufferHeight; iY++)
		{
			//process 4 floats in parallel
			__m128 Xmm_A = _mm_load_ps(pImage1);
			__m128 Xmm_B = _mm_load_ps(pImage2);
			__m128 Xmm_C = _mm_add_ps (Xmm_A, Xmm_B);
			_mm_store_ps(pOutImage, Xmm_C);
			pOutImage+=4;
			pImage1+=4;
			pImage2+=4;
		}
	}
	return;
}

void avx_add()
{
	int lcount = loopCount;
	int iBufferHeight = MAT_HEIGHT;
	for (int i = 0; i < lcount; i++)
	{
		float* pImage1 = InputBuffer1;
		float* pImage2 = InputBuffer2;
		float* pOutImage = OutputBuffer;

		for(int iY = 0; iY < iBufferHeight; iY+= 2)
		{
			//process 4 floats in parallel
			__m256 Ymm_A = _mm256_load_ps(pImage1);
			__m256 Ymm_B = _mm256_load_ps(pImage2);
			__m256 Ymm_C = _mm256_add_ps (Ymm_A, Ymm_B);
			_mm256_store_ps(pOutImage, Ymm_C);
			pOutImage+=8;
			pImage1+=8;
			pImage2+=8;
		}
	}
	return;
}

//the ReferenceKernelProcessBuffer function should contain reference code for the kernel
void ReferenceKernelProcessBuffer()
{
	float* pInImage1 = InputBuffer1;
	float* pInImage2 = InputBuffer2;
	float* pRefImage = ReferenceBuffer;

	int iBufferHeight = MAT_HEIGHT;
	int iBufferWidth = MAT_WIDTH;

	for(int iY = 0; iY < iBufferHeight; iY++)
	{
		for(int iX = 0; iX < iBufferWidth; iX++)
		{
			*pRefImage = (*pInImage1)+(*pInImage2);
			pRefImage++;
			pInImage1++;
			pInImage2++;
		}
	}

	return;
}

//the function should check that the kernel generated results are valid
bool ValidateResult()
{
	float* pRefImage = ReferenceBuffer;
	float* pOutImage = OutputBuffer;

	int iBufferHeight = MAT_HEIGHT;
	int iBufferWidth = MAT_WIDTH;

	for(int iY = 0; iY < iBufferHeight; iY++)
	{
		for(int iX = 0; iX < iBufferWidth; iX++)
		{
			//printf("out = %f, ref = %f\n", *pOutImage, *pRefImage);
			if(*pRefImage != *pOutImage)
			{
				return false; //compare failed
			}
			pRefImage++;
			pOutImage++;
		}
	}
	return true; //compare passed
}


int main(int argc, char * argp[])	{

	cout << "Welcome to Add4x4Matrices" << endl;

	//Initialization steps
	SetKernelParameters(argc, argp);
	AllocateBuffers(32);
	KernelInit();
	ReferenceKernelProcessBuffer();

	//Do add using Intel(R) AVX instructions
	avx_add();
	if(ValidateResult() == true)	{
		cout << "256-bit results matched for Add of 4x4 matrices" << endl;
	}
	else	{
		cout << "256-bit results failed for Add of 4x4 matrices" << endl;
	}

	//Do add using Intel(R) SSE instructions
	sse_add();
	if(ValidateResult() == true)	{
		cout << "128-bit results matched for Add of 4x4 matrices" << endl;
	}
	else	{
		cout << "128-bit results failed for Add of 4x4 matrices" << endl;
	}

	//Cleanup and exit
	DeAllocateBuffers();

	return 0;
}
