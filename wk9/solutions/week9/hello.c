#include <stdio.h>
#include <pthread.h>

#define THREADS 4

static void *worker(void *arg) {
    const int argument = *(int *)arg;
    printf("Hello from %d\n", argument);
    return NULL;
}

int main(void) {
    int args[THREADS] = { 0, 1, 3, 4 };
    pthread_t thread_ids[THREADS];

    for (size_t i = 0; i < THREADS; ++i) {
        // creates and passes the argument to the thread
        if (pthread_create(thread_ids + i, NULL, worker, args + i) != 0) {
            perror("pthread_create failed");
            return 1;
        }
    }

    // wait for the all the threads to finish
    for (size_t i = 0; i < THREADS; ++i) {
        if (pthread_join(thread_ids[i], NULL) != 0) {
            perror("pthread_join failed");
            return 1;
        }
    }
}
