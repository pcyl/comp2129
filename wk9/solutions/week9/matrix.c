#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <pthread.h>

/** A square matrix of given width */
#define WIDTH 1024
#define IDX(x, y) ((y) * WIDTH + (x))

#define THREADS 2
#define CHUNK (WIDTH / THREADS)

struct argument {
    const float *a;
    const float *b;
    float *res;
    size_t tid;
};

static void *worker(void *argument) {
    struct argument arg = *(struct argument *)argument;

    // sum the values from start to end
    const size_t start = arg.tid * CHUNK;
    const size_t end =
        arg.tid == THREADS - 1 ? WIDTH : (arg.tid + 1) * CHUNK;

    for (size_t y = start; y < end; ++y) {
        for (size_t x = 0; x < WIDTH; x++) {
            for (size_t k = 0; k < WIDTH; k++) {
                arg.res[IDX(x, y)] += arg.a[IDX(k, y)] * arg.b[IDX(x, k)];
            }
        }
    }

    return NULL;
}

/**
 * Matrix multiplication
 * Important: this function assumes that res is zero initalised
 */
static void multiply(const float *a, const float *b, float *res) {
    // create the jobs for each thread
    struct argument *args = malloc(sizeof(struct argument) * THREADS);
    if (!args) {
        perror("malloc");
        exit(1);
    }

    for (size_t i = 0; i < THREADS; ++i) {
        args[i] = (struct argument) {
            .a = a,
            .b = b,
            .res = res,
            .tid = i,
        };
    }

    // stores the thread IDs
    pthread_t thread_ids[THREADS];

    // launch
    for (size_t i = 0; i < THREADS; ++i) {
        if (pthread_create(thread_ids + i, NULL, worker, args + i) != 0) {
            perror("pthread_create failed");
            exit(1);
        }
    }

    // wait for the all the threads to finish
    for (size_t i = 0; i < THREADS; ++i) {
        if (pthread_join(thread_ids[i], NULL) != 0) {
            perror("pthread_join failed");
            exit(1);
        }
    }

    free(args);
}

/**
 * Create a Hadamard matrix, if H is Hadamard matrix, then
 * HH^T = nI, where I is the identity matrix and n is the width.
 * It makes it easy to verify that matrix multiplication was done correctly.
 *
 * Sylverster's construction (implemented here)
 * only works for matrices that have width that is a power of 2
 *
 * Note that this construction produces matrices that are symmentric
 */
static void hadamard(float *m) {
    assert(((WIDTH - 1) & WIDTH) == 0); // must be a power of 2

    size_t w = WIDTH, quad_size = 1;

    m[0] = 1;
    while ((w >>= 1) != 0) {
        // duplicate the upper left quadrant into the other three quadrants
        for (size_t y = 0; y < quad_size; ++y) {
            for (size_t x = 0; x < quad_size; ++x) {
                const float v = m[IDX(x, y)];
                m[IDX(x + quad_size, y)] = v;
                m[IDX(x, y + quad_size)] = v;
                m[IDX(x + quad_size, y + quad_size)] = -v;
            }
        }

        quad_size *= 2;
    }
}

/** Prints a matrix */
static void print(const float *m) {
    for (size_t y = 0; y < WIDTH; y++) {
        for (size_t x = 0; x < WIDTH; x++) {
            printf("%5.1f ", m[IDX(x, y)]);
        }
        puts("");
    }
}

int main(void) {
    // allocate memory for the matrices
    float *a, *b, *c;
    a = malloc(WIDTH * WIDTH * sizeof(float));
    b = malloc(WIDTH * WIDTH * sizeof(float));

    // initalise the result matrix
    c = calloc(WIDTH * WIDTH, sizeof(float));

    hadamard(a);
    memcpy(b, a, WIDTH * WIDTH * sizeof(float));

    // compute the result
    multiply(a, b, c);

    // verify the result
    for (size_t y = 0; y < WIDTH; y++) {
        for (size_t x = 0; x < WIDTH; x++) {
            assert(x == y ? c[IDX(x, y)] == WIDTH : c[IDX(x, y)] == 0);
        }
    }

    print(c);

    free(a);
    free(b);
    free(c);

    return 0;
}
