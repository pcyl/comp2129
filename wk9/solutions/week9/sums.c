#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

#define ARRAY_LEN 1000000
#define REPEATS 1000
#define THREADS 4
#define CHUNK (ARRAY_LEN / THREADS)

struct argument {
    long long *array;
    size_t tid;

    long long results;
};

static void *worker(void *arg) {
    struct argument *argument = (struct argument *)arg;

    // sum the values from start to end
    const size_t start = argument->tid * CHUNK;
    const size_t end =
        argument->tid == THREADS - 1 ? ARRAY_LEN : (argument->tid + 1) * CHUNK;

    long long result = 0;
    for (size_t i = start; i < end; ++i) {
         result += argument->array[i];
    }

    argument->results = result;

    return NULL;
}

int main(void) {
    // create an array of numbers from 1 to ARRAY_LEN
    long long *numbers = malloc(sizeof(long long) * ARRAY_LEN);
    if (!numbers) {
        perror("malloc");
        return 1;
    }

    for (size_t i = 0; i < ARRAY_LEN; ++i) {
        numbers[i] = i + 1;
    }

    // create the jobs for each thread
    struct argument *args = malloc(sizeof(struct argument) * THREADS);
    if (!args) {
        perror("malloc");
        return 1;
    }

    // repeat many times since our computers are too fast
    for (size_t j = 0; j < REPEATS; ++j) {
        long long final_result = 0;

        for (size_t i = 0; i < THREADS; ++i) {
            args[i] = (struct argument) {
                .array   = numbers,
                .tid     = i,
                .results = 0
            };
        }

        // stores the thread IDs
        pthread_t thread_ids[THREADS];

        // launch
        for (size_t i = 0; i < THREADS; ++i) {
            if (pthread_create(thread_ids + i, NULL, worker, args + i) != 0) {
                perror("pthread_create failed");
                return 1;
            }
        }

        // wait for the all the threads to finish
        for (size_t i = 0; i < THREADS; ++i) {
            if (pthread_join(thread_ids[i], NULL) != 0) {
                perror("pthread_join failed");
                return 1;
            }
        }

        // get the final result
        for (size_t i = 0; i < THREADS; ++i) {
            final_result += args[i].results;
        }

        printf("%zu: The final result is: %lld\n", j, final_result);
    }

    free(args);
    free(numbers);
}
