#!/bin/sh
total=0

for cfile in *.c
do
  nlines=$(wc -l < $cfile)
  total=$(($total + $nlines))
done

echo "$total lines of code found."
