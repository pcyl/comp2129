#!/usr/bin/env sh
# Exercise 1

for testfile in tests/*.in
do
  name=$(basename "$testfile" | cut -d "." -f 1)
  echo "Running test for $name"
  cat $testfile | $1 > tests/$name.txt | diff - tests/$name.out
done

echo "finished running tests"
