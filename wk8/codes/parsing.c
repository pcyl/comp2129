#define _GNU_SOURCE

#include <assert.h>
#include <string.h>

static char *my_strtok_r(char *str, const char *delim, char **saveptr);
static int my_strcasecmp(const char *s1, const char *s2);

static void test_strtok_r(void) {
    char test1[] = "hello world";
    char *saveptr;

    assert(strcmp(my_strtok_r(test1, " ", &saveptr), "hello") == 0);
    assert(strcmp(my_strtok_r(NULL, " \t", &saveptr), "world") == 0);
    assert(my_strtok_r(NULL, "\t ", &saveptr) == NULL);

    char test2[] = "    ";
    assert(my_strtok_r(test2, "\t ", &saveptr) == NULL);

    char test3[] = "";
    assert(my_strtok_r(test3, " ", &saveptr) == NULL);

    char test4[] = " \t  prefix space";
    assert(strcmp(my_strtok_r(test4, " \t", &saveptr), "prefix") == 0);
    assert(strcmp(my_strtok_r(NULL, " \t", &saveptr), "space") == 0);
    assert(my_strtok_r(NULL, "\t ", &saveptr) == NULL);

    char test5[] = " trailing space \t test  \t ";
    assert(strcmp(my_strtok_r(test5, " \t", &saveptr), "trailing") == 0);
    assert(strcmp(my_strtok_r(NULL, " \t", &saveptr), "space") == 0);
    assert(strcmp(my_strtok_r(NULL, " \t", &saveptr), "test") == 0);
    assert(my_strtok_r(NULL, "\t ", &saveptr) == NULL);
}

static void test_strcase_cmp(void) {
    assert(my_strcasecmp("", "") == 0);
    assert(my_strcasecmp("abc", "abc") == 0);
    assert(my_strcasecmp("ABC", "abc") == 0);
    assert(my_strcasecmp("abc", "ABC") == 0);
    assert(my_strcasecmp("abcd", "ef") < 0);
    assert(my_strcasecmp("ef", "abcd") > 0);
    assert(my_strcasecmp("abcd", "abcde") < 0);
    assert(my_strcasecmp("abcde", "abcde") == 0);
}

int main(void) {
    test_strtok_r();
    test_strcase_cmp();
    return 0;
}
