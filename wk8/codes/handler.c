#include <stdbool.h>
#include <stdio.h>
#include <signal.h>

#include <unistd.h> // needed for sleep

int main(void) {
    signal(SIGINT, SIG_IGN);

    while (true) {
        sleep(1);
    }

    return 0;
}
