#include <stdio.h>

#define LEN(x) (sizeof(x)/sizeof(x[0]))

struct animal {
    int cuteness;
    char *name;
};

// A comparator for integers
static int comp_int(const void *x, const void *y) {
    return (*(int *)x) - (*(int *)y);
}

// A comparator for animals, in terms of cuteness
static int comp_animal(const void *x, const void *y) {
    // TODO:
}

// Bubble sort
static void bubble_sort(void *base, size_t nmemb, size_t size,
        int (*compar)(const void *, const void *)) {
    // TODO:
}

int main(void) {
    int array[] = { 9, 1, 4, 1, 2, 10 };
    struct animal animals[] = {
        { -2, "anchovies" },
        { 10, "kitten"},
        { 11, "puppy" },
        { -1, "trout" },
    };

    bubble_sort(array, LEN(array), sizeof(array[0]), comp_int);

    puts("The integers from the smallest to the largest:");
    for (size_t i = 0; i < LEN(array); ++i) {
        printf("%d\n", array[i]);
    }

    bubble_sort(animals, LEN(animals), sizeof(animals[0]), comp_animal);

    puts("The animals ranked from the cutest to the least:");
    for (size_t i = 0; i < LEN(animals); ++i) {
        printf("%d %s\n", animals[i].cuteness, animals[i].name);
    }

    return 0;
}
