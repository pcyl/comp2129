#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <ctype.h>

#include "snapshot.h"

#define DELIMITER " "

enum COMMAND { CMD_BYE, CMD_HELP, CMD_LIST_KEYS, CMD_LIST_ENTRIES,
               CMD_LIST_SNAPSHOTS, CMD_GET, CMD_DEL, CMD_PURGE,
               CMD_SET, CMD_PUSH, CMD_APPEND, CMD_PICK, CMD_PLUCK,
               CMD_POP, CMD_DROP, CMD_ROLLBACK, CMD_CHECKOUT, CMD_SNAPSHOT,
               CMD_MIN, CMD_MAX, CMD_SUM, CMD_LEN, CMD_REV, CMD_UNIQ, CMD_SORT, CMD_END};
const char *COMMAND_STR[] = { "BYE", "HELP", "LIST KEYS", "LIST ENTRIES", "LIST SNAPSHOTS",
                              "GET", "DEL", "PURGE", "SET", "PUSH", "APPEND", "PICK", "PLUCK",
                              "POP","DROP", "ROLLBACK", "CHECKOUT", "SNAPSHOT", "MAX", "SUM", "LEN",
                              "REV", "UNIQ", "SORT"};

struct command
{
    enum COMMAND tag;
    char key[MAX_KEY_LENGTH];
    char **args;
};

/* Initialise lists */
static entry* headEntry = NULL;
static snapshot* headSnapshot = NULL;

// Snapshot id counter
int snapshotCount = 0;

/* Converts lowercase to uppercase string */
void my_toupper(char* str)
{
    for(int i=0; i<=strlen(str); i++)
    {
        if(str[i]>=97&&str[i]<=122)
        {
            str[i]=str[i]-32;
        }
    }
}

void *check(void *ptr) {
    if (!ptr) {
        abort(); // no point cleaning up, just let the program die
    }
    return ptr;
}

/* Finds command from token
    part of this was from todo*/
static enum COMMAND find_command(const char *str, const char *str2)
{
    int successful = FALSE;
    enum COMMAND result = CMD_END;
    char *command = check(malloc(sizeof(char *) * MAX_LINE_LENGTH));

    if (!str2) // Check for NULL
    {
        strcpy(command, (char *) str);
    }
    else // Interpret as two word command
    {
        strcpy(command, (char *) str);
        strcat(command, " ");
        strcat(command, (char *) str2);
    }

    // printf("command concatenated: %s\n", command);
    // puts("currently in find command");

    for (enum COMMAND i = CMD_BYE; i < CMD_END - 1; ++i)
    {
        // printf("command_str index: %d\n", i);
        if (strcmp(command, COMMAND_STR[i]) == 0)
        {
            //return i;
             result = i;
             successful = TRUE;
             break;
        }
    }

    //return CMD_END;
    free(command);
    if (successful == TRUE)
    {
        return result;
    }
    else
    {
        return CMD_END;
    }
}

/* Check command for validity
    part of this was from todo*/
static int parse_command(int argc, char **argv, struct command *cmd)
{
    //puts("currently in parse command");
    enum COMMAND tag;

    my_toupper(argv[0]);
    if (strcmp(argv[0], "LIST") == 0) // Check for LIST command
    {
        //puts("processing a LIST command");
        my_toupper(argv[1]);
        tag = find_command(argv[0], argv[1]);
    }
    else
    {
        tag = find_command(argv[0], NULL);
    }

    cmd->tag = tag;

    switch (tag)
    {
        case CMD_GET:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_DEL:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_PURGE:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_SET:
            { // Do first
                if (argc < 3)
                {
                    goto invalid;
                }
                // UPTO HERE
                char key[MAX_KEY_LENGTH];
                strcpy(key, argv[1]);

                char **args = check(calloc(sizeof(char *), MAX_LINE_LENGTH));

                int count = 0; // Count for debugging purposes

                for (int i = 2; i < argc; i++)
                {
                    args[i-2] = argv[i]; // Set arguments for set
                    count++; // Increment for debugging
                }

                // debugging print key and values
                printf("key: %s\n", cmd->key);
                for (int j = 0; j < count; j++)
                {
                    printf("args[%d]: %s, ", j, args[j]);
                }
                puts("");
                // ==============================

                *cmd = (struct command) { // Set command tag and key
                    .tag = tag,
                    // .key = key,
                    .args = args
                };
                strcpy(cmd->key, key); // Since char

                free(args);
            }
            break;

        case CMD_PUSH:
            { // Similar to SET
                if (argc < 3)
                {
                    goto invalid;
                }
                // char *key = argv[1];
                char key[MAX_KEY_LENGTH];
                strcpy(key, argv[1]);

                char **args = check(calloc(sizeof(char *), MAX_LINE_LENGTH));

                int count = 0; // Count for debugging purposes

                for (int i = 2; i < argc; i++)
                {
                    args[i-2] = argv[i]; // Set arguments for set
                    count++; // Increment for debugging
                }

                // debugging print key and values
                printf("key: %s\n", cmd->key);
                for (int j = 0; j < count; j++)
                {
                    printf("args[%d]: %s, ", j, args[j]);
                }
                puts("");
                // ==============================

                *cmd = (struct command) { // Set command tag and key
                    .tag = tag,
                    // .key = key,
                    .args = args
                };
                strcpy(cmd->key, key); // Since char

                free(args);
            }
            break;

        case CMD_APPEND:
            { // Similar to SET
                if (argc < 3)
                {
                    goto invalid;
                }
                char key[MAX_KEY_LENGTH] = "";
                strcpy(key, argv[1]);

                char **args = check(calloc(sizeof(char *), MAX_LINE_LENGTH));

                int count = 0; // Count for debugging purposes

                for (int i = 2; i < argc; i++)
                {
                    args[i-2] = argv[i]; // Set arguments for set
                    count++; // Increment for debugging
                }

                // debugging print key and values
                printf("key: %s\n", key);
                for (int j = 0; j < count; j++)
                {
                    printf("args[%d]: %s, ", j, args[j]);
                }
                puts("");
                // ==============================

                *cmd = (struct command) { // Set command tag and key
                    .tag = tag,
                    // .key = key,
                    .args = args
                };
                strcpy(cmd->key, key); // Since char

                free(args);
            }
            break;

        case CMD_PICK:
            if (argc != 3)
            {
                goto invalid;
            }
            break;

        case CMD_PLUCK:
            if (argc != 3)
            {
                goto invalid;
            }
            break;

        case CMD_POP:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_DROP:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_ROLLBACK:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_CHECKOUT:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_MIN:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_MAX:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_SUM:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_LEN:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_REV:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_UNIQ:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_SORT:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_LIST_KEYS:
        case CMD_LIST_ENTRIES:
        case CMD_LIST_SNAPSHOTS:
        case CMD_SNAPSHOT:
        case CMD_BYE:
        case CMD_HELP:
        case CMD_END:
            *cmd = (struct command) { .tag = tag };
            break;
    }
    return TRUE;

invalid:
    return FALSE;
}

/* Execute command
    part of this was from todo*/
static int do_command(const struct command *cmd, int argc)
{
    printf("enum command tag: %d\n\n", cmd->tag);
    puts("///////// OUTPUT //////////");
    switch(cmd->tag)
    {
        // List Commands
        case CMD_LIST_KEYS:
            if (list_entryEmpty(headEntry) == TRUE)
            {
                puts("no keys");
            }
            else
            {
                print_listkey(headEntry);
            }
            break;

        case CMD_LIST_ENTRIES:
            if (list_entryEmpty(headEntry) == TRUE)
            {
                puts("no entries");
            }
            else
            {
                puts("entries not empty");
            }
            break;

        case CMD_LIST_SNAPSHOTS:
            if (list_snapshotEmpty(headSnapshot) == TRUE)
            {
                puts("no snapshots");
            }
            else
            {
                puts("snapshots not empty");
            }
            break;
        // End List Commands

        case CMD_GET:
            {
                if (list_entryEmpty(headEntry) == TRUE)
                {
                    puts("no such key");
                }
            }
            break;

        case CMD_DEL:
            break;

        case CMD_PURGE:
            break;

        case CMD_SET:
            {
                // Debugging
                printf("Key: %s, ", cmd->key);
                printf("Values: ");
                int argcCount = argc - 2;
                for (int i = 0; i < argcCount; i++)
                {
                    printf("%d ", atoi(cmd->args[i]));
                }
                puts("");
                // ===============
                int nums[100];
                for(int i = 0; i < argcCount; i++)
                {
                    nums[i] = atoi(cmd->args[i]);
                }

                for(int i = 0; i < argcCount; i++)
                {
                    printf("%d ", nums[i]);
                }
                puts("");
                add_entrytail(headEntry, (char*)cmd->key);
                //
                static value *headValue;
                headValue = add_valuetail(nums, argcCount);
                // link_entryvalue(entry, headValue);
                //
                print_listkey(headEntry);
                puts("");
                print_listvalue(add_valuetail(nums, argcCount));
                puts("ok");
                // free(newEntry);
                // free(newValueHead);
                // =========
            }
            break;

        case CMD_PUSH:
            {
                // Debugging
                printf("Key: %s, ", cmd->key);
                printf("Values: ");
                for (int i = 0; i < argc - 2; i++)
                {
                    printf("%s ", cmd->args[i]);
                }
                puts("");
                // =================

                push_entry(&headEntry, (char *) cmd->key);
                print_listkey(headEntry);
                puts("ok");
            }
            break;

        case CMD_APPEND:
            break;

        case CMD_PICK:
            break;

        case CMD_PLUCK:
            break;

        case CMD_POP:
            break;

        case CMD_DROP:
            break;

        case CMD_ROLLBACK:
            break;

        case CMD_CHECKOUT:
            break;

        case CMD_SNAPSHOT:
            puts("reached snapshot");
            break;

        case CMD_MIN:
            break;

        case CMD_MAX:
            break;

        case CMD_SUM:
            break;

        case CMD_LEN:
            break;

        case CMD_REV:
            break;

        case CMD_UNIQ:
            break;

        case CMD_SORT:
            break;

        case CMD_HELP:
            printf("%s", HELP);
            break;

        case CMD_BYE:
            puts("bye");
            return TRUE;
            break;

        case CMD_END:
            puts("no such command");
            break;
    }
    return FALSE;
}

/* Main
    tokenising from todo */
int main(void)
{
    headEntry = check(calloc(1, sizeof(entry*)));
    strcat(headEntry->key, "headEntry");
    // headSnapshot = check(calloc(1, sizeof(snapshot*)));

    while(TRUE)
    {
        int terminate = 0;
        printf("> ");
        char line[MAX_LINE_LENGTH];

        // DEPRECATED
        // char *line = calloc(sizeof(char *), MAX_LINE_LENGTH);
        // if (!(read_line(&line, stdin)))
        // {
        //     break;
        // }
        //
        // char buffer[MAX_LINE_LENGTH];

        fgets(line, MAX_LINE_LENGTH, stdin);

        for (int i = 0; i < strlen(line); i++)
        {
            if (line[i] == '\n')
            {
                line[i] = '\0';
            }
        }

        // strcat(line, buffer);


        int argc = 0;
        char *token = strtok(line, DELIMITER);
        // size_t capacity = 1;
        char **argv = check(calloc(1, sizeof(char*) * MAX_LINE_LENGTH));

        while(token)
        {
            argv[argc++] = token;

            // if (argc == capacity)
            // {
            //     capacity *= 2;
            //     argv = realloc(argv, sizeof(char*) * capacity);
            // }

            token = strtok(NULL, DELIMITER);
        }

        if (argc != 0)
        {
            puts("\n/////// DEBUG COMMANDS ///////");
            // Print the tokenised items for debugging
            for (int i = 0;  i < argc; i++)
            {
                printf("arg[%d] tokenised: %s\n", i, argv[i]);
            }

            struct command cmd;
            if(parse_command(argc, argv, &cmd) == TRUE)
            {
                terminate = do_command(&cmd, argc);
            }
            else
            {
                puts("\n////////// OUTPUT //////////");
                puts("invalid input");
            }
        }

        puts("");
        free(argv);
        //free(line);

        // Check to exit program
        if (terminate == TRUE)
        {
            break;
        }
    }
    free(headEntry);
    free(headSnapshot);
    return 0;
}
