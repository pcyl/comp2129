#ifndef SNAPSHOT_H
#define SNAPSHOT_H

#define MAX_KEY_LENGTH 16
#define MAX_LINE_LENGTH 1024

#define TRUE 1
#define FALSE 0

typedef struct value value;
typedef struct entry entry;
typedef struct snapshot snapshot;
typedef struct node node;

struct value
{
    value* prev;
    value* next;
    int value;
};

struct entry
{
    char key[MAX_KEY_LENGTH];
    entry* prev;
    entry* next;
    value* values;
};

struct snapshot
{
    int id;
    snapshot* prev;
    snapshot* next;
    entry* entries;
};

int convert(char num[]) // Convert char string to signed int
{
    char digit;
    int result = 0;
    int sign = 1;
    // Check for minus
    if (num[0] == '-')
      {
	num++;   // Skip over the minus.
	sign = -1;
      }

    while((digit = *num))   // Stop the loop if we are at the end of the string.
      {
	result *= 10; // Shift digit by one place
	result += digit - '0';
	num++;  // Go to next digit.
      }

    return result * sign;
}

// LINKED LIST FUNCTIONS
static value* add_valuetail(int *nums, int num_args)
{
    struct value *head = NULL;
    head = calloc(1, sizeof(value *));
    if (num_args == 1)
    {
        head->next = NULL;
        head->prev = NULL;
        head->value = nums[0];
        return head;
    }
    else
    {
        struct value *temp = head;
        for (int i = 0; i < num_args; i++)
        {
            temp->next = malloc(sizeof(value *));
            (temp->next)->value = nums[i];
            (temp->next)->prev = temp;
            (temp->next)->next = NULL;
            temp = temp->next;
            printf("%d ", temp->value);
        }
        puts("");
        return head;
    }
    // value *temp = malloc(sizeof(value*));
    // temp->value = arg;
    // if(current == NULL)
    // {
    //     current = temp;
    //     return;
    // }
    //
    // while(current->next != NULL) {
    //     current = current->next;
    // }
    // // create a new Node and insert the item
    // current->next = calloc(1, sizeof(value *));
    // (current->next)->prev = current;
    // current = current->next;
    // current->value = arg;
    // current->next = NULL;
    // printf("%d ", current->value);
}

void link_entryvalue(struct entry **entry, struct value *valueslist)
{
    (*entry)->values = valueslist;
}

void push_entry(struct entry **current, char key[MAX_KEY_LENGTH])
{
	if(strcmp((*current)->key, "headEntry") == 0)
    {
		strcpy((*current)->key, key);
		return;
	}
    entry * new;
    new = malloc(sizeof(entry *));

    strcpy(new->key, key);
    new->next = *current;
    *current = new;
    (*current)->next->prev = *current;
    free(new);
}

void add_entrytail(struct entry *current,char key[MAX_KEY_LENGTH])
{
    if(strcmp(current->key, "headEntry") == 0)
    {
        strcpy(current->key, key);
        return;
    }
    while(current->next != NULL) {
        current = current->next;
    }
    // create a new Node and insert the item
    current->next = malloc(sizeof(entry*));
    (current->next)->prev = current;
    current = current->next;
    strcpy(current->key, key);
    current->next = NULL;
    return;
}

int list_entryEmpty(struct entry *head)
{
    if (head->next == NULL)
    {
        return TRUE;
    }
    return FALSE;
}

int list_snapshotEmpty(struct snapshot *head)
{
    if (head->next == NULL)
    {
        return TRUE;
    }
    return FALSE;
}

void print_listkey(struct entry *current)
{
    while(current != NULL) {
        printf("%s\n", current->key);
        // printf("[ ");
        // while(current->values != NULL)
        // {
        //     printf("%d", current->values->value);
        //     current->values = current->values->next;
        // }
        // printf(" ]\n");
        current = current->next;
    }
}

void print_listvalue(struct value *current)
{
    while(current != NULL) {
        printf("%d ", current->value);
        current = current->next;
    }
    puts("");
}
void print_listkeyRev(struct entry *head)
{
    struct entry* current = head;
    if(current == NULL) return; // empty list, exit
    // Going to last Node
    while(current->next != NULL) {
        current = current->next;
    }
    // Traversing backward using prev pointer
    printf("Reverse: ");
    while(current != NULL) {
        printf("%s ",current->key);
        current = current->prev;
    }
    printf("\n");
}

// int find(entry *current, int data) {
//     // First pointer is head aka dummy node with no data
//     // so we go to next one
//     current = current->next;
//
//     // Iterate through the linked list
//     while(current != NULL) {
//         if(current->data == data) {
//             return 1;
//         }
//         current = current->next;
//     }
//     return 0;
// }

// END LINKED LIST FUNCTIONS

const char* HELP =
    "BYE   clear database and exit\n"
    "HELP  display this help message\n"
    "\n"
    "LIST KEYS       displays all keys in current state\n"
    "LIST ENTRIES    displays all entries in current state\n"
    "LIST SNAPSHOTS  displays all snapshots in the database\n"
    "\n"
    "GET <key>    displays entry values\n"
    "DEL <key>    deletes entry from current state\n"
    "PURGE <key>  deletes entry from current state and snapshots\n"
    "\n"
    "SET <key> <value ...>     sets entry values\n"
    "PUSH <key> <value ...>    pushes each value to the front one at a time\n"
    "APPEND <key> <value ...>  append each value to the back one at a time\n"
    "\n"
    "PICK <key> <index>   displays entry value at index\n"
    "PLUCK <key> <index>  displays and removes entry value at index\n"
    "POP <key>            displays and removes the front entry value\n"
    "\n"
    "DROP <id>      deletes snapshot\n"
    "ROLLBACK <id>  restores to snapshot and deletes newer snapshots\n"
    "CHECKOUT <id>  replaces current state with a copy of snapshot\n"
    "SNAPSHOT       saves the current state as a snapshot\n"
    "\n"
    "MIN <key>  displays minimum entry value\n"
    "MAX <key>  displays maximum entry value\n"
    "SUM <key>  displays sum of entry values\n"
    "LEN <key>  displays number of entry values\n"
    "\n"
    "REV <key>   reverses order of entry values\n"
    "UNIQ <key>  removes repeated adjacent entry values\n"
    "SORT <key>  sorts entry values in ascending order\n";

#endif
