#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "snapshot.h"

entry* entry_head = NULL;
entry* entry_tail = NULL;

snapshot* snapshot_head = NULL;
snapshot* snapshot_tail = NULL;

#define MAX_BUFFER 100
#define BUFFER_SIZE 100

enum COMMAND { CMD_BYE, CMD_HELP, CMD_LIST_KEYS, CMD_LIST_ENTRIES,
               CMD_LIST_SNAPSHOTS, CMD_GET, CMD_DEL, CMD_PURGE,
               CMD_SET, CMD_PUSH, CMD_APPEND, CMD_PICK, CMD_PLUCK,
               CMD_POP, CMD_DROP, CMD_ROLLBACK, CMD_CHECKOUT, CMD_SNAPSHOT,
               CMD_MIN, CMD_MAX, CMD_SUM, CMD_LEN, CMD_REV, CMD_UNIQ, CMD_SORT,
               CMD_END};

const char *COMMAND_STR[] = { "BYE", "HELP", "LIST KEYS", "LIST ENTRIES",
                              "LIST SNAPSHOTS", "GET", "DEL", "PURGE", "SET",
                              "PUSH", "APPEND", "PICK", "PLUCK", "POP", "DROP",
                              "ROLLBACK", "CHECKOUT", "SNAPSHOT", "MIN", "MAX",
                              "SUM", "LEN", "REV", "UNIQ", "SORT", "END" };

/** Finds the command matching the string. This is taken from todo.c from 
    Week 6 Tutorial*/
static enum COMMAND find_command(const char *str1, const char *str2) {
    
    const char* full_command = malloc(MAX_LINE_LENGTH*sizeof(char));
    /**
     * If there is no second argument then only deal with the first one
     * otherwise combine the two arguments into a single string and 
     * find the command.
     */
    if (!str2) {
        full_command = str1;
    }
    else {
        sprintf((char *) full_command, "%s %s", str1, str2);
    }
    
    for (enum COMMAND i = CMD_BYE; i < CMD_END; ++i) {
        if (strcmp(full_command, COMMAND_STR[i]) == 0) {
            return i;
        }
    }
    free((char*)full_command);
    return CMD_END;
}

// Directly alters the string passed through and converts it all to uppercase
void str_toupper(char* str) {

    for(int i=0; i<=strlen(str); i++) {
        if(str[i]>=97&&str[i]<=122)
            str[i]=str[i]-32;
    }
}

//This determines which function is called from user and executes it
void functions ( int function_index ) {
  switch(function_index) {
    case 0:
            printf("bye");
            exit(1);
    case 1:
            printf(HELP);
            break;
    case 2:
            printf("You have invoked LIST KEYS\n");
            break;
    case 3:
            printf("You have invoked LIST ENTRIES\n");
            break;
    case 4:
            printf("You have invoked LIST SNAPSHOTS\n");
            break;
    case 5:
            printf("You have invoked GET\n");
            break;
    case 6:
            printf("You have invoked DEL\n");
            break;
    case 7:
            printf("You have invoked PURGE\n");
            break;
    case 8:
            printf("You have invoked SET\n");
            break;
    case 9:
            printf("You have invoked PUSH\n");
            break;
    case 10:
            printf("You have invoked APPEND\n");
            break;
    case 11:
            printf("You have invoked PICK\n");
            break;
    case 12:
            printf("You have invoked PLUCK\n");
            break;
    case 13:
            printf("You have invoked POP\n");
            break;
    case 14:
            printf("You have invoked DROP\n");
            break;
    case 15:
            printf("You have invoked ROLLBACK\n");
            break;
    case 16:
            printf("You have invoked CHECKOUT\n");
            break;
    case 17:
            printf("You have invoked SNAPSHOT\n");
            break;
    case 18:
            printf("You have invoked MIN\n");
            break;
    case 19:
            printf("You have invoked MAX\n");
            break;
    case 20:
            printf("You have invoked SUM\n");
            break;
    case 21:
            printf("You have invoked LEN\n");
            break;
    case 22:
            printf("You have invoked REV\n");
            break;
    case 23:
            printf("You have invoked UNIQ\n");
            break;
    case 24:
            printf("You have invoked SORT\n");
            break;
    case 25:
            printf("No such command\n");
            break;

  }
}

int main(void) {
  while (true) {

        printf("> ");
        char line[MAX_LINE_LENGTH];

        if (fgets(line, MAX_LINE_LENGTH, stdin) == NULL) {
            break;
        }

        for (int i = 0; i < strlen(line); i++) {
            if (line[i] == '\n') {
                line[i] = '\0';
            }
        }

        /**
        * Lines 181 through to lines 198 are taken from todo.c
        * from the week 6 tutorial.
        */
        // count the number of arguments in argc
        // save the the arguments in argv
        int argc = 0;
        size_t capacity = 1;
        char **argv = malloc(sizeof(char *) * capacity);

        // tokenise the commands
        char *token = strtok(line, " ");
        while (token) {
            argv[argc++] = token;

            // increase the capacity of the array if it's full
            if (argc == capacity) {
                capacity *= 2;
                argv = realloc(argv, sizeof(char *) * capacity);
            }
            token = strtok(NULL, " ");
        }

        /**
        * Checks if the first argument is equal to "LIST" or not
        * so that we know the next argument will be a
        * secondary command, and then it is passed through to
        * find_command after having the correct arguments converted
        * to upper case.
        */
        str_toupper(argv[0]);
        if ( strcmp(argv[0], "LIST") != 0) {
            functions(find_command(argv[0], NULL));
        }
        else {
            str_toupper(argv[1]);
            functions(find_command(argv[0], argv[1]));
        }
        free(argv);
  }
    return 0;
}


////////////////////////////////////////////////////////////////////////////////////

// PATRICK
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <ctype.h>

#include "snapshot.h"

#define DELIMITER " "
#define TRUE 1
#define FALSE 0

enum COMMAND { CMD_BYE, CMD_HELP, CMD_LIST_KEYS, CMD_LIST_ENTRIES,
               CMD_LIST_SNAPSHOTS, CMD_GET, CMD_DEL, CMD_PURGE,
               CMD_SET, CMD_PUSH, CMD_APPEND, CMD_PICK, CMD_PLUCK,
               CMD_POP, CMD_DROP, CMD_ROLLBACK, CMD_CHECKOUT, CMD_SNAPSHOT,
               CMD_MIN, CMD_MAX, CMD_SUM, CMD_LEN, CMD_REV, CMD_UNIQ, CMD_SORT, CMD_END};
const char *COMMAND_STR[] = { "BYE", "HELP", "LIST KEYS", "LIST ENTRIES", "LIST SNAPSHOTS",
                              "GET", "DEL", "PURGE", "SET", "PUSH", "APPEND", "PICK", "PLUCK",
                              "DROP", "ROLLBACK", "CHECKOUT", "SNAPSHOT", "MAX", "SUM", "LEN",
                              "REV", "UNIQ", "SORT"};

struct command
{
    enum COMMAND tag;
    union
    {
        struct
        {
            char *new_value;
        };
    };
};

entry* entry_head = NULL;
snapshot* snapshot_head = NULL;

/* Converts lowercase to uppercase string */
void my_toupper(char* str)
{
    for(int i=0; i<=strlen(str); i++)
    {
        if(str[i]>=97&&str[i]<=122)
        {
            str[i]=str[i]-32;
        }
    }
}

/* HELP COMMAND */
static void help()
{
    printf(
    "BYE   clear database and exit\n"
    "HELP  display this help message\n\n"

    "LIST KEYS       displays all keys in current state\n"
    "LIST ENTRIES    displays all entries in current state\n"
    "LIST SNAPSHOTS  displays all snapshots in the database\n\n"

    "GET <key>    displays entry values\n"
    "DEL <key>    deletes entry from current state\n"
    "PURGE <key>  deletes entry from current state and snapshots\n\n"

    "SET <key> <value ...>     sets entry values\n"
    "PUSH <key> <value ...>    pushes each value to the front one at a time\n"
    "APPEND <key> <value ...>  append each value to the back one at a time\n\n"

    "PICK <key> <index>   displays entry value at index\n"
    "PLUCK <key> <index>  displays and removes entry value at index\n"
    "POP <key>            displays and removes the front entry value\n\n"

    "DROP <id>      deletes snapshot\n"
    "ROLLBACK <id>  restores to snapshot and deletes newer snapshots\n"
    "CHECKOUT <id>  replaces current state with a copy of snapshot\n"
    "SNAPSHOT       saves the current state as a snapshot\n\n"

    "MIN <key>  displays minimum entry value\n"
    "MAX <key>  displays maximum entry value\n"
    "SUM <key>  displays sum of entry values\n"
    "LEN <key>  displays number of entry values\n\n"

    "REV <key>   reverses order of entry values\n"
    "UNIQ <key>  removes repeated adjacent entry values\n"
    "SORT <key>  sorts entry values in ascending order\n");
}

/* Finds command from token
    part of this was from todo*/
static enum COMMAND find_command(const char *str, const char *str2)
{

    const char *command = malloc(sizeof(char *) * MAX_LINE_LENGTH);
    if (!str2) // Check for NULL
    {
        command = (char *) str;
    }
    else // Interpret as two word command
    {
        sprintf((char *) command, "%s %s", str, str2);
    }

    printf("command concatenated: %s\n", command);
    puts("currently in find command");

    for (enum COMMAND i = CMD_BYE; i < CMD_END; ++i)
    {
        printf("command_str index: %d\n", i);
        if (strcmp(command, COMMAND_STR[i]) == 0)
        {
            puts("find successful");
            free((char *) command);
            return i;
        }
    }
    puts("find unsuccessful");
    free((char *) command);
    return CMD_END;
}

/* Check command for validity
    part of this was from todo*/
static int parse_command(int argc, char **argv, struct command *cmd)
{
    puts("currently in parse command");
    enum COMMAND tag;

    my_toupper(argv[0]);
    if (strcmp(argv[0], "LIST") == 0) // Check for LIST command
    {
        puts("processing a LIST command");
        my_toupper(argv[1]);
        tag = find_command(argv[0], argv[1]);
    }
    else
    {
        tag = find_command(argv[0], NULL);
    }

    cmd->tag = tag;

    switch (tag)
    {
        // List Commands
        case CMD_LIST_KEYS:
            break;

        case CMD_LIST_ENTRIES:
            break;

        case CMD_LIST_SNAPSHOTS:
            break;
        // End List Commands

        case CMD_GET:
            break;

        case CMD_DEL:
            break;

        case CMD_PURGE:
            break;

        case CMD_SET:
            break;

        case CMD_PUSH:
            break;

        case CMD_APPEND:
            break;

        case CMD_PICK:
            break;

        case CMD_PLUCK:
            break;

        case CMD_POP:
            break;

        case CMD_DROP:
            break;

        case CMD_ROLLBACK:
            break;

        case CMD_CHECKOUT:
            break;

        case CMD_SNAPSHOT:
            break;

        case CMD_MIN:
            break;

        case CMD_MAX:
            break;

        case CMD_SUM:
            break;

        case CMD_LEN:
            break;

        case CMD_REV:
            break;

        case CMD_UNIQ:
            break;

        case CMD_SORT:
            break;

        case CMD_BYE:
        case CMD_HELP:
        case CMD_END:
            *cmd = (struct command) { .tag = tag };
            break;
    }
    return TRUE;

invalid:
    return FALSE;
}

/* Execute command
    part of this was from todo*/
static void do_command(const struct command *cmd)
{
    printf("enum command tag: %d\n\n", cmd->tag);
    puts("///////// OUTPUT //////////");
    switch(cmd->tag)
    {
        // List Commands
        case CMD_LIST_KEYS:
            puts("no keys");
            break;

        case CMD_LIST_ENTRIES:
            puts("no entries");
            break;

        case CMD_LIST_SNAPSHOTS:
            puts("no snapshots");
            break;

        // End List Commands

        case CMD_GET:
            break;

        case CMD_DEL:
            break;

        case CMD_PURGE:
            break;

        case CMD_SET:
            break;

        case CMD_PUSH:
            break;

        case CMD_APPEND:
            break;

        case CMD_PICK:
            break;

        case CMD_PLUCK:
            break;

        case CMD_POP:
            break;

        case CMD_DROP:
            break;

        case CMD_ROLLBACK:
            break;

        case CMD_CHECKOUT:
            break;

        case CMD_SNAPSHOT:
            break;

        case CMD_MIN:
            break;

        case CMD_MAX:
            break;

        case CMD_SUM:
            break;

        case CMD_LEN:
            break;

        case CMD_REV:
            break;

        case CMD_UNIQ:
            break;

        case CMD_SORT:
            break;

        case CMD_HELP:
            help();
            break;

        case CMD_BYE:
            puts("bye");
            exit(0);
            break;

        case CMD_END:
            puts("no such command");
            break;
    }
}

/* Main
    tokenising from todo */
int main(void)
{
    while(TRUE)
    {
        printf("> ");

        char *line = calloc(sizeof(char *), MAX_LINE_LENGTH);
        /*if (!(read_line(&line, stdin)))
        {
            break;
        }*/

        char buffer[MAX_LINE_LENGTH];
        fgets(buffer, MAX_LINE_LENGTH, stdin);

        for (int i = 0; i < strlen(buffer); i++)
        {
            if (buffer[i] == '\n')
            {
                buffer[i] = '\0';
            }
        }

        strcat(line, buffer);

        int argc = 0;
        size_t capacity = 1;
        char **argv = malloc(sizeof(char*) * capacity);

        char *token = strtok(line, DELIMITER);

        while(token)
        {
            argv[argc++] = token;

            if (argc == capacity)
            {
                capacity *= 2;
                argv = realloc(argv, sizeof(char*) * capacity);
            }

            token = strtok(NULL, DELIMITER);
        }

        if (argc != 0)
        {
            puts("\n/////// DEBUG COMMANDS ///////");
            // Print the tokenised items for debugging
            for (int i = 0;  i < argc; i++)
            {
                printf("arg[%d] tokenised: %s\n", i, argv[i]);
            }

            struct command cmd;
            if(parse_command(argc, argv, &cmd) == TRUE)
            {
                do_command(&cmd);
            }
            else
            {
                puts("Invalid Arguments");
            }
        }

        puts("");
        free(argv);
        free(line);
    }
    return 0;
}

-----------------------------------------------------------------------------------------------
const char* HELP =
    "BYE   clear database and exit\n"
    "HELP  display this help message\n"
    "\n"
    "LIST KEYS       displays all keys in current state\n"
    "LIST ENTRIES    displays all entries in current state\n"
    "LIST SNAPSHOTS  displays all snapshots in the database\n"
    "\n"
    "GET <key>    displays entry values\n"
    "DEL <key>    deletes entry from current state\n"
    "PURGE <key>  deletes entry from current state and snapshots\n"
    "\n"
    "SET <key> <value ...>     sets entry values\n"
    "PUSH <key> <value ...>    pushes each value to the front one at a time\n"
    "APPEND <key> <value ...>  append each value to the back one at a time\n"
    "\n"
    "PICK <key> <index>   displays entry value at index\n"
    "PLUCK <key> <index>  displays and removes entry value at index\n"
    "POP <key>            displays and removes the front entry value\n"
    "\n"
    "DROP <id>      deletes snapshot\n"
    "ROLLBACK <id>  restores to snapshot and deletes newer snapshots\n"
    "CHECKOUT <id>  replaces current state with a copy of snapshot\n"
    "SNAPSHOT       saves the current state as a snapshot\n"
    "\n"
    "MIN <key>  displays minimum entry value\n"
    "MAX <key>  displays maximum entry value\n"
    "SUM <key>  displays sum of entry values\n"
    "LEN <key>  displays number of entry values\n"
    "\n"
    "REV <key>   reverses order of entry values\n"
    "UNIQ <key>  removes repeated adjacent entry values\n"
    "SORT <key>  sorts entry values in ascending order\n";
