#ifndef SNAPSHOT_H
#define SNAPSHOT_H

#define MAX_KEY_LENGTH 16
#define MAX_LINE_LENGTH 1024

typedef struct value value;
typedef struct entry entry;
typedef struct snapshot snapshot;
typedef struct node node;

struct value
{
    struct value* prev;
    struct value* next;
    int value;
};

struct entry
{
    char key[MAX_KEY_LENGTH];
    struct entry* prev;
    struct entry* next;
    struct value* values;
};

struct snapshot
{
    int id;
    struct snapshot* prev;
    struct snapshot* next;
    struct entry* entries;
};

void free_entry(struct entry **current);

// LINK LIST FUNCTIONS
// Initialise the list
void list_init(struct entry *head, struct snapshot *snapshot) {
    head->next = NULL;
    head->prev = NULL;
    head->values = NULL;
    snapshot->next = NULL;
    snapshot->prev = NULL;
}

// Checks whether the input key exists
bool keyexists(const char *key, struct entry *head)
{
    entry * current = head;
    bool found = false;
    while (current != NULL)
    {
        if (strcmp(current->key, key) == 0)
        {
            found = true;
        }
        current = current->next;
    }
    return found;
}

value* make_value(int number)
{
    value* newvalue = malloc(sizeof(value));
    newvalue->next = NULL;
    newvalue->prev = NULL;
    newvalue->value = number;
    return newvalue;
}

// Push entry into list
void pushValue(struct value **head, int number)
{
    struct value *newEntry = malloc(sizeof(value));
    newEntry->value = number;
    newEntry->prev = NULL;

    newEntry->next = *head;
    *head = newEntry;
    (*head)->next->prev = newEntry;
    // free(new);
    // return *head;
}

void appendvalue(struct value* head, int number)
{
    struct value *current = head;

    while (current->next != NULL)
    {
        current = current->next;
    }

    current->next = malloc(sizeof(value));
    (current->next)->value = number;
    (current->next)->next = NULL;
    (current->next)->prev = current;
}

// Find entry pointer and return it
entry* findEntry(const char *key, struct entry *head)
{
    entry* current = head;
    entry* found = NULL;
    while (current != NULL)
    {
        if (strcmp(current->key, key) == 0)
        {
            found = current;
            break;
        }
        current = current->next;
    }
    return found;
}

void replaceValues(const int *nums, const char *key, int argc, struct value *head)
{
    value *currentvalue = head;
    for (int i = 0; i < argc - 2; i++)
    {
        if (currentvalue != NULL)
        {
            currentvalue->value = nums[i];
        }
        else
        {
            appendvalue(head, nums[i]);
            continue;
        }
        currentvalue = currentvalue->next;
    }

}

void deleteEntry(struct entry **head, struct entry *del)
{
  // base case
  if(*head == NULL || del == NULL)
    return;

  // If node to be deleted is head node
  if(*head == del)
  {
      if (((*head)->next) != NULL)
      {
          *head = del->next;
      }
      else
      {
          struct entry *newhead = calloc(1, sizeof(entry));
          *head = newhead;
      }
  }

  // Change next only if node to be deleted is NOT the last node
  if(del->next != NULL)
  {
      del->next->prev = del->prev;
  }

  // Change prev only if node to be deleted is NOT the first node
  if(del->prev != NULL)
  {
      del->prev->next = del->next;
  }

  // Free the memory occupied by del
  free_entry(&del);
  return;
}

void deleteValue(struct value **head, struct value *del)
{
  // base case
  if(*head == NULL || del == NULL)
    return;

  // If node to be deleted is head node
  if(*head == del)
  {
      if (((*head)->next) != NULL)
      {
          *head = del->next;
      }
      else
      {
        //   struct value *newhead = calloc(1, sizeof(value));
          *head = NULL;
      }
  }

  // Change next only if node to be deleted is NOT the last node
  if(del->next != NULL)
  {
      del->next->prev = del->prev;
  }

  // Change prev only if node to be deleted is NOT the first node
  if(del->prev != NULL)
  {
      del->prev->next = del->next;
  }

  // Free the memory occupied by del
  free(del);
  return;
}

// Make a values list and link to the entry
static void make_vlist(const int *nums, int argc, struct entry *entry)
{
    int truecount = argc - 2;
    value *head = NULL;
    head = malloc(sizeof(value));
    (head)->value = nums[0];
    (head)->next = NULL;
    (head)->prev = NULL;
    if (truecount > 1)
    {
        value *current = head;
        for (int i = 1; i < truecount; i++)
        {
            current->next = malloc(sizeof(value));
            current->next->value = nums[i];
            (current->next)->next = NULL;
            (current->next)->prev = current;
            current = current->next;
        }
        current->next = NULL;
    }
    (entry)->values = head;
    // free(head);
}

// Inserts entry at the end of the list
void add_to_tail(const char *key, struct entry *head)
{
    if (items_count == 0)
    {
        strcpy(head->key, key);
        return;
    }

    struct entry *current = head;
    while (current->next != NULL)
    {
        current = current->next;
    }
    current->next = malloc(sizeof(entry*));
    strcpy(current->next->key, key);
    (current->next)->next = NULL;
    (current->next)->prev = current;
    // free(current->next);
}

// Push entry into list
entry* pushEntry(const char *key, struct entry **head)
{
    struct entry *newEntry = malloc(sizeof(entry));
    strcpy(newEntry->key, key);

    if (items_count == 0)
    {
        strcpy((*head)->key, newEntry->key);
        free(newEntry);
        return *head;
    }
    newEntry->next = *head;
    *head = newEntry;
    (*head)->next->prev = newEntry;
    // free(new);
    return *head;
}

// Checks whether entries list is empty
int list_entryEmpty(struct entry *head)
{
    if (items_count == 0)
    {
        return true;
    }
    return false;
}

// Checks whether snapshots list is empty
int list_snapshotEmpty(struct snapshot *head)
{
    if (snapshotCount == 0)
    {
        return true;
    }
    return false;
}

// Print list of keys
void print_listkeys(struct entry *current)
{
    while(current != NULL)
    {
        printf("%s\n", current->key);
        current = current->next;
    }
}

void pluck(struct value *current, struct value **head, const char *key, int pluckvalue)
{
    if (pluckvalue < 1)
    {
        puts("index out of range");
        return;
    }
    value *currentvalue = current;
    for (int i = 0; i < pluckvalue; i++)
    {
        if (currentvalue != NULL)
        {
            if (i == (pluckvalue-1))
            {
                printf("%d\n", currentvalue->value);
                deleteValue(head, currentvalue);
                return;
            }
        }
        else
        {
            puts("index out of range");
            return;
        }
        currentvalue = currentvalue->next;
    }
}

void pick(struct value *current, const char *key, int pickvalue)
{
    if (pickvalue < 1)
    {
        puts("index out of range");
        return;
    }
    value *currentvalue = current;
    for (int i = 0; i < pickvalue; i++)
    {
        if (currentvalue != NULL)
        {
            if (i == (pickvalue-1))
            {
                printf("%d\n", currentvalue->value);
            }
        }
        else
        {
            puts("index out of range");
            return;
        }
        currentvalue = currentvalue->next;
    }
}

void pop(struct value *current, struct value **head, const char *key)
{
    value *currentvalue = current;

    if (currentvalue != NULL)
    {
        printf("%d\n", currentvalue->value);
        deleteValue(head, currentvalue);
        return;
    }
    else
    {
        puts("nil");
        return;
    }
}

// Get min value
int min(struct value* head)
{
    struct value *current = head;
    int min = current->value;

    while (current->next != NULL)
    {
        if ( (current->value) < min )
        {
            min = current->value;
        }
        current = current->next;
    }

    if ( (current->value) < min )
    {
        min = current->value;
    }
    return min;
}

// Get max value
int max(struct value* head)
{
    struct value *current = head;
    int max = current->value;

    while (current->next != NULL)
    {
        if ( (current->value) > max )
        {
            max = current->value;
        }
        current = current->next;
    }

    if ( (current->value) > max )
    {
        max = current->value;
    }
    return max;
}

// Get sum of values
int sum(struct value* head)
{
    struct value *current = head;
    int sum = 0;

    while (current->next != NULL)
    {
        sum += current->value;
        current = current->next;
    }

    sum += current->value;
    return sum;
}

// Get length of values
int len(struct value* head)
{
    struct value *current = head;
    int len = 0;

    while (current->next != NULL)
    {
        len += 1;
        current = current->next;
    }

    len += 1;
    return len;
}

// Get the values of a specified key
void get(struct entry *current, const char *key)
{
    entry* currententry = current;
    while(currententry != NULL)
    {
        if(!strcmp(currententry->key, key))
        {
            // value* currentvalues = current->values;
            value* currentvalues = currententry->values;
            printf("[");
            while(currentvalues != NULL)
            {
                printf("%d", currentvalues->value);
                currentvalues = currentvalues->next;
                if(currentvalues)
                {
                    printf(" ");
                }
            }
            printf("]");
            puts("");
        }
        currententry = currententry->next;
    }
}

// Print List of entries
void print_listEntries(struct entry *current)
{
    entry* currententry = current;
    while(currententry != NULL)
    {
        printf("%s ", currententry->key);
        value* currentvalues = currententry->values;
        printf("[");
        while(currentvalues != NULL)
        {
            printf("%d", currentvalues->value);
            currentvalues = currentvalues->next;
            if(currentvalues)
            {
                printf(" ");
            }
        }
        printf("]");
        puts("");
        currententry = currententry->next;
    }
}


void free_entry(struct entry **current)
{
    entry *temp_entry;
    value *temp_value;
    // entry *tempEnthead = *current;

    while((*current)->values)
    {
        temp_value = (*current)->values;
        (*current)->values = (*current)->values->next;
        free(temp_value);
    }
    // puts("out of value loop");
    temp_entry = *current;
    free(temp_entry);
}

void free_values(struct entry *current)
{
    value *temp_value;
    // entry *tempEnthead = *current;

    while((current)->values)
    {
        temp_value = (current)->values;
        (current)->values = (current)->values->next;
        free(temp_value);
    }
    // puts("out of value loop");
}

// Free memory allocations
void free_list(struct entry **current)
{
    entry *temp_entry;
    value *temp_value;
    // entry *tempEnthead = *current;
    while((*current))
    {
        while((*current)->values)
        {
            temp_value = (*current)->values;
            (*current)->values = (*current)->values->next;
            free(temp_value);
        }
        // puts("out of value loop");
        temp_entry = *current;
        *current = (*current)->next;
        free(temp_entry);
    }
    // current = NULL;
    // free(tempEnthead);
}
// END LINKED LIST FUNCTIONS

const char* HELP =
    "BYE   clear database and exit\n"
    "HELP  display this help message\n"
    "\n"
    "LIST KEYS       displays all keys in current state\n"
    "LIST ENTRIES    displays all entries in current state\n"
    "LIST SNAPSHOTS  displays all snapshots in the database\n"
    "\n"
    "GET <key>    displays entry values\n"
    "DEL <key>    deletes entry from current state\n"
    "PURGE <key>  deletes entry from current state and snapshots\n"
    "\n"
    "SET <key> <value ...>     sets entry values\n"
    "PUSH <key> <value ...>    pushes each value to the front one at a time\n"
    "APPEND <key> <value ...>  append each value to the back one at a time\n"
    "\n"
    "PICK <key> <index>   displays entry value at index\n"
    "PLUCK <key> <index>  displays and removes entry value at index\n"
    "POP <key>            displays and removes the front entry value\n"
    "\n"
    "DROP <id>      deletes snapshot\n"
    "ROLLBACK <id>  restores to snapshot and deletes newer snapshots\n"
    "CHECKOUT <id>  replaces current state with a copy of snapshot\n"
    "SNAPSHOT       saves the current state as a snapshot\n"
    "\n"
    "MIN <key>  displays minimum entry value\n"
    "MAX <key>  displays maximum entry value\n"
    "SUM <key>  displays sum of entry values\n"
    "LEN <key>  displays number of entry values\n"
    "\n"
    "REV <key>   reverses order of entry values\n"
    "UNIQ <key>  removes repeated adjacent entry values\n"
    "SORT <key>  sorts entry values in ascending order\n";

#endif
