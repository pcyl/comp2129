#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

#define DELIMITER " "

enum COMMAND { CMD_BYE, CMD_HELP, CMD_LIST_KEYS, CMD_LIST_ENTRIES,
               CMD_LIST_SNAPSHOTS, CMD_GET, CMD_DEL, CMD_PURGE,
               CMD_SET, CMD_PUSH, CMD_APPEND, CMD_PICK, CMD_PLUCK,
               CMD_POP, CMD_DROP, CMD_ROLLBACK, CMD_CHECKOUT, CMD_SNAPSHOT,
               CMD_MIN, CMD_MAX, CMD_SUM, CMD_LEN, CMD_REV, CMD_UNIQ, CMD_SORT, CMD_END};
const char *COMMAND_STR[] = { "BYE", "HELP", "LIST KEYS", "LIST ENTRIES", "LIST SNAPSHOTS",
                              "GET", "DEL", "PURGE", "SET", "PUSH", "APPEND", "PICK", "PLUCK",
                              "POP","DROP", "ROLLBACK", "CHECKOUT", "SNAPSHOT", "MIN", "MAX", "SUM", "LEN",
                              "REV", "UNIQ", "SORT"};

static int items_count = 0;
// Snapshot id counter
int snapshotCount = 0;

#include "snapshot.h"

struct command
{
    enum COMMAND tag;
    char key[MAX_KEY_LENGTH];
    int nums[MAX_LINE_LENGTH];
};

/* Initialise lists */
static struct entry* entries = NULL;
static struct snapshot* snapshots = NULL;

/* Converts lowercase to uppercase string */
void my_toupper(char* str)
{
    for(int i=0; i<=strlen(str); i++)
    {
        if(str[i]>=97&&str[i]<=122)
        {
            str[i]=str[i]-32;
        }
    }
}

void *check(void *ptr)
{
    if (!ptr)
    {
        abort();
    }
    return ptr;
}

/* Finds command from token
    part of this was from todo*/
static enum COMMAND find_command(const char *str, const char *str2)
{
    bool successful = false;
    enum COMMAND result = CMD_END;
    char *command = check(malloc(sizeof(char *) * MAX_LINE_LENGTH));

    if (!str2) // Check for NULL
    {
        strcpy(command, (char *) str);
    }
    else // Interpret as two word command
    {
        strcpy(command, (char *) str);
        strcat(command, " ");
        strcat(command, (char *) str2);
    }

    for (enum COMMAND i = CMD_BYE; i < CMD_END - 1; ++i)
    {
        // printf("command_str index: %d\n", i);
        if (strcmp(command, COMMAND_STR[i]) == 0)
        {
             result = i;
             successful = true;
             break;
        }
    }

    free(command);
    if (successful)
    {
        return result;
    }
    else
    {
        return CMD_END;
    }
}

void print_list(entry * head) {
    entry * current = head;

    while (current != NULL)
    {
        printf("%s\n", current->key);
        current = current->next;
    }
}

/* Check command for validity
    part of this was from todo*/
static int parse_command(int argc, char **argv, struct command *cmd)
{
    //puts("currently in parse command");
    enum COMMAND tag;

    my_toupper(argv[0]);
    if (strcmp(argv[0], "LIST") == 0) // Check for LIST command
    {
        //puts("processing a LIST command");
        my_toupper(argv[1]);
        tag = find_command(argv[0], argv[1]);
    }
    else
    {
        tag = find_command(argv[0], NULL);
    }

    cmd->tag = tag;

    switch (tag)
    {
        case CMD_GET:
            {
                if (argc != 2)
                {
                    goto invalid;
                }
                char key[MAX_KEY_LENGTH];
                strcpy(key, argv[1]);

                *cmd = (struct command)
                { // Set command tag and key
                    .tag = tag,
                    // .key = key,
                };
                for (int i = 2; i < argc; i++)
                {
                    cmd->nums[i-2] = atoi(argv[i]); // Set arguments for set
                }
                strcpy(cmd->key, key); // Since char
            }
            break;

        case CMD_REV:
        case CMD_UNIQ:
        case CMD_SORT:
        case CMD_LEN:
        case CMD_SUM:
        case CMD_MAX:
        case CMD_MIN:
        case CMD_POP:
        case CMD_DEL:
            if (argc != 2)
            {
                goto invalid;
            }
            char key[MAX_KEY_LENGTH];
            strcpy(key, argv[1]);

            *cmd = (struct command)
            { // Set command tag and key
                .tag = tag,
                // .key = key,
            };
            for (int i = 2; i < argc; i++)
            {
                cmd->nums[i-2] = atoi(argv[i]); // Set arguments for set
            }
            strcpy(cmd->key, key); // Since char
            break;

        case CMD_PURGE:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_APPEND:
        case CMD_PUSH:
        case CMD_SET:
            {
                if (argc < 3)
                {
                    goto invalid;
                }
                char key[MAX_KEY_LENGTH];
                strcpy(key, argv[1]);

                *cmd = (struct command) { // Set command tag and key
                    .tag = tag,
                    // .key = key,
                };

                char *endptr; // For strtol
                for (int i = 2; i < argc; i++)
                {
                    cmd->nums[i-2] = strtol(argv[i], &endptr, 10); // Set arguments for set
                }
                strcpy(cmd->key, key); // Since char
            }
            break;

        case CMD_PLUCK:
        case CMD_PICK:
            {
                if (argc != 3)
                {
                    goto invalid;
                }
                char key[MAX_KEY_LENGTH];
                strcpy(key, argv[1]);

                *cmd = (struct command) { // Set command tag and key
                    .tag = tag,
                    // .key = key,
                };

                char *endptr; // For strtol
                for (int i = 2; i < argc; i++)
                {
                    cmd->nums[i-2] = strtol(argv[i], &endptr, 10); // Set arguments for set
                }
                strcpy(cmd->key, key); // Since char
            }

            break;

        case CMD_DROP:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_ROLLBACK:
            if (argc != 2)
            {
                goto invalid;
            }
            break;

        case CMD_CHECKOUT:
            if (argc != 2)
            {
                goto invalid;
            }
            break;


        case CMD_LIST_KEYS:
        case CMD_LIST_ENTRIES:
        case CMD_LIST_SNAPSHOTS:
        case CMD_SNAPSHOT:
        case CMD_BYE:
        case CMD_HELP:
        case CMD_END:
            *cmd = (struct command) { .tag = tag };
            break;
    }
    return true;

invalid:
    return false;
}

/* Execute command
    part of this was from todo*/
static int do_command(const struct command *cmd, int argc)
{
    switch(cmd->tag)
    {
        // List Commands
        case CMD_LIST_KEYS:
            if (list_entryEmpty(entries) == true)
            {
                puts("no keys");
            }
            else
            {
                print_listkeys(entries);
            }
            break;

        case CMD_LIST_ENTRIES:
            if (list_entryEmpty(entries))
            {
                puts("no entries");
            }
            else
            {
                print_listEntries(entries);
            }
            break;

        case CMD_LIST_SNAPSHOTS:
            if (list_snapshotEmpty(snapshots))
            {
                puts("no snapshots");
            }
            else
            {
                puts("snapshots not empty");
            }
            break;
        // End List Commands

        case CMD_GET:
            {
                bool check = keyexists(cmd->key, entries);
                if (list_entryEmpty(entries)) // Check for empty list
                {
                    puts("no such key");
                    break;
                }
                if(check) // Check whether the searched key exists
                {
                    get(entries,cmd->key);
                    break;
                }
                if(!check)
                {
                    puts("no such key");
                }
            }
            break;

        case CMD_DEL:
            {
                bool check = keyexists(cmd->key, entries);
                if (!check)
                {
                    puts("no such key");
                }
                else
                {
                    deleteEntry(&entries, findEntry(cmd->key, entries));
                    puts("ok");
                    --items_count;
                }

            }
            break;

        case CMD_PURGE:
            break;

        case CMD_SET:
            {
                // pushEntry(cmd->key, &entries);
                // Create a new entry with the attached values
                if (!keyexists(cmd->key, entries))
                {
                    make_vlist(cmd->nums, argc, pushEntry(cmd->key, &entries));
                    ++items_count;
                }
                else
                {
                    free_values((findEntry(cmd->key, entries)));
                    // replaceValues(cmd->nums, cmd->key, argc, (findEntry(cmd->key, entries))->values);
                    make_vlist(cmd->nums, argc, findEntry(cmd->key, entries));
                }
                // print_list(entries);
                puts("ok");
            }
            break;

        case CMD_PUSH:
            {
                if (!keyexists(cmd->key, entries))
                {
                    puts("no such key");
                }
                else
                {
                    for (int i = 0; i < argc-2; i++)
                    {
                        pushValue(&((findEntry(cmd->key, entries))->values), cmd->nums[i]);
                    }
                    puts("ok");
                }
            }
            break;

        case CMD_APPEND:
            {
                // add_to_tail(cmd->key, entries);
                // pushEntry(cmd->key, &entries);
                if (!keyexists(cmd->key, entries))
                {
                    puts("no such key");
                }
                else
                {
                    for (int i = 0; i < argc-2; i++)
                    {
                        appendvalue((findEntry(cmd->key, entries))->values, cmd->nums[i]);
                    }
                    puts("ok");
                }
                // print_list(entries);
            }
            break;

        case CMD_PICK:
            {
                if (!keyexists(cmd->key, entries))
                {
                    puts("no such key");
                }
                else
                {
                    pick((findEntry(cmd->key, entries))->values, cmd->key, cmd->nums[0]);
                }
                // print_list(entries);
            }
            break;

        case CMD_PLUCK:
            {
                if (!keyexists(cmd->key, entries))
                {
                    puts("no such key");
                }
                else
                {
                    pluck((findEntry(cmd->key, entries))->values, &((findEntry(cmd->key, entries))->values), cmd->key, cmd->nums[0]);
                }
            }
            break;

        case CMD_POP:
            {
                if (!keyexists(cmd->key, entries))
                {
                    puts("no such key");
                }
                else
                {
                    pop((findEntry(cmd->key, entries))->values, &((findEntry(cmd->key, entries))->values), cmd->key);
                }
            }
            break;

        case CMD_DROP:
            break;

        case CMD_ROLLBACK:
            break;

        case CMD_CHECKOUT:
            break;

        case CMD_SNAPSHOT:
            puts("reached snapshot");
            break;

        case CMD_MIN:
            {
                if (!keyexists(cmd->key, entries))
                {
                    puts("no such key");
                }
                else
                {
                    printf("%d\n",min((findEntry(cmd->key, entries))->values);
                }
            }
            break;

        case CMD_MAX:
            {
                if (!keyexists(cmd->key, entries))
                {
                    puts("no such key");
                }
                else
                {
                    printf("%d\n",max((findEntry(cmd->key, entries))->values));
                }
            }
            break;

        case CMD_SUM:
            {
                if (!keyexists(cmd->key, entries))
                {
                    puts("no such key");
                }
                else
                {
                    printf("%d\n",sum((findEntry(cmd->key, entries))->values));
                }
            }
            break;

        case CMD_LEN:
            {
                if (!keyexists(cmd->key, entries))
                {
                    puts("no such key");
                }
                else
                {
                    printf("%d\n",len((findEntry(cmd->key, entries))->values));
                }
            }
            break;

        case CMD_REV:
            {
                if (!keyexists(cmd->key, entries))
                {
                    puts("no such key");
                }
                else
                {
                    int len = len((findEntry(cmd->key, entries))->values);
                    
                }
            }
            break;

        case CMD_UNIQ:
            break;

        case CMD_SORT:
            break;

        case CMD_HELP:
            printf("%s", HELP);
            break;

        case CMD_BYE: // Exit program
            puts("bye");
            return true;
            break;

        case CMD_END: // If input command doesn't exist
            puts("no such command");
            break;
    }
    return false;
}

/* Main includes list initialisation
    tokenising concept from todo */
int main(void)
{
    entries = check(malloc(sizeof(entry)));
    snapshots = check(malloc(sizeof(snapshot)));
    list_init(entries, snapshots);

    while(true)
    {
        int terminate = 0;
        printf("> ");
        char line[MAX_LINE_LENGTH];

        fgets(line, MAX_LINE_LENGTH, stdin);

        for (int i = 0; i < strlen(line); i++)
        {
            if (line[i] == '\n')
            {
                line[i] = '\0';
            }
        }

        int argc = 0;
        char *token = strtok(line, DELIMITER);
        // size_t capacity = 1;
        char **argv = check(calloc(1, sizeof(char*) * MAX_LINE_LENGTH));

        while(token)
        {
            argv[argc++] = token;
            token = strtok(NULL, DELIMITER);
        }

        if (argc != 0)
        {
            struct command cmd;
            if(parse_command(argc, argv, &cmd))
            {
                terminate = do_command(&cmd, argc);
            }
            else
            {
                puts("invalid input");
            }
        }

        free(argv);
        puts("");
        // Check to exit program
        if (terminate)
        {
            break;
        }
    }
    // Free all memory allocations
    free_list(&entries);
    free(snapshots);
    return 0;
}
