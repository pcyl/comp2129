/**
 * The way undo works is that we create a list of inverse operations
 * CMD_MOVE is its own inverse
 * CMD_NEW is inverted by CMD_DELETE
 * CMD_DELETE is inverted by CMD_NEW followed by CMD_MOVE, we name this: CMD_INSERT
 *
 * Alternative you can just copy the state of the list every time it changes.
 * However, that is less efficient but a lot easier to do.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

#define BUFFER_SIZE 20
#define SEPARATOR " "
#define FILE_NAME "todo.txt"

/** Declare the commands avaliable */
enum COMMAND { CMD_HELP, CMD_NEW, CMD_MOVE, CMD_DELETE, CMD_UNDO, CMD_END, CMD_INSERT};
const char *COMMAND_STR[] = { "help", "new", "move", "delete", "undo" };

/** A tagged union that represents a command */
struct command {
    enum COMMAND tag;
    union {
        // CMD_NEW
        struct {
            char *new_value;
        };

        // CMD_DELETE
        struct {
            size_t delete_at;
        };

        // CMD_INSERT (a NEW + DELETE)
        struct {
            char *insert_value;
            size_t insert_before;
        };

        // CMD_MOVE
        struct {
            size_t move_from, move_to;
        };
    };
};

/** A value that can be stored in the linked list */
union value {
    char *list_item;
    struct command cmd;
};

// list_head needs the declaration of value, so we can't include it at the top
#include "list.h"

/** List used to undo */
static struct list_head undos;

/** List used to store the list items */
static struct list_head items;
// keep a count the of the number of items so we don't need to count every single time
static int items_count = 0;

/** Checks the return value from *alloc */
static void *check(void *ptr) {
    if (!ptr) {
        perror("*alloc failed");
        abort(); // no point cleaning up, just let the program die
    }
    return ptr;
}

/** Finds the command matching the string */
static enum COMMAND find_command(const char *str) {
    for (enum COMMAND i = CMD_HELP; i < CMD_END; ++i) {
        if (strcmp(str, COMMAND_STR[i]) == 0) {
            return i;
        }
    }
    return CMD_END;
}

/** Prints the items in the todo list */
static void print_items(void) {
    size_t i = 0;
    struct list_head *ptr;
    list_for_each(ptr, &items) {
        printf("%zu. %s\n", ++i, ptr->value.list_item);
    }
}

/**
 * Converts a command to a command struct
 * We need to do this because we need to save what commands were executed
 * This just puts the command into a machine readable form
 */
static bool parse_command(int argc, char **argv, struct command *cmd) {
    enum COMMAND tag = find_command(argv[0]);
    cmd->tag = tag;

    switch (tag) {
        case CMD_NEW:
            if (argc < 2) {
                goto invalid;
            }

            // join the arguments back together
            for (char *ptr = argv[1]; ptr != argv[argc - 1]; ++ptr) {
                if (*ptr == '\0') {
                    *ptr = ' ';
                }
            }

            // copy the string since it'll be freed by the caller
            char *value = check(malloc(strlen(argv[1]) + 1));
            strcpy(value, argv[1]);

            *cmd = (struct command) {
                .tag = tag,
                .new_value = value
            };

            break;

        case CMD_DELETE:
            {
                if (argc < 2) {
                    goto invalid;
                }

                // validate input
                int at = atoi(argv[1]) - 1;
                if (at < 0 || at >= items_count) {
                    goto invalid;
                }

                *cmd = (struct command) {
                    .tag = tag,
                    .delete_at = at
                };
            }

            break;

        case CMD_MOVE:
            {
                if (argc < 3) {
                    goto invalid;
                }

                // validate input
                int from = atoi(argv[1]) - 1, to = atoi(argv[2]) - 1;
                if (from < 0 || from >= items_count || to < 0 || to >= items_count) {
                    goto invalid;
                }

                *cmd = (struct command) {
                    .tag = tag,
                    .move_from = from,
                    .move_to = to
                };
            }

            break;

        case CMD_INSERT:
            // cannot happen
            break;

        case CMD_HELP:
        case CMD_UNDO:
        case CMD_END:
            *cmd = (struct command) { .tag = tag };
            break;
    }

    return true;

invalid:
    return false;
}

/** Runs the command, the undo parameters tell us if we need to record the undo history for the comamnd */
static void do_command(const struct command *cmd, bool undo) {
    switch (cmd->tag) {
        case CMD_HELP:
            puts("help, new, delete, move, undo");
            break;

        case CMD_NEW:
            { // need to create a scope so we can declare variables
                struct list_head *item = check(malloc(sizeof(struct list_head)));
                item->value.list_item = cmd->new_value;
                list_add_tail(item, &items);
                ++items_count;

                // create the inverse command on the undo list
                if (undo) {
                    struct list_head *inv = check(malloc(sizeof(struct list_head)));
                    inv->value.cmd = (struct command) {
                        .tag = CMD_DELETE,
                        .delete_at = items_count - 1
                    };
                    list_add(inv, &undos);
                }
            }
            break;

        case CMD_INSERT:
            { // a hidden command so we can undo delete
                struct list_head *item = check(malloc(sizeof(struct list_head)));
                item->value.list_item = cmd->new_value;
                list_add(item, list_at(cmd->insert_before, &items)->prev);
                ++items_count;
            }
            break;

        case CMD_DELETE:
            {
                struct list_head *item = list_at(cmd->delete_at, &items);
                char *value = item->value.list_item;
                list_del(item);
                free(item);
                --items_count;

                if (undo) {
                    // create the inverse command
                    struct list_head *inv = check(malloc(sizeof(struct list_head)));
                    inv->value.cmd = (struct command) {
                        .tag = CMD_INSERT,
                        .insert_value = value,
                        .insert_before = cmd->delete_at
                    };
                    list_add(inv, &undos);
                } else {
                    // we are done with this string
                    free(value);
                }
            }
            break;

        case CMD_MOVE:
            {
                // check if we are moving to and from the same index
                if (cmd->move_from == cmd->move_to) {
                    break;
                }

                // we remove the item first
                // we need to do this because this allows us to insert it into the correct position
                struct list_head *from_item = list_at(cmd->move_from, &items);
                list_del(from_item);

                struct list_head *to_item = list_at(cmd->move_to, &items);
                list_add(from_item, to_item->prev);

                if (undo) {
                    // create the inverse command on the undo list
                    struct list_head *inv = check(malloc(sizeof(struct list_head)));
                    inv->value.cmd = (struct command) {
                        .tag = CMD_MOVE,
                        .move_from = cmd->move_to,
                        .move_to = cmd->move_from
                    };
                    list_add(inv, &undos);
                }
            }
            break;

        case CMD_UNDO:
            {
                if (list_empty(&undos)) {
                    puts("Nothing to undo");
                    break;
                }

                // apply the undo
                struct list_head *undo_item = undos.next;
                list_del(undo_item);
                do_command(&undo_item->value.cmd, false);
                free(undo_item);
            }
            break;

        case CMD_END:
            puts("Unknown command");
            break;
    }

    // print the items only if the command changes the list
    if (cmd->tag != CMD_END && cmd->tag != CMD_HELP && cmd->tag != CMD_UNDO) {
        print_items();
    }
}

/** Reads a line of arbitrary length from a file pointer, returns false if error or EOF */
static bool read_line(char **line, FILE *fp) {
    bool ret = true;
    char buffer[BUFFER_SIZE];
    size_t count = 0;
    size_t capacity = BUFFER_SIZE;

    // use a dynamic array to store the line
    *line = check(calloc(1, BUFFER_SIZE));

    for (size_t i = 0; ; ++i) {
        if (fgets(buffer, BUFFER_SIZE, fp) != buffer) {
            // encountered an error or EOF
            if (i == 0) {
                free(*line);
                return false;
            } else {
                // we have a partial line already read
                return true;
            }
        }

        // double the buffer size if we can't fit this line in
        size_t len = strlen(buffer);
        if (len + count >= capacity) {
            capacity *= 2;
            *line = check(realloc(*line, capacity));
        }

        count += len;
        strcat(*line, buffer);

        // check if we got the entire line
        if (len < BUFFER_SIZE - 1 || buffer[BUFFER_SIZE - 2] == '\n') {
            // remove the newline from the output
            (*line)[count - 1] = '\0';
            break;
        }
    }

    return ret;
}

/** Frees all the strings stored in the item list */
static void free_items(void) {
    struct list_head *ptr;
    list_for_each(ptr, &items) {
        free(ptr->value.list_item);
    }
}

/** Frees all strings stored in the undo list */
static void free_undos(void) {
    struct list_head *ptr;
    list_for_each(ptr, &undos) {
        if (ptr->value.cmd.tag == CMD_NEW) {
            free(ptr->value.cmd.new_value);
        } else if (ptr->value.cmd.tag == CMD_INSERT) {
            free(ptr->value.cmd.insert_value);
        }
    }
}

/** Removes and frees the items of a linked list */
static void free_list(struct list_head *list) {
    for (struct list_head *elem = list->prev; elem != list; ) {
        struct list_head *prev = elem->prev;

        list_del(elem);
        free(elem);

        elem = prev;
    }
}

/** Read from todo.txt */
static void load_file(void) {
    FILE *file = fopen(FILE_NAME, "r");

    if (!file) {
        return;
    }

    char *line;
    while (read_line(&line, file)) {
        struct list_head *item = check(malloc(sizeof(struct list_head)));
        item->value.list_item = line;
        list_add_tail(item, &items);
        ++items_count;
    }

    fclose(file);
}

/** Write to the text file */
static void save_file(void) {
    FILE *file = fopen(FILE_NAME, "w");
    if (!file) {
        perror("Unable to save file");
        return;
    }

    struct list_head *ptr;
    list_for_each(ptr, &items) {
        fprintf(file, "%s\n", ptr->value.list_item);
    }
    fclose(file);
}

int main(void) {
    // initalise global variables
    list_init(&undos);
    list_init(&items);

    load_file();

    print_items();
    puts("");

    while (true) {
        // print out the state of the list
        printf("> ");

        char *line;
        if (!read_line(&line, stdin)) {
            break; // encountered EOF
        }

        // count the number of arguments in argc
        // save the the arguments in argv
        int argc = 0;
        size_t capacity = 1;
        char **argv = check(malloc(sizeof(char *) * capacity));

        // tokenise the commands
        char *token = strtok(line, " ");
        while (token) {
            argv[argc++] = token;

            // increase the capacity of the array if it's full
            if (argc == capacity) {
                capacity *= 2;
                argv = check(realloc(argv, sizeof(char *) * capacity));
            }

            token = strtok(NULL, " ");
        }

        // ignore empty lines
        if (argc != 0) {
            for (int i = 0; i < argc; i++)
            {
                printf("%s\n", argv[i]);
            }

            struct command cmd;
            if (parse_command(argc, argv, &cmd)) {
                do_command(&cmd, true);
            } else {
                puts("Invalid arguments");
            }

        }
        puts("");

        free(argv);
        free(line);
    }

    save_file();

    // clean up
    free_undos();
    free_list(&undos);

    free_items();
    free_list(&items);

    puts("");

    return 0;
}
