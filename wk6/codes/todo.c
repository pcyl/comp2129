#include <stdlib.h>
#include <stdio.h>

#include "list.h"

int main(void) {
  FILE *list;
  char line[256];
  struct list_head head;
  list = fopen("todo.txt", "r+");

  list_init(&head); // Initialise the head of the list

  while(fgets(line, sizeof(line), list)) {
    printf("%s", line);
  }

  fclose(list);
  return 0;
}
