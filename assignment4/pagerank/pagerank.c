#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>
#include <immintrin.h>

#include "pagerank.h"

#define ESQUARE (5E-3 * 5E-3)
pthread_mutex_t lockP = PTHREAD_MUTEX_INITIALIZER;

struct argument{
	int __attribute__((aligned(64))) tid;
	int __attribute__((aligned(64))) npages;
	int __attribute__((aligned(64))) nthreads;
	int __attribute__((aligned(64))) dampener;
	double* __attribute__((aligned(64))) convergence;
	double* __attribute__((aligned(64))) scores_t;
	double* __attribute__((aligned(64))) scores_t1;	
	struct node* list;
};

void* Reinit(void* args)
{

	struct argument arg = *(struct argument *)args;
	int npages = arg.npages;
	int nthreads = arg.nthreads;
	int tid = arg.tid;
	int thread_dimension = npages/nthreads;
	// double scores_t[npages];
	// double scores_t1[npages];

    const size_t start = tid * thread_dimension;
    const size_t end =
        tid == nthreads - 1 ? npages : (tid + 1) * thread_dimension;

	for (int y = start; y < end; ++y)
	{
		arg.scores_t[y] = arg.scores_t1[y];
	}

	return NULL;
}

/***** Main parallel algorithm *****/

// void* Pagerank_Parallel(void* args)
// {

// 	struct argument arg = *(struct argument *)args;

// 	int thread_dimension = arg.npages/arg.nthreads;

//     const size_t start = arg.tid * thread_dimension;
//     const size_t end =
//         arg.tid == arg.nthreads - 1 ? arg.npages : (arg.tid + 1) * thread_dimension;

// 	// Every thread will compute a local sum and add it
// 	// to the global one
// 	// double temp_sum = 0.0;
// 	struct node* current_node = &arg.list[start];

// 	for (int i = start; i < end; ++i)
// 	{
// 		// if (Nodes[i].con_size == 0)
// 		// {
// 		// 	 temp_sum = temp_sum + (double) Nodes[i].p_t0 / N;
// 		// }

//   //       // Compute the total probability, contributed by node's neighbors
// 		// for (j = 0; j < Nodes[i].from_size; j++)
// 		// {
// 		// 	index = Nodes[i].From_id[j];	
// 		// 	Nodes[i].p_t1 = Nodes[i].p_t1 + (double) Nodes[index].p_t0 / Nodes[index].con_size;
// 		// }

// 		struct node* inLinks = current_node->page->inlinks;
// 		int sum = 0.0;

// 		while(inLinks != NULL) {
// 			sum += arg.scores_t[inLinks->page->index]/(inLinks->page->noutlinks);
// 			inLinks = inLinks->next;
// 		}

// 		arg.scores_t1[i] = ((1- arg.dampener)/arg.npages) + arg.dampener * sum;
// 		current_node = current_node->next;	
// 	}
	
// 	// This is an atomic operation
// 	// pthread_mutex_lock(&locksum);
// 	// sum = sum + temp_sum; 
// 	// pthread_mutex_unlock(&locksum);
// 	return NULL;
// }

void* CalCon(void* args)
{

	struct argument arg = *(struct argument *)args;
	int npages = arg.npages;
	int nthreads = arg.nthreads;
	int thread_dimension = npages/nthreads;
	double convergence = 0.0;
	int tid = arg.tid;
	// arg.convergence[arg.tid] = 0.0;
    const size_t start = tid * thread_dimension;
    const size_t end =
        tid == nthreads - 1 ? npages : (tid + 1) * thread_dimension;

	for (int y = start; y < end; ++y)
	{
		convergence += (arg.scores_t[y] - arg.scores_t1[y]) * (arg.scores_t[y] - arg.scores_t1[y]);
	}

	arg.convergence[tid] = convergence;
	// pthread_mutex_lock(&lockP);
	// printf("Thread %d\n", arg.tid);
	// // for (int i = 0; i < (end - start); ++i) {
	// 	printf("%.4lf\n", arg.convergence[arg.tid]);
	// // }
	// pthread_mutex_unlock(&lockP);
	return NULL;
}

void pagerank(node* list, int npages, int nedges, int nthreads, double dampener) {

	/*
		TODO
		- implement this function
		- implement any other necessary functions
		- implement any other useful data structures
	*/

	// Initialise Variables
	double scores_t[npages];
	double scores_t1[npages];
	double convergence = 1.0;
	double* temp_con = calloc(1,sizeof(double)*nthreads);
	struct node* current_node;
	// Initialise PageRank score
	for (size_t i = 0; i < npages; i++) {
		scores_t[i] = 1/ (float)npages;
	}

	struct argument *args = malloc(sizeof(struct argument) * nthreads);

	for (size_t i = 0; i < nthreads; ++i) {
		args[i] = (struct argument) {
			.tid = i,
			.npages = npages,
			.nthreads = nthreads,
			.dampener = dampener,
			.convergence = temp_con,
			.scores_t = &scores_t[0],
			.scores_t1 = &scores_t1[0]
			// .list = list
		};
	}

	pthread_t thread_ids[nthreads];

	while (convergence > (ESQUARE)) {
		convergence = 0.0;

		/*
		 * Main
		 */
    	// launch
		// for (size_t i = 0; i < nthreads; ++i) {
		// 	if (pthread_create(thread_ids + i, NULL, Pagerank_Parallel, args + i) != 0) {
		// 		perror("pthread_create failed");
		// 		exit(1);
		// 	}
		// }

  //   	// wait for the all the threads to finish
		// for (size_t i = 0; i < nthreads; ++i) {
		// 	if (pthread_join(thread_ids[i], NULL) != 0) {
		// 		perror("pthread_join failed");
		// 		exit(1);
		// 	}
		// }
		// --------------------------------------

		current_node = &list[0];

		for (int i = 0; i < npages; ++i) {
			struct node* inLinks = current_node->page->inlinks;
			int sum = 0.0;

			while(inLinks != NULL) {
				sum += scores_t[inLinks->page->index]/(inLinks->page->noutlinks);
				inLinks = inLinks->next;
			}

			scores_t1[i] = ((1- dampener)/npages) + dampener * sum;
			current_node = current_node->next;
		}

		/*
		 * Calculate Convergence
		 */
    	// launch
		for (size_t i = 0; i < nthreads; ++i) {
			if (pthread_create(thread_ids + i, NULL, CalCon, args + i) != 0) {
				perror("pthread_create failed");
				exit(1);
			}
		}

    	// wait for the all the threads to finish
		for (size_t i = 0; i < nthreads; ++i) {
			if (pthread_join(thread_ids[i], NULL) != 0) {
				perror("pthread_join failed");
				exit(1);
			}
		}
		// --------------------------------------

		// for (int j = 0; j < npages; ++j) { // Parallelise
		// 	convergence += (scores_t[j] - scores_t1[j]) * (scores_t[j] - scores_t1[j]);
		// 	// scores_t[j] = scores_t1[j];
		// }

		for (int i = 0; i < nthreads; ++i) {
			convergence += temp_con[i];
		}
		/*
		 * Reinitialise Scores t and scores t1
		 */
    	// launch
		for (size_t i = 0; i < nthreads; ++i) {
			if (pthread_create(thread_ids + i, NULL, Reinit, args + i) != 0) {
				perror("pthread_create failed");
				exit(1);
			}
		}

    	// wait for the all the threads to finish
		for (size_t i = 0; i < nthreads; ++i) {
			if (pthread_join(thread_ids[i], NULL) != 0) {
				perror("pthread_join failed");
				exit(1);
			}
		}
		// --------------------------------------
	}

	current_node = &list[0];
    for(int i = 0; i < npages; ++i){
        printf("%s %.4lf\n",current_node->page->name, scores_t[i]);
        current_node = current_node->next;
    }

    free(args);
    free(temp_con);
}

/*
######################################
### DO NOT MODIFY BELOW THIS POINT ###
######################################
*/

int main(int argc, char** argv) {

	/*
	######################################################
	### DO NOT MODIFY THE MAIN FUNCTION OR HEADER FILE ###
	######################################################
	*/

	config conf;

	init(&conf, argc, argv);

	node* list = conf.list;
	int npages = conf.npages;
	int nedges = conf.nedges;
	int nthreads = conf.nthreads;
	double dampener = conf.dampener;

	pagerank(list, npages, nedges, nthreads, dampener);

	release(list);

	return 0;
}
